#include<stdio.h>
#include<iostream>
#include<fstream>
#define exitThre            15
#define matchThre           0.1
#define EPSILON             0.00001
int globalID = 0;
#define PI                  3.14159265
#define RANSAC_THREHOLD     10
#define orientHist          0
#define fourDimHist         0
#define rgbHist             1
#define HIST_BIN_SIZE       16
float dt = 1;

#define OPENCV3         0

#if(OPENCV3)
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
#include<opencv2/features2d.hpp>
#include<opencv2/calib3d.hpp>
#include<opencv2/tracking.hpp>
#include<opencv2/objdetect.hpp>
#include<opencv2/xfeatures2d.hpp>
#include<opencv2/ml.hpp>
#include<opencv2/video.hpp>
#define RTREESNAME  RTrees
#define DTREESNAME  DTrees
#define LINENAME    LINE_AA
#define CV_ROW_SAMPLE   ROW_SAMPLE
#define CV_VAR_ORDERED  VAR_ORDERED
#define CV_VAR_CATEGORICAL  VAR_CATEGORICAL
#define CV_FOURCC   VideoWriter::fourcc
#else
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>
#include<opencv2/objdetect/objdetect.hpp>
#include<opencv2/calib3d/calib3d.hpp>
#include<opencv2/nonfree/nonfree.hpp>
#include<opencv2/ml/ml.hpp>
#define RTREESNAME  RandomTrees
#define DTREESNAME  DecisionTree
#define LINENAME    CV_AA
#endif

//#include<kalmanFilterClass.h>
#define IMG_APPEARANCE_MODEL    0

#define TS2         0
#define TRACKING_STRATEGY3      1   // Spatial Layout Graph Matching + Bayesian Framework
#define RF_LEARN                0   // Random Forest Learning for each tracker
#define SURF_RESOLVER           0
#define TEST_FF_CC              0   // Testing flood fill (connected components)
#define KF_PAIRS                1   // Defining KF pair model
#define KF_PAIRS_PRED           0   // Prediction based on KF_PAIRS
#define SLC_PRED                0   // Prediction based on SLC
//#include<gpf.h>


#include<dai/bp.h>
#include<dai/jtree.h>
#include<dai/factorgraph.h>

//#include<Timer.h>

#include<tinyxml.h>
#include<tinystr.h>

#if(!TRACKING_STRATEGY3)
#define HUNGARIAN               1   // 0 for BIP and 1 for Hungarian Method
#endif

#if(HUNGARIAN)
#include<munkres.h>
#endif

//#include<tracking/include/opencv2/tracking.hpp>

// PF params
#define delta       0.01
#define var_x       50.0
#define var_y       20.0
#define var_xdot    1.0
#define var_z       1.0
#define Nx          4   // can't be changed for now
#define Nz          2   // can't be changed for now
//#define resample_percentage 0.025;
#define Ns          100
#define Nt          5

// One and only one of the following two flags should always be 1
#define PF_MOTION               0       // Particle Filter
#define KF_MOTION               1       // Kalman Filter

#define KF_4                    0       // Using 4 measure params for KF

// WARNING: These parameters need to be set same in source file as well
#define IMG_SEG_GRAPH_MATCH     0

#define K_MEANS         10

#define GMM_COMPONENTS_FG       5
#define GMM_COMPONENTS_BG       5
#define GMM_FG_BG_THRESH    0.8         // Probabilty threshold to call a pixel FG if greater than this value
#define FLOOD_FILL_DIFF     20


using namespace cv;
using namespace std;

#if(OPENCV3)
using namespace cv::ml;
using namespace cv::xfeatures2d;
#endif

void clusterImgWithPos(Mat segmentImg, int K, vector<Point2f>& clusterCenters, vector<Vec3b>& colors, Mat& imgMeans, Mat &outImg);
void generateSamples_GMM(Mat inputImg, Rect agentRoi, vector<Rect> activeAgentsWin, Mat& samplesFG, Mat& samplesBG);
void updateSamples_GMM(Mat& samplesFG, Mat& samplesBG, Mat samplesFGNew, Mat samplesBGNew, float oldSamplesRatio = 0.8);
void trainGMM(Ptr<EM> &emModel, Mat samples, int components);
void createGMModel(Mat inputImg, Rect agentRoi, Ptr<EM> &m1, Ptr<EM> &m2, Ptr<EM> &m3, Ptr<EM> &m4);
void calcHistogram(Mat srcImg, Mat& hist, int bins );
void scaleRect(Rect& ioRoi, float percentScale);
static Ptr<TrainData> prepare_train_data(const Mat& data, const Mat& responses, int ntrain_samples);
float findOverlap(Rect r1, Rect r2);
void getHist(Mat frameData, Rect reg, Mat& hist, int bins = HIST_BIN_SIZE);
void rf_generateNegativeSamples(Mat img, Rect agnPos, Mat& rfSamples, Mat& rfLabels, vector<Rect> activeAgentsWin);

#if(PF_MOTION)
//----------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------PARTCLE FILTER----------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------
//----------------------------
// Process Equation
// xn : process noise
void process(std::vector<double> &xk, const std::vector<double> &xkm1, void* data)
{
  gsl_rng *r = (gsl_rng*)data;

  xk[0] = xkm1[0] + delta * xkm1[1] + gsl_ran_gaussian(r, sqrt(var_x));
  xk[1] = xkm1[1] +  gsl_ran_gaussian(r, sqrt(var_xdot));
  xk[2] = xkm1[2] + delta * xkm1[3] + gsl_ran_gaussian(r, sqrt(var_y));
  xk[3] = xkm1[3] + gsl_ran_gaussian(r, sqrt(var_xdot));
}
//-------------------------
// Observation Equation
// vn: measurement noise
void observation(std::vector<double> &zk, const std::vector<double> &xk, void* data)
{
 gsl_rng *r = (gsl_rng*) data;

  // (x,y) position
  zk[0] = xk[0] +  gsl_ran_gaussian(r, sqrt(var_x));
  zk[1] = xk[2] +  gsl_ran_gaussian(r, sqrt(var_y));

}
//-----------------------------------------------------
// Likelihood is a t-distribution with nu = 10
double likelihood(const std::vector<double> &z, const std::vector<double> &zhat, void* data)
{

  double prod = 1.0, e;
  for(uint i = 0; i < z.size(); ++i)
  {
    e = z[i] - zhat[i];
   //prod = prod* gsl_ran_gaussian_pdf(e, var_z);
   prod = prod * 1.5 * pow((1+e*e/10), -5.5);
  }

  return prod;
}
//-----------------------------------
//----------------------------------------------------------------------------------------------------------------------------
#endif



class MultiTracker
{
private:

protected:

public:

    Rect MS_Rect;       //Mean Shift predicted window
    Rect trackWindow;  //position of detected agent

    //    KalmanFilter_ KF;
    KalmanFilter KF_CV;//Kalman Filter openCV

    Rect KF_RectPredicted;  //predicted region through kalman filter
    Rect prevWindow;

    int bins; //no. of bins of the histogram
    int orientbins; //no. of orientation bins
    Mat hist,rgb_hist,orient_hist,fourDim_hist; //histogram of the agent
    Mat grad; //gradient of tracked window of the agent
    Mat orient; //orients of tracked window of the image

    Mat agentOriginal,accMatrix,invAccMatrix;
    int id; //agent ID
    bool status;        // active=true or passive=false
    int  exitTimeSpan;  // remove the agent passive for duration greater than this
    int activeTimeSpan;
    unsigned char red,green,blue;   //Color of the agent
    vector<Point2i> traj, activeTraj; //Vector storing trajectory for the agent
    float avgVelocityDirection;
    float scale;

    float Epsilon;
    float PrevBhatCoeff;
    float CurrBhatCoeff;
    float ItrNumBound;
    float SubItrNumBound;
    float ItrCond;
    bool UpdateScaleAfterIterations;
    float XScaleChange;
    float YScaleChange;
    bool ToTrack;
    bool MeanShiftTrackerDataReady;
    Rect PrevRegion;
    Rect CurrRegion;
    Mat TargetHist;
    Mat PrevPosHist;
    Mat CurrPosHist;
    int ItrNum;


    // Parameters for graph matching module
    vector<Point2f> clusterCenters;
    vector<Vec3b> clusterColors;
    Mat imgMeans;
    Mat segmentedImg;

    // Parameters for groups reolution
    bool endangered;            // For GROUP_RESOLVE_SURF_2, it identifies cases where resolution has to be done
    Rect initialAssignment;     // This is the assigned roi for an agent, changes can be made and then finally passed to updateAgent
    Mat agentImgSolo;           // A snapshot image of an agent when it is not in a group
    bool inGroup;               // Flag that tells if the agent is in a group or not
    bool dead;                  // If an agent has died (i.e. been passive for certain number of frames (exitThresh) )

    // Parameters for appearance model and graph matching Part-2
    Mat segmentedImg2;      // Segmented image based on GMM model, FG as true colors and BG as black
    Mat graphImg;           // Draws graph on the given src image based on GMM model segmentation and flood fill
    Ptr<EM> m1, m2, m3, m4;      // EM model for head, torso, legs and background respectively or alternatively m1 for fg and m4 for bg
    Mat samplesFG, samplesBG;       // Samples for GMM trained for FG and BG
    Mat_<int> adjMat_src;   // Adjacency Matrix for the src graph of agent
    vector<Point2f> ccPoints;       // Center points of the nodes of the graph
    vector<int> selectedCompInd;    // Indicies of components of GMM model of Agent FG that are selected for the graph
    vector<Vec3b> ccColors;         // Mean colors of the components
    Mat histSrcGraph;               // Histogram of src image based on FG selected by GMM segmentation

    Mat agentImgUpdated;            // The image of agent after a tracker of that agent is updated

    // Parameters for Particle Filter
#if(PF_MOTION)
    gsl_rng *rg;

    std::vector<double> x_PF;      // state
    std::vector<double> z_PF;
    std::vector<double> xf;     // Filtered state
    std::vector<double> wt;     // weights

    PF::pf pointCloud;

#endif

    // Extra parameters for writing result data in XML
    vector<int> frameIds;       // vector of frame number in which this id was detected
    vector<Rect> frameROIs;     // vector of all rectangles detected for this id
    vector<int> frameObservationFlag;   // vector of observation flag (1 for detection 0 for prediction)

    // Parameters of TS3
    int agentState;     // agent state = 1 is active, 2 is InterObjectOcclusion, 3 is BG occlusion, 4 is Lack of confirmation (refine 3,4)
    int occludingAgentID;   // ID of the agent who has occluded the current agent
    map< int,pair<float,float> > relation;  // mapping with the other agents (ids) and the corresponding dij and occ_prob
    map< int,Rect > relationROIs;
//    Ptr<Tracker> tldTracker;
    Ptr<RTREESNAME> randForest;
    Ptr<DTREESNAME> dtree;
    Mat rfSamples, rfLabels;
    int coPedId;            // ID of the agent who is probably walking together with this agent
    Rect initialPrediction; // Required for setting a prediction window using SURF
//    map< int, KalmanFilter > relation2; // A Kalman Filter Predictor for every pair of tracker

    MultiTracker();
    void initAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool appearanceModel);   //Initializes newly detected agents.
    void updateAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool appearanceModel); //Updates active agents if they match detected windows.
    void computeColorDistributionRGB(Mat img, Rect agnPos,Mat &hist); //Find RGB Histogram of provided rectangle in image.
    void markAgents(Mat img);   //For plotting agent position.
    void gradImage(Mat img, Rect agnWin, Mat& grad, Mat& orient); //Calculates gradient magnitudes of the pixels in the agent window.
    void calcOrientHist(Mat img,Rect agnPos);

    void meanShiftTrack(Mat& image, Rect& selection, int& trackObject);

    void localizeAppearanceByMeanShift( Mat img, float bhatCoefThreshold, float* matchVal );
    void meanShiftTrackingRGB( Mat frameData , float bhatCoeffThreshold );
    void setMeanShiftTrackerTarget( Mat targetHistRGB , Rect initReg );
    void iterateRGB( Mat frameData );
    void updateScaleRGB( Mat frameData );
    void copyHistogram( Mat srcHist ,Mat destHist );
    void getNewRegionRGB( Mat frameData , Rect&  newRegion );

//    ~MultiTracker();
};


unsigned char computeBhattacharyaCoefficient( Mat hist1, Mat hist2, float* bhatCoefVal );
void calcOverlap(Point tl1, Point tl2, Size sz1, Size sz2, float *overlap);
void checkBoundary(Mat frameData, Rect& tempRect);

MultiTracker::MultiTracker()
{
    bins = HIST_BIN_SIZE;
    orientbins = HIST_BIN_SIZE;
    hist = Mat(1, pow(bins,3), CV_32FC1, Scalar(0));

    if(rgbHist)
    {
        TargetHist = Mat(1, pow(bins,3) ,CV_32FC1,Scalar(0));
        PrevPosHist = Mat(1, pow(bins,3),CV_32FC1,Scalar(0));
        CurrPosHist = Mat(1, pow(bins,3),CV_32FC1,Scalar(0));
    }

    if(orientHist)
    {
        TargetHist = Mat(1, pow(orientbins,3) ,CV_32FC1,Scalar(0));
        PrevPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
        CurrPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
    }

    if(fourDimHist)
    {
        TargetHist = Mat(1, pow(orientbins,3) ,CV_32FC1,Scalar(0));
        PrevPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
        CurrPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
    }

    orient_hist = Mat(1, pow(orientbins,3), CV_32FC1, Scalar(0));
    rgb_hist = Mat(1, pow(bins,3), CV_32FC1, Scalar(0));
    fourDim_hist = Mat(1, pow(orientbins,3), CV_32FC1, Scalar(0));

#if(PF_MOTION)

    long seed = time(NULL)*getpid();
    rg = gsl_rng_alloc(gsl_rng_rand48);
    gsl_rng_set(rg,seed);

    // Parameters for PF
    x_PF.assign(Nx,0.0);
    z_PF.assign(Nz,0.0);
    xf.assign(Nx,0.0);
    wt.assign(Ns,0.0);

    pointCloud = PF::pf(Ns, Nx, Nz, PF::WRSWR);

#endif
}


/*!
  @brief Initializes Agents detected for first time.
  @param[in] img Input image for agent properties.
  @param[in] agnPos Position of agent in the given image.
*/
void MultiTracker::initAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool trainingGMM = false)
{
//    id = globalID;
//    globalID++;

    prevWindow = agnPos;
    trackWindow = agnPos;
    MS_Rect = agnPos;
    Mat img2 = img.clone();
//    cvtColor(img,img2,CV_BGR2HSV);

    computeColorDistributionRGB(img2 , agnPos, hist);

    //    KF.setInitialState(Mat(Point2f(agnPos.x, agnPos.y)));
    //    KF_RectPredicted = agnPos;
    gradImage(img2,trackWindow, grad, orient);
    calcOrientHist(img2, agnPos);

    exitTimeSpan = 0;
    status = true;
    activeTimeSpan = 1;

    //Generating random values for random color generation of the agents
    timespec t1;
    clock_gettime(0,&t1);
    RNG rng(t1.tv_nsec);

    red = rng.uniform(0,255);
    green = rng.uniform(0,255);
    blue = rng.uniform(0,255);

    // Setting the Mean-Shist Tracker Parameters : Initialising Variables
    Epsilon = 1.0;
    PrevBhatCoeff = 0.0;
    ItrNumBound = 5;
    SubItrNumBound = 5;
    ItrCond = 255;
    UpdateScaleAfterIterations = true;
    XScaleChange = 0.0;
    YScaleChange = 0.0;
    ToTrack = true;
    MeanShiftTrackerDataReady = false;

    activeTraj.push_back(Point2i(agnPos.x+agnPos.width/2,agnPos.y+agnPos.height/2));
    avgVelocityDirection = 0;

    //Predicting kalman filter
    //    Mat kalmanRect = KF.predict();
    //    KF_RectPredicted.x = kalmanRect.at<float>(0);
    //    KF_RectPredicted.y = kalmanRect.at<float>(1);

    //    checkBoundary(img,KF_RectPredicted);

#if(KF_MOTION)

    dt = 0.5;

#if(KF_4)
    KF_CV.init(8,4,0);
    KF_CV.transitionMatrix = (Mat_<float>(8,8) << 1,0,0,0,dt,0,0,0, 0,1,0,0,0,dt,0,0, 0,0,1,0,0,0,dt/20,0, 0,0,0,1,0,0,0,dt/20,
                              0,0,0,0,1,0,0,0, 0,0,0,0,0,1,0,0, 0,0,0,0,0,0,0.01,0, 0,0,0,0,0,0,0,0.01); // A
    KF_CV.statePre.at<float>(0) = agnPos.x+agnPos.width/2;
    KF_CV.statePre.at<float>(1) = agnPos.y+agnPos.height/2;
    KF_CV.statePre.at<float>(2) = agnPos.width;
    KF_CV.statePre.at<float>(3) = agnPos.height;

    Point2f c1(0.5*(trackWindow.tl()+trackWindow.br()));
    Mat measure = (Mat_<float>(4,1) << c1.x, c1.y, agnPos.width, agnPos.height);

#else
    KF_CV.init(4,2,0);
    KF_CV.transitionMatrix = (Mat_<float>(4, 4) << 1,0,dt,0,0,1,0,dt,0,0,1,0,0,0,0,1); // A
    KF_CV.statePre.at<float>(0) = agnPos.x+agnPos.width/2;
    KF_CV.statePre.at<float>(1) = agnPos.y+agnPos.height/2;
    Mat measure = Mat(0.5*(trackWindow.tl()+trackWindow.br()));
#endif

    setIdentity(KF_CV.measurementMatrix); // H
    setIdentity(KF_CV.processNoiseCov, Scalar::all(0.01)); // Q
    setIdentity(KF_CV.measurementNoiseCov, Scalar::all(0.01)); // R
    setIdentity(KF_CV.errorCovPost, Scalar::all(0.01)); // P

//    randn(KF_CV.statePost, Scalar::all(0), Scalar::all(0.1));
//    KF_CV.statePost = KF_CV.statePre.clone();


    measure.convertTo(measure,CV_32FC1);
    KF_CV.correct(measure);

    KF_CV.predict();
#if(KF_4)
    KF_RectPredicted.width = KF_CV.statePost.at<float>(2);
    KF_RectPredicted.height = KF_CV.statePost.at<float>(3);
#else
    KF_RectPredicted.width = agnPos.width;
    KF_RectPredicted.height = agnPos.height;
#endif

    KF_RectPredicted.x = KF_CV.statePost.at<float>(0)-KF_RectPredicted.width/2;
    KF_RectPredicted.y = KF_CV.statePost.at<float>(1)-KF_RectPredicted.height/2;

    checkBoundary(img,KF_RectPredicted);

#endif

#if(PF_MOTION)

    z_PF[0] = agnPos.x+agnPos.width/2;;
    z_PF[1] = agnPos.y+agnPos.height/2;;

    vector<double> mean;
    mean.push_back(z_PF[0]);
    mean.push_back(z_PF[1]);

    pointCloud.initialize(0, mean, var_x);

    // Actual values
    x_PF[0] = gsl_ran_gaussian(rg, var_x);
    x_PF[1] = gsl_ran_gaussian(rg, var_xdot);
    x_PF[2] = gsl_ran_gaussian(rg, var_y);
    x_PF[3] = gsl_ran_gaussian(rg, var_xdot);


    KF_RectPredicted.x = z_PF[0] - KF_RectPredicted.width/2;
    KF_RectPredicted.y = z_PF[1] - KF_RectPredicted.height/2;

    checkBoundary(img,KF_RectPredicted);

#endif

#if(IMG_SEG_GRAPH_MATCH)
    clusterImgWithPos(img(agnPos).clone(),K_MEANS,clusterCenters,clusterColors,imgMeans,segmentedImg);
#endif

    if(trainingGMM)
    {
        generateSamples_GMM(img,agnPos,activeAgentsWin,samplesFG,samplesBG);

        // Learn the respective models
        trainGMM(m1,samplesFG,int(GMM_COMPONENTS_FG));
        trainGMM(m4,samplesBG,int(GMM_COMPONENTS_BG));

        calcHistogram(segmentedImg2,histSrcGraph,HIST_BIN_SIZE);
    }

    // Extra parameters for writing result data in XML
//    if(frameId != 0)
//    {
//        frameIds.push_back(frameId);
//        frameROIs.push_back(agnPos);
//        frameObservationFlag.push_back(1);
//    }

    endangered = false;
    initialAssignment = Rect(0,0,0,0);
    agentImgSolo = img(agnPos).clone();
    inGroup = false;
    dead = false;

    agentImgUpdated = img(agnPos).clone();

    agentState = 1;
    occludingAgentID = -1;

    coPedId = -1;
    initialPrediction = Rect(0,0,0,0);

//    Rect expandedROI = agnPos;
//    scaleRect(expandedROI,1.5);
//    checkBoundary(img,expandedROI);
//    Rect2d newRectPos = agnPos;
//    newRectPos.x = agnPos.x - expandedROI.x;
//    newRectPos.y = agnPos.y - expandedROI.y;
//    tldTracker = Tracker::create("MIL");
//    bool initSuccess = tldTracker->init(img/*(expandedROI)*/.clone(),agnPos);//newRectPos);
//    if(!initSuccess)
//        cerr << "Tracker Init was unsuccessful for Id " << id << endl;

//    randForest->create(RTREESNAME::Params(5,5,0,false,0,Mat(),false,0,TermCriteria(TermCriteria::MAX_ITER + TermCriteria::EPS, 50, 0.1f)));

#if(RF_LEARN)
    rfSamples.push_back(hist);
    rfLabels.push_back(1);
//    rfSamples.push_back(hist);
//    rfLabels.push_back(1);

    rf_generateNegativeSamples(img,agnPos,rfSamples,rfLabels,activeAgentsWin);

    Ptr<TrainData> tdata = prepare_train_data(rfSamples, rfLabels, rfSamples.rows);
    randForest = StatModel::train<RTREESNAME>(tdata,RTREESNAME::Params(5,5,0,false,10,Mat(),false,0,
                                                               TermCriteria(TermCriteria::MAX_ITER + TermCriteria::EPS, 50, 0.1f)));
//    dtree = StatModel::train<DTREESNAME>(tdata,DTREESNAME::Params(INT_MAX,10,0.01f,false,10,10,true,true,Mat()));
#endif

}


/*!
  @brief Updates agent with HogWin associtaed to it.
  @param[in] img Input image for agent properties.
  @param[in] agnPos Position of agent in the given image.
*/
void MultiTracker::updateAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool trainingGMM = false)
{
    prevWindow = trackWindow;
    scale = (agnPos.area())/(64.0*128.0);

    KF_RectPredicted.width = trackWindow.width;
    KF_RectPredicted.height = trackWindow.height;

    Mat imgTemp = img.clone();
//    cvtColor(img,imgTemp,CV_BGR2HSV);

    trackWindow = agnPos;
//    trackWindow.width = 0.5 * (prevWindow.width + agnPos.width);
//    trackWindow.height = 0.5 * (prevWindow.height + agnPos.height);

    Mat oldHist = hist.clone();
    computeColorDistributionRGB(imgTemp , agnPos, hist);
    Mat avgHist;
    add(oldHist,hist,avgHist);
    avgHist *= 1/sum(avgHist)[0];

    hist = avgHist.clone();

    gradImage(imgTemp,agnPos, grad, orient);
    calcOrientHist(imgTemp, agnPos);

    exitTimeSpan = 0;
    status = true;
    activeTimeSpan++;
    dead = false;

#if(KF_MOTION)

#if(KF_4)
    Point2f c1(0.5*(trackWindow.tl()+trackWindow.br()));
    Mat measure = (Mat_<float>(4,1) << c1.x, c1.y, agnPos.width, agnPos.height);
#else
    setIdentity(KF_CV.measurementMatrix,Scalar::all(1));
    Mat measure = Mat(0.5*(trackWindow.tl()+trackWindow.br()));
#endif
    measure.convertTo(measure,CV_32FC1);
    KF_CV.correct(measure);

    KF_CV.predict();

#if(KF_4)
    KF_RectPredicted.width = KF_CV.statePost.at<float>(2);
    KF_RectPredicted.height = KF_CV.statePost.at<float>(3);
#endif

    KF_RectPredicted.x = KF_CV.statePost.at<float>(0) - KF_RectPredicted.width/2;
    KF_RectPredicted.y = KF_CV.statePost.at<float>(1) - KF_RectPredicted.height/2;
    checkBoundary(img, KF_RectPredicted);
#endif

#if(PF_MOTION)

    process(x_PF, x_PF, (void*)rg);             // p(xk | xkm1)

//    for(uint i = 0; i < Nz; ++i)
//    {
    z_PF[0] = (trackWindow.x+trackWindow.width/2) + gsl_ran_gaussian(rg, var_z);
    z_PF[1] = (trackWindow.y+trackWindow.height/2) + gsl_ran_gaussian(rg, var_z);
//    }

    pointCloud.particleFilterUpdate(process, observation, likelihood, z_PF, 0); // Don't resample  here

    float neff = pointCloud.getEffectivePopulation();
    pointCloud.filterOutput(xf);

    if(ceil(neff) < Nt)
    {
      pointCloud.resample();
//      cout << "k = " << k << "\tNeff = " << neff << "\t Resampling ..." << endl;
    }
//    else
//    {
//      nonSampItnCnt++;
//      cout << "k = " << k << "\tNeff = " << neff << endl;
//    }


    KF_RectPredicted.x = xf[0] - KF_RectPredicted.width/2;
    KF_RectPredicted.y = xf[2] - KF_RectPredicted.height/2;

    checkBoundary(img, KF_RectPredicted);

#endif


    // Setting the Mean-Shist Tracker Parameters : Initialising Variables
    Epsilon = 1.0;
    PrevBhatCoeff = 0.0;
    ItrNumBound = 5;
    SubItrNumBound = 5;
    ItrCond = 255;
    UpdateScaleAfterIterations = true;
    XScaleChange = 0.0;
    YScaleChange = 0.0;
    ToTrack = true;
    MeanShiftTrackerDataReady = false;


    activeTraj.push_back(Point2i(agnPos.x+agnPos.width/2,agnPos.y+agnPos.height/2));
    Point2f currPosn = activeTraj[activeTraj.size()-1];
    Point2f prevPosn = activeTraj[activeTraj.size()-2];
    float instantVelDir = atan2(currPosn.y-prevPosn.y,currPosn.x-prevPosn.x);
    avgVelocityDirection = (instantVelDir + avgVelocityDirection*(activeTraj.size()-2))/(activeTraj.size()-1);

    if(!frameIds.empty())
    {
        if(frameIds[frameIds.size()-1] != frameId)
        {
            frameIds.push_back(frameId);
            frameROIs.push_back(agnPos);
            frameObservationFlag.push_back(1);
        }
    }
    else
    {
    frameIds.push_back(frameId);
    frameROIs.push_back(agnPos);
    frameObservationFlag.push_back(1);
    }

    if(!inGroup)
        agentImgSolo = img(agnPos).clone();
    agentImgUpdated = img(agnPos).clone();

    initialAssignment = Rect(0,0,0,0);

    // Update the GMM model
//    if(trainingGMM)
//    {
//        Mat samplesNewFG, samplesNewBG;
//        generateSamples_GMM(img,agnPos,activeAgentsWin,samplesNewFG,samplesNewBG);
//        updateSamples_GMM(samplesFG,samplesBG,samplesNewFG,samplesNewBG);

//        trainGMM(m1,samplesFG,GMM_COMPONENTS_FG);
//        trainGMM(m4,samplesBG,GMM_COMPONENTS_BG);
//    }

//    if(occludingAgentID != -1)
//    {
//        relation[occludingAgentID].second = 0.5;
//        relation[id].second = 0.5;
//    }

    agentState = 1;
    occludingAgentID = -1;

    initialPrediction = Rect(0,0,0,0);

#if(RF_LEARN)
    float tempBC = 0;
    computeBhattacharyaCoefficient(oldHist,hist,&tempBC);
    if(tempBC > 0.5)
    {
//        rfSamples.push_back(oldHist);
//        rfLabels.push_back(1);

        rfSamples.push_back(hist);
        rfLabels.push_back(1);
    }

    rf_generateNegativeSamples(img,agnPos,rfSamples,rfLabels,activeAgentsWin);

    Ptr<TrainData> tdata = prepare_train_data(rfSamples, rfLabels, rfSamples.rows);
    int minSampleCount = 0.01*rfSamples.rows;
    minSampleCount = minSampleCount >= 1 ? minSampleCount : 1;
    randForest = StatModel::train<RTREESNAME>(tdata,RTREESNAME::Params(100,1,0,false,10,Mat(),false,0,
                                                       TermCriteria(TermCriteria::MAX_ITER + TermCriteria::EPS, 100, 0.01f)));
//    dtree = StatModel::train<DTREESNAME>(tdata,DTREESNAME::Params(INT_MAX,10,0.01f,false,10,10,true,true,Mat()));

#endif
}


void rf_generateNegativeSamples(Mat img, Rect agnPos, Mat& rfSamples, Mat& rfLabels, vector<Rect> activeAgentsWin)
{
    // Pass a random roi from image (not overlapping with agent)
    RNG randNum;
    bool foundRandomRect = false, foundRandAgent = true;
    while(foundRandomRect == false || foundRandAgent == false)
    {
        if(!foundRandomRect)
        {
            int randX = randNum.uniform(0,img.cols);
            int randY = randNum.uniform(0,img.rows);
            Rect randRect(randX,randY,agnPos.width,agnPos.height);
            float randOlap = findOverlap(agnPos,randRect);
            if(randOlap == 0)
            {
                foundRandomRect = true;
                Mat randHist;
                getHist(img,randRect,randHist);
                rfSamples.push_back(randHist);
                rfLabels.push_back(-1);
            }
        }

        if(!foundRandAgent)
        {
            int randID = randNum.uniform(0,activeAgentsWin.size());
            float randOlap = findOverlap(agnPos,activeAgentsWin[randID]);
            if(randOlap == 0)
            {
                foundRandAgent = true;
                Mat randHist;
                getHist(img,activeAgentsWin[randID],randHist);
                rfSamples.push_back(randHist);
                rfLabels.push_back(-1);
            }
        }
    }

}

/*!
  @brief Finds HOG windows in the given image.
  @param[in] img Input image for detection.
  @param[in,out] found_filtered Output detected windows.
  @return Number of detections.
*/
int HogDetector(Mat img, vector<Rect>& found_filtered, float hitRate = 0.0)
{
    found_filtered.clear();
    int rect_num=0;

    HOGDescriptor hog;
//    hog.nlevels = 200;
    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

    vector<Rect> found;
    vector<Rect> tempRects;
    // run the detector with default parameters. to get a higher hit-rate
    // (and more false alarms, respectively), decrease the hitThreshold and
    // groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
    hog.detectMultiScale(img, found, hitRate, Size(8,8), Size(0,0), 1.05);
    size_t i, j;
    for( i = 0; i < found.size(); i++ )
    {
        Rect r = found[i];
        for( j = 0; j < found.size(); j++ )
            if( j != i && (r & found[j]) == r)
                break;
        if( j == found.size() )
            tempRects.push_back(r);
    }
    for( i = 0; i < tempRects.size(); i++ )
    {
        Rect r = tempRects[i];
        // the HOG detector returns slightly larger rectangles than the real objects.
        // so we slightly shrink the rectangles to get a nicer output.
        r.x += cvRound(r.width*0.125);
        r.width = cvRound(r.width*0.7);
        r.y += cvRound(r.height*0.1);
        r.height = cvRound(r.height*0.75);
        //    rectangle(img, r.tl(), r.br(), cv::Scalar(0,255,0), 3);
        found_filtered.push_back(r);
    }
    rect_num=found_filtered.size();
    return( rect_num);
}



/*!
  @brief Finds colour distribution of given location in the image.
  @param[in] frameData Input image for histogram extraction.
  @param[in] reg rectangle to location in the image.
  @param[in] hist Histogram to be calculated for the specified rectangle in the image.
*/
void MultiTracker::computeColorDistributionRGB(Mat frameData, Rect reg, Mat& hist)
{
    reg.x += cvRound(reg.width*0.2);
    reg.width = cvRound(reg.width*0.6);
    reg.y += cvRound(reg.height*0.1);
    reg.height = cvRound(reg.height*0.8);

    for(int i=0; i<hist.cols; i++)
        hist.at<float>(0,i)=0.0;

    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            Vec3b color = frameData.at< Vec3b >(y,x);

            // Compute the Bin
            int redBin = (int) (invBinSize * ( (unsigned char) color[2]));
            int greenBin = (int) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (int) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = (redBin * binSqr ) + ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > EPSILON )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}



/*!
  @brief Finds orientation/HSO histogram.
  @param[in] frameData Input image for histogram calculation.
  @param[in] reg Portion where histogram to be calculated in the image.
*/
void MultiTracker::calcOrientHist(Mat frameData, Rect reg)
{
    if(fourDimHist)
    {
        for(int i=0; i<fourDim_hist.cols; i++)
            fourDim_hist.at<float>(0,i) = 0.0;
    }

    if(orientHist)
    {
        for(int i=0; i<orient_hist.cols; i++)
            orient_hist.at<float>(0,i) = 0.0;
    }

    int tempbins = HIST_BIN_SIZE;
    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( tempbins ) ) / 256.0;
    int binSqr = ( tempbins ) * ( tempbins );

    /************************
        for orientations-3D histogram
     ************************/

    Mat tempGradients, tempOrientations; //calculating gradients and orientations for histogram calculation for a window
    gradImage(frameData, reg, tempGradients, tempOrientations);

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            Vec3f orientBin = tempOrientations.at<Vec3f>(y-reg.y, x-reg.x);

            int histIndx;

            if(fourDimHist)
            {
                Vec3b color = frameData.at<Vec3b>(y,x);

                int max;

                if(orientBin[0]>=orientBin[1] && orientBin[0]>=orientBin[2]) max = orientBin[0];
                if(orientBin[1]>=orientBin[0] && orientBin[1]>=orientBin[2]) max = orientBin[1];
                if(orientBin[2]>=orientBin[0] && orientBin[2]>=orientBin[1]) max = orientBin[2];

                // Compute the Bin
                int redBin = (int) (invBinSize * ( (float) color[2]));
                int greenBin = (int) (invBinSize * ( (float) color[1]));
                int blueBin = (int) (invBinSize * ( (float) color[0]));

                histIndx = (redBin * pow(tempbins,2) ) + ( greenBin * pow(tempbins,1) ) /*+ (blueBin*tempbins) */+ max;
            }

            if(orientHist)
                histIndx = (orientBin[2] * pow(orientbins,2) ) + ( orientBin[1] * pow(orientbins,1) ) + orientBin[0];


            // Update Histogram and Weight Sum

            if(orientHist)
                orient_hist.at<float>(0,histIndx ) = orient_hist.at<float>(0,histIndx ) + 1;

            if(fourDimHist)
                fourDim_hist.at<float>(0,histIndx ) = fourDim_hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    float inv_sumW = 1.0 / sumW;

    // Normalizing the Histogram
    if( sumW > EPSILON )
    {
        int cols;
        if(fourDimHist) cols = fourDim_hist.cols;
        if(orientHist) cols = orient_hist.cols;

        for( int histIndx = 0 ; histIndx < cols  ; ++histIndx )
        {
            if(fourDimHist)
                fourDim_hist.at<float>(0, histIndx) = inv_sumW * ( fourDim_hist.at<float>(0,histIndx ) );

            if(orientHist)
                orient_hist.at<float>(0, histIndx) = inv_sumW * ( orient_hist.at<float>(0,histIndx ) );
        }
    }
}



/*!
  @brief Calculates gradient of the agent provided throug agent window.
  @param[in] frameData image from which gradient & orientation to be calculated.
  @param[in] angWin Portion for which gradient & orienations to be calculated in the image.
  @param[in,out] grad Matrix into which gradient mag of the specified location are stored.
  @param[in,out] orient Matrix into which orientations of the specified location are stored.
*/
void MultiTracker::gradImage(Mat frameData, Rect agnWin, Mat& grad, Mat& orient)
{
    grad = Mat(agnWin.height, agnWin.width, CV_32FC3 ,Scalar(0,0,0));

    orient = Mat(agnWin.height, agnWin.width, CV_32FC3,Scalar(0,0,0));

    //    grad = frameData.rowRange(agnWin.y, agnWin.y+agnWin.height).colRange(agnWin.x, agnWin.x+agnWin.width).clone();
    //    GaussianBlur( grad, grad, Size(3,3), 0, 0 );
    //    Laplacian(grad, grad, CV_16S, 3, 1, 1);
    //    convertScaleAbs( grad, grad);

    Mat tempGrad = Mat(agnWin.height, agnWin.width, CV_8UC3,Scalar(0,0,0));
    tempGrad = frameData.rowRange(agnWin.y, agnWin.y+agnWin.height).colRange(agnWin.x, agnWin.x+agnWin.width).clone();

    GaussianBlur( tempGrad, tempGrad, Size(3,3), 0, 0 );

    Mat tempGrad1 = Mat(agnWin.height+2, agnWin.width+2, CV_8UC3,Scalar(0,0,0)); //window appended with zeros to calculate gradients
    tempGrad.copyTo(tempGrad1.rowRange(1, tempGrad1.rows-1).colRange(1, tempGrad1.cols-1));

    float binSize = 180.f/(orientbins);
    //    cout << binSize << "\t" << 180.0/orientbins << "\t" << 180.0/float(orientbins) << "\t" << 180/float(orientbins) << endl;

    for(int y=1; y<tempGrad1.rows-1; y++)
    {
        for(int x=1; x<tempGrad1.cols-1; x++)
        {
            Vec3f mag;
            Vec3f angle;
            Vec3b magdown = tempGrad1.at<Vec3b> (y+1,x);   Vec3b magup = tempGrad1.at<Vec3b> (y-1,x);
            Vec3b magright = tempGrad1.at<Vec3b> (y,x+1);   Vec3b magleft = tempGrad1.at<Vec3b> (y,x-1);

            float dx,dy;
            for(int i=0; i<3; i++)
            {
                dx = (float) (magright[i] - magleft[i]);
                dy = (float) (magup[i] - magdown[i]);
                mag[i] = sqrt((dx*dx) + (dy*dy));
                angle[i] = atan2(dy,dx);
                angle[i] = cvRound((angle[i]*180/PI)+180)/2;
                if(angle[i] > 180 )
                {
                    cout << "angle >180"<< endl;
                    exit(-1);
                }

                angle[i] = floor((angle[i]-(binSize/2))/binSize);
            }

            grad.at<Vec3f> (y-1,x-1) = mag;
            orient.at<Vec3f> (y-1,x-1) = angle;
        }
    }
}



/*!
  @brief Finds BhattacharyaCoeff between two hisrograms.
  @param[in] hist1 First Histogram.
  @param[in] hist2 Second Histogram.
  @param[out] bhatCoefVal Bhattacharya Coeff obtained through histogram matching.
*/
unsigned char computeBhattacharyaCoefficient( Mat hist1, Mat hist2 , float* bhatCoefVal )
{
    (*bhatCoefVal) = 0.0;
    for( int counter = 0 ; counter < hist1.cols ; ++counter )
        (*bhatCoefVal) = (*bhatCoefVal) + sqrt( ( hist1.at<float>(0,counter) ) * ( hist2.at<float>(0, counter) ) );

    return( 0 );
}



/*!
  @brief Mark the agents with their assigned colours.
  @param[in] img Image in which markings to be done.
*/
void MultiTracker::markAgents(Mat img)
{

    if(!dead /*&& activeTimeSpan >1*/)
    {
        if(status == true)
        {
            rectangle(img,trackWindow.tl(), trackWindow.br(), Scalar(blue,green,red), 3);

            char tempText[100];
            sprintf(tempText,"%d",id);
            putText(img,tempText,Point(trackWindow.x,trackWindow.y),2, 1,Scalar(red,green,blue),2,LINENAME,false);

            traj.push_back(Point(trackWindow.x+trackWindow.width/2, trackWindow.y+trackWindow.height));

            if(traj.size()>1)
            {
                for(int i=0; i < traj.size()-1; i++)
                    line(img,traj[i+1],traj[i],Scalar(blue,green,red),2);
            }
        }

        if(status==false)
        {
            Rect pred;
//            pred.x = KF_RectPredicted.x;
//            pred.y = KF_RectPredicted.y;
//            pred.width = KF_RectPredicted.width;
//            pred.height = KF_RectPredicted.height;
            pred = trackWindow;

            checkBoundary(img,pred);

            traj.push_back(Point(pred.x+pred.width/2, pred.y+pred.height));

            if(traj.size()>1)
            {
                for(int i=0; i < traj.size()-1; i++)
                    line(img,traj[i+1],traj[i],Scalar(blue,green,red),2);
            }

            char tempText[100];
            sprintf(tempText,"%d",id);
            putText(img,tempText,Point(pred.x, pred.y),2, 1,Scalar(red,green,blue),2,LINENAME,false);

            float x = pred.x+pred.width/2;
            float y = pred.y+pred.height/2;

            if(pred.x+pred.width/2 > img.cols) x = img.cols;
            if(pred.y+pred.height/2 > img.rows) y = img.rows;

            RotatedRect rPred = RotatedRect(Point2f(x,y), Size2f(pred.width,pred.height), 0);
            ellipse(img, rPred, Scalar(blue,green,red), 2, 8);
        }
    }
}


/*!
  @brief Finds Mean Shift Locations for the agent.
  @param[in] frameData Input image for extracting histogram for MS iterations.
  @param[in] bhatCoefThreshold Threshold used for finalizing MS location.
  @param[in,out] matchVal Final match value after iterations.
*/
void MultiTracker::localizeAppearanceByMeanShift(Mat frameData , float bhatCoefThreshold ,float* matchVal )
{

    // Set Mean Shift Tracker Target
    if(rgbHist) setMeanShiftTrackerTarget( hist, trackWindow );

    if(orientHist)  setMeanShiftTrackerTarget( orient_hist, trackWindow );

    if(fourDimHist) setMeanShiftTrackerTarget( fourDim_hist, trackWindow );

    // Set Current Bhattacharya Coefficient to 1.0
    CurrBhatCoeff = 1.0;

    // Tracking to Localize the Agent Region in Current Pixel Set
    meanShiftTrackingRGB( frameData , bhatCoefThreshold );

    // Check, Whether the Current Bhattacharya Coefficient Exceeds the Threshold
    if( CurrBhatCoeff > bhatCoefThreshold )
    {
        // Set the Final Region with Mean-Shift Tracker Output
        MS_Rect.x = CurrRegion.x;
        MS_Rect.y = CurrRegion.y;
        MS_Rect.width = CurrRegion.width;
        MS_Rect.height = CurrRegion.height;

        checkBoundary(frameData,MS_Rect);

        // Updating Histogram Match Value
        (*matchVal) = CurrBhatCoeff;
    }
    // Otherwise,...
    else
    {
        // Set the Final region as the Initial Region
        MS_Rect.x = trackWindow.x;
        MS_Rect.y = trackWindow.y;
        MS_Rect.width = trackWindow.width;
        MS_Rect.height = trackWindow.height;

        checkBoundary(frameData,MS_Rect);

        // Updating Histogram Match Value
        (*matchVal) = CurrBhatCoeff;
    }
}


/*!
  @brief Set the target hidrogram for MS iterations.
  @param[in] targetDensity Matrix in which target histogram is being stored.
  @param[in] initReg Target location required for extracting target histogram.
*/
void MultiTracker:: setMeanShiftTrackerTarget( Mat targetDensity , Rect initReg )
{

    // Set the Data Ready Status
    MeanShiftTrackerDataReady = true;
    // Set the Target Region
    CurrRegion.x = initReg.x;

    CurrRegion.y = initReg.y;
    CurrRegion.width = initReg.width;
    CurrRegion.height = initReg.height;
    PrevRegion.x = initReg.x;
    PrevRegion.y = initReg.y;
    PrevRegion.width = initReg.width;
    PrevRegion.height = initReg.height;

    // Set the Target Histogram
    copyHistogram( targetDensity , TargetHist);
    copyHistogram( TargetHist , CurrPosHist);
    copyHistogram( TargetHist , PrevPosHist);

    // Set Current Bhattacharya Coefficient to 1.0
    CurrBhatCoeff = 1.0;
}


/*!
  @brief Performs original Convergence of MS location.
  @param[in] frameData Input image for extracting histogram for MS iterations.
  @param[in] bhatCoefThreshold Threshold used during iterations.
*/
void MultiTracker::meanShiftTrackingRGB( Mat frameData , float bhatCoeffThreshold )
{
    // If Scale Changes are Zero
    if( ( XScaleChange < EPSILON ) && ( YScaleChange < EPSILON ) )
    {
        // Only Iterate
        iterateRGB(frameData );

        // Enable/Disable Tracker Based on Current Bhattacharya Coefficient
        if( CurrBhatCoeff < bhatCoeffThreshold )
        {
            ToTrack = false;
        }
    }
    // Otherwise, Invoke Scale Updates According to Desired Update-Iterate Order
    else
    {
        // If Iterate-Update is Preferred
        if( UpdateScaleAfterIterations )
        {
            // Iterate and Update
            iterateRGB(frameData );
            updateScaleRGB(frameData );

            // Disable Tracker Based on Current Bhattacharya Coefficient
            if( CurrBhatCoeff < bhatCoeffThreshold )
            {
                ToTrack = false;
            }
        }
        // Otherwise, Execute in Update-Iterate Order
        else
        {
            // Update and Iterate
            updateScaleRGB(frameData );
            iterateRGB(frameData );

            // Disable Tracker Based on Current Bhattacharya Coefficient
            if( CurrBhatCoeff < bhatCoeffThreshold )
            {
                ToTrack = false;
            }
        }
    }
}


/*!
  @brief Performs MS iterations.
  @param[in] frameData Input image for extracting histogram for MS iterations.
*/
void MultiTracker::iterateRGB( Mat frameData )
{
//    cout << "curr0 rgn" << Mat(CurrRegion.tl()) << Mat(CurrRegion.br()) << endl;

    // Prior to Iterations, Assign Current Region to Previous Region
    PrevRegion.x = CurrRegion.x;
    PrevRegion.y = CurrRegion.y;
    PrevRegion.width = CurrRegion.width;
    PrevRegion.height = CurrRegion.height;

    // Recompute the Position Weighted Color Distribution at Previous Region ( prevRegion )
    // That is Recompute the Previous Position Histogram ( prevPosHist )
    if(rgbHist)
        computeColorDistributionRGB( frameData , PrevRegion , PrevPosHist );

    if(orientHist || fourDimHist)
    {
        MultiTracker tempAgent;
        tempAgent.calcOrientHist(frameData,PrevRegion);

        if(orientHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.orient_hist.at<float>(0,i);
        }

        if(fourDimHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.fourDim_hist.at<float>(0,i);
        }

    }

    // Compute the Bhattacharya Coefficient Between this Histogram and the Target Histogram
    // And Store the Result as Previous Region's Bhattacharya Coefficient ( prevBhatCoeff )
    computeBhattacharyaCoefficient( TargetHist , PrevPosHist , &PrevBhatCoeff );

    // Iterating for Searching Target
    ItrNum = 0;
    float bcMax = -1;
    Rect rectBcMax;
    bool continueIterations = true;
    while( continueIterations && CurrRegion.area() != 0 )
    {
        // Increment Iteration Number
        ItrNum = ( ItrNum ) + 1;

        // Check, Whether Iteration Number Per Frame Exceeds Bound
        if( ItrNum > ItrNumBound )
        {
            // Abnormal Iteration Behavior 1
            ItrCond = 1;
            break;
        }

        // Compute the New Region
//        cout << "currIter rgn" << Mat(CurrRegion.tl()) << Mat(CurrRegion.br()) << endl;

        getNewRegionRGB(frameData , CurrRegion );

        // Recompute the Position Weighted Color Distribution at Current Region ( currRegion )
        // That is Recompute the Current Position Histogram ( currPosHist )
        if(rgbHist)
            computeColorDistributionRGB( frameData , CurrRegion , CurrPosHist );

        if(orientHist || fourDimHist)
        {
            MultiTracker tempAgent1;
            tempAgent1.calcOrientHist(frameData,CurrRegion);

            if(orientHist)
            {
                for(int i=0; i<CurrPosHist.cols; i++)
                    CurrPosHist.at<float>(0,i) = tempAgent1.orient_hist.at<float>(0,i);
            }

            if(fourDimHist)
            {
                for(int i=0; i<CurrPosHist.cols; i++)
                    CurrPosHist.at<float>(0,i) = tempAgent1.fourDim_hist.at<float>(0,i);
            }

        }

        // Compute the Bhattacharya Coefficient Between this Histogram and the Target Histogram
        // And Store the Result as Current Region's Bhattacharya Coefficient ( currBhatCoeff )
        if(rgbHist)
            computeBhattacharyaCoefficient( TargetHist, CurrPosHist, &CurrBhatCoeff );

        if(CurrBhatCoeff > bcMax)
        {
            bcMax = CurrBhatCoeff;
            rectBcMax = CurrRegion;
        }

        // Fine Tuning of Current Region, if Bhattacharya Coefficient is not Maximised
        int abnItr = 0;
        while( CurrBhatCoeff < PrevBhatCoeff )
        {
            // Increment Sub-Iteration Number
            abnItr = abnItr + 1;

            // Check, Whether Sub-Iterations Per Mean Shift Iteration Exceeds Limit
            if( abnItr > ( SubItrNumBound ) )
            {
                // Abnormal Iteration Behavior 2
                ItrCond = 2;
                break;
            }

            // Try with New Current Region as Dictated by Binary Search
            CurrRegion.y = ( ( CurrRegion.y ) + ( PrevRegion.y ) ) / 2;
            CurrRegion.x = ( ( CurrRegion.x ) + ( PrevRegion.x ) ) / 2;

            // Recompute the Position Weighted Color Distribution at Current Region ( currRegion )
            // That is Recompute the Current Position Histogram ( currPosHist )
            if(rgbHist)
                computeColorDistributionRGB( frameData , CurrRegion , CurrPosHist );

            if(orientHist || fourDimHist)
            {
                MultiTracker tempAgent2;
                tempAgent2.calcOrientHist(frameData,CurrRegion);

                if(orientHist)
                {
                    for(int i=0; i<CurrPosHist.cols; i++)
                        CurrPosHist.at<float>(0,i) = tempAgent2.orient_hist.at<float>(0,i);
                }

                if(fourDimHist)
                {
                    for(int i=0; i<CurrPosHist.cols; i++)
                        CurrPosHist.at<float>(0,i) = tempAgent2.fourDim_hist.at<float>(0,i);
                }

            }

            // Compute the Bhattacharya Coefficient Between this Histogram and the Target Histogram
            // And Store the Result as Current Region's Bhattacharya Coefficient ( currBhatCoeff )
            computeBhattacharyaCoefficient( TargetHist, CurrPosHist, &CurrBhatCoeff );
        }

        // Checking the Stopping Criteria
        int regDist = ( ( PrevRegion.x ) - ( CurrRegion.x ) ) * ( ( PrevRegion.x ) - ( CurrRegion.x ) );
        regDist = regDist + ( ( ( PrevRegion.y ) - ( CurrRegion.y ) ) * ( ( PrevRegion.y ) - ( CurrRegion.y ) ) );

        // Terminate, If Stopping Criteria is Satisfied
        if( regDist <= ( Epsilon ) )
        {
            continueIterations = false;
        }

        // Preparing for Next Iteration : Copying the Current Region to Previous Region
        PrevRegion = CurrRegion;

        // Preparing for Next Iteration : Copying the Current Region Histogram to Previous Region Histogram
        //        memcpy( PrevPosHist.row(1) , CurrPosHist.row(1) , ( ( PrevPosHist.cols ) * sizeof( float ) ) );
        for(int i=0; i<PrevPosHist.cols ; i++)
            PrevPosHist.at<float>(0,i) = CurrPosHist.at<float>(0,i);

        // Preparing for Next Iteration : Copying the Current Bhattacharya Coefficient to the Previous One
        PrevBhatCoeff = CurrBhatCoeff;
    }

    //    cout << "iteration toatl \t \t \t" <<  ItrNum << endl;
    if(ItrNum >= ItrNumBound)
    {
        CurrBhatCoeff = bcMax;
        CurrRegion = rectBcMax;
    }

    // Assert Normal Iterations
    ItrCond = 0;
}


/*!
  @brief Performs MS updations for scale during iterations.
  @param[in] frameData Input image for extracting histogram during MS iterations.
*/
void MultiTracker::updateScaleRGB( Mat frameData )
{
    // Create a Smaller Region By Changing the Scale
    float cx = ( (float) CurrRegion.x ) + ( 0.5 *  ( (float) CurrRegion.width ) );
    float cy = ( (float) CurrRegion.y ) + ( 0.5 *  ( (float) CurrRegion.height ) );
    float sx = 0.5 * ( 1.0 - ( 0.01 * ( XScaleChange ) ) ) * ( (float) CurrRegion.width );
    float sy = 0.5 * ( 1.0 - ( 0.01 * ( YScaleChange ) ) ) * ( (float) CurrRegion.height );
    Rect smallRegion = Rect( cvRound( cx - sx ) , cvRound( cy - sy ) , cvRound( sx + sx ) , cvRound( sy + sy ) );

    checkBoundary(frameData, smallRegion);

    // Computing the Bhattacharya Coeefficient by using the Small Region Histogram Stored in Previous Position's Histogram
    if(rgbHist)
        computeColorDistributionRGB( frameData , smallRegion , PrevPosHist );

    if(orientHist || fourDimHist)
    {
        MultiTracker tempAgent;
        tempAgent.calcOrientHist(frameData,smallRegion);

        if(orientHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.orient_hist.at<float>(0,i);
        }

        if(fourDimHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.fourDim_hist.at<float>(0,i);
        }

    }

    float smallBhatCoeff;
    computeBhattacharyaCoefficient( TargetHist, PrevPosHist, &smallBhatCoeff );

    // Create a Bigger Region By Changing the Scale
    // And Store the Result in the Previous Region
    sx = 0.5 * ( 1.0 + ( 0.01 * ( XScaleChange ) ) ) * ( (float) CurrRegion.width );
    sy = 0.5 * ( 1.0 + ( 0.01 * ( YScaleChange ) ) ) * ( (float) CurrRegion.height );
    Rect bigRegion = Rect( cvRound( cx - sx ) , cvRound( cy - sy ) , cvRound( sx + sx ) , cvRound( sy + sy ) );

    checkBoundary(frameData, bigRegion);

    // Computing the Bhattacharya Coeefficient by using the Big Region Histogram Stored in Previous Position's Histogram
    if(rgbHist)
        computeColorDistributionRGB( frameData , bigRegion , PrevPosHist );

    if(orientHist || fourDimHist)
    {
        MultiTracker tempAgent1;
        tempAgent1.calcOrientHist(frameData,bigRegion);

        if(orientHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent1.orient_hist.at<float>(0,i);
        }

        if(fourDimHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent1.fourDim_hist.at<float>(0,i);
        }

    }

    float bigBhatCoeff;
    computeBhattacharyaCoefficient( TargetHist, PrevPosHist, &bigBhatCoeff );

    // IF Scaling Down is Better...
    if( ( smallBhatCoeff > ( CurrBhatCoeff ) ) && ( smallBhatCoeff > bigBhatCoeff ) )
    {
        // Adopt Small Region as Current Region
        CurrRegion.x = smallRegion.x;
        CurrRegion.y = smallRegion.y;
        CurrRegion.width = smallRegion.width;
        CurrRegion.height = smallRegion.height;

        checkBoundary(frameData, CurrRegion);

        // Adopt Smaller Region's Bhattacharya Coefficient as Current Region Bhattacharya Coefficient
        CurrBhatCoeff = smallBhatCoeff;
    }

    // IF Scaling Up is Better...
    if( ( bigBhatCoeff > ( CurrBhatCoeff ) ) && ( bigBhatCoeff > smallBhatCoeff ) )
    {
        // Adopt Big Region as Current Region
        CurrRegion.x = bigRegion.x;
        CurrRegion.y = bigRegion.y;
        CurrRegion.width = bigRegion.width;
        CurrRegion.height = bigRegion.height;

        checkBoundary(frameData,CurrRegion);

        // Adopt Smaller Region's Bhattacharya Coefficient as Current Region Bhattacharya Coefficient
        CurrBhatCoeff = bigBhatCoeff;
    }
}



/*!
  @brief Copies histogram from source to destination.
  @param[in] srcHist source Histogram.
  @param[in,out] desHist destination Histogram.
*/
void MultiTracker::copyHistogram( Mat srcHist ,Mat destHist )
{
    for(int i=0; i<srcHist.cols ; i++)
    {
        destHist.at<float>(0,i) = srcHist.at<float>(0,i);
    }
}


/*!
  @brief Finding new Region for next iteration.
  @param[in] frameData Image used for finding new location.
  @param[in,out] newRegion Location for the next iteration.
*/
void MultiTracker::getNewRegionRGB( Mat frameData , Rect&  newRegion )
{
    // Computing Mean Shift Vector
//    cout << "prev rgn" << Mat(PrevRegion.tl()) << Mat(PrevRegion.br()) << endl;
    float sumWX = 0.0 , sumWY = 0.0 , sumW = 0.0;
    int rightBotX = ( PrevRegion.x ) + ( PrevRegion.width );
    int rightBotY = ( PrevRegion.y ) + ( PrevRegion.height );
    float sx = 0.5 * ( PrevRegion.width );
    float sy = 0.5 * ( PrevRegion.height );
    float cx = ( PrevRegion.x ) + sx;
    float cy = ( PrevRegion.y ) + sy;

    Mat tempGradients, tempOrientations;    //calculating gradients and orientations for histogram calculation for a window
    //    cout << Mat(PrevRegion.tl()) << Mat(PrevRegion.br()) << endl;
    gradImage(frameData, PrevRegion, tempGradients, tempOrientations);

    int tempbins = HIST_BIN_SIZE;

    float invBinSize = ( (float) tempbins ) / 256.0;

    for( int y = ( PrevRegion.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( PrevRegion.x ) ; x < rightBotX ; ++x )
        {
            int histIndx;

            if(rgbHist)
            {
                Vec3b color = frameData.at< Vec3b >(y,x);

                // Compute the Bin
                int redBin = (int) (invBinSize * ( (float) color[2]));
                int greenBin = (int) (invBinSize * ( (float) color[1]));
                int blueBin = (int) (invBinSize * ( (float) color[0]));

                histIndx = (redBin * pow(tempbins,2) ) + ( greenBin * pow(tempbins,1) ) + (blueBin) ;
            }

            if(orientHist)
            {
                Vec3f orientBin  = tempOrientations.at<Vec3f> (y-PrevRegion.y, x-PrevRegion.x);
                histIndx = (orientBin[2] * pow(orientbins,2) ) + ( orientBin[1]* pow(orientbins,1) ) + (orientBin[0]) ;
            }

            if(fourDimHist)
            {
                Vec3b color = frameData.at< Vec3b >(y,x);
                Vec3f orientBin  = tempOrientations.at<Vec3f> (y-PrevRegion.y, x-PrevRegion.x);

                int max;

                if(orientBin[0]>=orientBin[1] && orientBin[0]>=orientBin[2]) max = orientBin[0];
                if(orientBin[1]>=orientBin[0] && orientBin[1]>=orientBin[2]) max = orientBin[1];
                if(orientBin[2]>=orientBin[0] && orientBin[2]>=orientBin[1]) max = orientBin[2];

                // Compute the Bin
                int redBin = (int) (invBinSize * ( (float) color[2]));
                int greenBin = (int) (invBinSize * ( (float) color[1]));
                int blueBin = (int) (invBinSize * ( (float) color[0]));

                histIndx = (redBin * pow(tempbins,2) ) + ( greenBin * pow(tempbins,1) ) /*+ (blueBin*pow(tempbins,1)) */+ max ;
            }

            double weightVal = 0;
            if(PrevPosHist.at<float>(0,histIndx) != 0)
                weightVal = sqrt( ( TargetHist.at<float>(0,histIndx )) / ( PrevPosHist.at<float>(0,histIndx) ) );

            //           cout << TargetHist.at<float>(0,histIndx ) <<"\t" << PrevPosHist.at<float>(0,histIndx) << endl;
            sumWY = sumWY + ( y * weightVal );
            sumWX = sumWX + ( x * weightVal );
            sumW = sumW + weightVal;
        }
    }

    cx = sumWX / sumW;
    cy = sumWY / sumW;
    newRegion.x = cvRound( cx - sx );
    newRegion.y = cvRound( cy - sy );
    //    newRegion.width = PrevRegion.width;
    //    newRegion.height = PrevRegion.height;

    checkBoundary(frameData,newRegion);
}


/*!
  @brief Finds colour distribution of given location in the image.
  @param[in] frameData Input image for histogram extraction.
  @param[in] reg rectangle to location in the image.
  @param[in] hist Histogram to be calculated for the specified rectangle in the image.
*/
void getHist(Mat frameData, Rect reg, Mat& hist, int bins)
{
    if(reg.area() == 0)
    {
        reg.x = 0;
        reg.y = 0;
        reg.width = frameData.cols;
        reg.height = frameData.rows;
    }
//    reg.x += cvRound(reg.width*0.2);
//    reg.width = cvRound(reg.width*0.6);
//    reg.y += cvRound(reg.height*0.1);
//    reg.height = cvRound(reg.height*0.8);

    hist = Mat(1, pow(bins,3), CV_32FC1, Scalar(0));

    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            Vec3b color = frameData.at< Vec3b >(y,x);

            // Compute the Bin
            int redBin = (int) (invBinSize * ( (unsigned char) color[2]));
            int greenBin = (int) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (int) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = (redBin * binSqr ) + ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > EPSILON )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}

/*!
  @brief Validates rectangle and fixes its dimensions inside the image.
  @param[in] frameData Image used for fixing rectangle dimensions.
  @param[in,out] tempRect Rectangle whose dimensions are to be fixed.
*/
void checkBoundary(Mat frameData, Rect& tempRect)
{
    if(tempRect.x < 0)
    {
        tempRect.width  =  tempRect.width + tempRect.x ;
        tempRect.x =0 ;
        if(tempRect.width < 0)
        {
            tempRect.width = 0;
            tempRect.height = 0;
        }
    }

    if(tempRect.y < 0)
    {
        tempRect.height =  tempRect.height + tempRect.y ;
        tempRect.y = 0;
        if(tempRect.height < 0)
        {
            tempRect.height = 0;
            tempRect.width = 0;
        }
    }

    if(tempRect.x > frameData.cols) tempRect.x = frameData.cols ;
    if(tempRect.y > frameData.rows) tempRect.y = frameData.rows;

    int rightBotX = tempRect.x + tempRect.width;
    int rightBotY = tempRect.y + tempRect.height;

    if(rightBotY  >  frameData.rows )  tempRect.height  =  frameData.rows - tempRect.y ;
    if(rightBotX  >  frameData.cols)   tempRect.width  =  frameData.cols - tempRect.x ;
}


/*!
  @brief Calculate overlap between twoo rectangles.
  @param[in] tl1 Topleft point of first rectangle.
  @param[in] tl2 Topleft point of second rectangle.
  @param[in] sz1 Size of first rectangle.
  @param[in] sz2 Size of second rectangle.
  @param[in,out] overlap Overlap calculated between two rectangles.
*/
void calcOverlap(Point tl1, Point tl2, Size sz1, Size sz2, float *overlap)
{
    Rect roi;

    int x_tl = max(tl1.x, tl2.x);
    int y_tl = max(tl1.y, tl2.y);
    int x_br = min(tl1.x + sz1.width, tl2.x + sz2.width);
    int y_br = min(tl1.y + sz1.height, tl2.y + sz2.height);
    if (x_tl < x_br && y_tl < y_br)
        roi = Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
    else
        roi = Rect(0,0,0,0);

    *overlap = roi.width*roi.height;
}

/*!
  @brief Small version of above function.
  */
float findOverlap(Rect r1, Rect r2)
{
    float overlap = 0;
    Point tl1 = r1.tl();
    Point tl2 = r2.tl();
    Size sz1 =r1.size();
    Size sz2 = r2.size();

    Rect roi;

    int x_tl = max(tl1.x, tl2.x);
    int y_tl = max(tl1.y, tl2.y);
    int x_br = min(tl1.x + sz1.width, tl2.x + sz2.width);
    int y_br = min(tl1.y + sz1.height, tl2.y + sz2.height);
    if (x_tl < x_br && y_tl < y_br)
        roi = Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
    else
        roi = Rect(0,0,0,0);

    overlap = roi.width*roi.height;

    return overlap;
}

/*!
  @brief Performs BIP operation provided the matrix.
  @param[in,out] reasonMat Matrix provided for solving through BIP.
*/
void getBIPsolution(Mat& reasonMat )
{
    ofstream mpsFile("./bip.mps");

    mpsFile << "NAME          BIP" << endl;
    mpsFile << "ROWS" << endl;
    mpsFile << " N  COST" << endl;

    int numRows = 0;
    Mat variableIndicies1 = reasonMat.clone();
    Mat variableIndicies2 = reasonMat.clone();
    // Get number of contraint equations (ROWS)
    for(int i=0; i<reasonMat.rows; i++)
    {
        if(countNonZero(reasonMat.row(i)) == 0)
            continue;
        else
        {
            for(int j=0; j<reasonMat.cols; j++)
            {
                variableIndicies1.at<float>(i,j) = numRows;
            }
            numRows++;
        }
    }

    for(int i=0; i<reasonMat.cols; i++)
    {
        if(countNonZero(reasonMat.col(i)) == 0)
            continue;
        else
        {
            for(int j=0; j<reasonMat.rows; j++)
            {
                variableIndicies2.at<float>(j,i) = numRows;
            }
            numRows++;
        }
    }

    //    cout << "Num of rows = " << numRows << endl;
    // ROWS

    for(int i=0; i<numRows; i++)
    {
        mpsFile << " L  ";
        char txt[100];
        sprintf(txt,"R%d",i+10001);
        mpsFile << txt << endl;
    }


    // Generate cost values
    vector<float> costVal;
    vector< vector<int> > rowIndex;
    for(int i=0; i<reasonMat.rows; i++)
        for(int j=0; j<reasonMat.cols; j++)
        {
            if(reasonMat.at<float>(i,j) != 0)
            {
                costVal.push_back(reasonMat.at<float>(i,j));
                vector<int> temp;
                temp.push_back(variableIndicies1.at<float>(i,j));
                temp.push_back(variableIndicies2.at<float>(i,j));
                rowIndex.push_back(temp);
            }
        }


    // COLUMNS

    mpsFile << "COLUMNS" << endl;

    int numCols = countNonZero(reasonMat);
    assert(numCols == costVal.size());

    //    cout << "Num of cols = " << numCols << endl;
    for(int i=0; i<numCols; i++)
    {
        mpsFile << "    ";

        char txt[100];
        sprintf(txt,"C%d",i+1001);
        mpsFile << txt;

        mpsFile << "     COST";

        mpsFile << "      ";
        mpsFile << costVal[i] << endl;

        for(int j=0; j<2; j++)
        {

            mpsFile << "    ";

            char txt[100];
            sprintf(txt,"C%d",i+1001);
            mpsFile << txt;

            mpsFile << "     ";
            sprintf(txt,"R%d",rowIndex[i][j]+10001);
            mpsFile << txt;

            mpsFile << "    ";
            mpsFile << 1 << endl;

        }
    }


    // RHS

    mpsFile << "RHS" << endl;

    for(int i=0; i<numRows; i++)
    {
        mpsFile << "    RHS1";

        mpsFile << "      ";
        char txt[100];
        sprintf(txt,"R%d",i+10001);
        mpsFile << txt;
        mpsFile << "    ";
        mpsFile << 1 << endl;
    }


    // BOUNDS

    mpsFile << "BOUNDS" << endl;

    for(int k=0; k<numCols; k++)
    {
        mpsFile << " UI ONE";
        mpsFile << "       ";
        char txt[100];
        sprintf(txt,"C%d",k+1001);
        mpsFile << txt;
        mpsFile << "     1";
        mpsFile << endl;
    }

    mpsFile << "ENDATA" <<endl;


    // System Call to the CBC executable
    system("cbc ./bip.mps -maximize -seconds 60 solve solu ./bipSolution.txt >> temp.txt");


    // Read the solution pof BIP

    ifstream readSol("./bipSolution.txt");

    vector<int> solution;

    int loopCounter=0;
    while(!readSol.eof())
    {
        string data1;

        getline(readSol, data1);

        stringstream strData1(data1);

        string dump;
        int index;
        int val;

        if(loopCounter<1)
        {
            strData1 >> dump;
            loopCounter++;
            continue;
        }
        else
        {
            strData1 >> index >> dump >> val >> dump;
        }

        loopCounter++;

        solution.push_back(val);
    }

    int counter = 0;
    for(int i=0; i<reasonMat.rows; i++)
        for(int j=0; j<reasonMat.cols; j++)
        {
            if(reasonMat.at<float>(i,j) != 0)
            {
                reasonMat.at<float>(i,j) = solution[counter];
                counter++;
            }
        }
}


bool verifyResultantRoi(cv::Rect r1, cv::Rect r2, float& ARerr, float& scale, float& roiConf, bool rotateFlag = false)
{
    float ARnow = 0;
    scale = 0;

    if(!rotateFlag)
    {
        ARnow = float(r2.width) / float(r2.height);
        scale = float(r2.width) / float(r1.width);
    }
    else
    {
        ARnow = float(r2.height) / float(r2.width);
        scale = float(r2.height) / float(r1.width);
    }

    float AR = float(r1.width) / float(r1.height);

    ARerr = AR/ARnow;

    //    cout << "err\t" << ARerr << "\t" << scale << endl;
//    if(ARerr > 1.3 || ARerr < 0.7)
//    {
//        return false;
//    }

//    if(scale < 0.67 || scale > 1.5 )
//    {
//        return false;
//    }


    float scaleConf = float(r1.area())/float(r2.area());
    scaleConf = scaleConf > 1 ? 1.0/scaleConf : scaleConf;

    float ARConf = ARerr > 1 ? 1.0/ARerr : ARerr;

//    cout << ARnow << "\t" << ARerr << "\t" <<scale << endl;
//    cout << scaleConf << "\t" << ARConf << endl;
    if(scaleConf > 0.7 && ARConf > 0.7)
    {
        roiConf = 0.5 * (scaleConf + ARConf);
        return true;
    }
    else
        return false;
}


void crossCheckMatching( Ptr<DescriptorMatcher>& descriptorMatcher,
                         const Mat& descriptors1, const Mat& descriptors2,
                         vector<DMatch>& filteredMatches12, int knn=1 )
{
    filteredMatches12.clear();

    vector<vector<DMatch> > matches12, matches21;
    descriptorMatcher->knnMatch( descriptors1, descriptors2, matches12, knn );
    descriptorMatcher->knnMatch( descriptors2, descriptors1, matches21, knn );
    for( size_t m = 0; m < matches12.size(); m++ )
    {
        bool findCrossCheck = false;
        for( size_t fk = 0; fk < matches12[m].size(); fk++ )
        {
            DMatch forward = matches12[m][fk];

            for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
            {
                DMatch backward = matches21[forward.trainIdx][bk];
                if( backward.trainIdx == forward.queryIdx )
                {
                    filteredMatches12.push_back(forward);
                    findCrossCheck = true;
                    break;
                }
            }
            if( findCrossCheck ) break;
        }
    }
}



double surfDesc_Matching1(Mat img1, Mat img2,  vector<KeyPoint> temp1Keypoint, vector<KeyPoint> temp2Keypoint,
                          Mat temp1Desc, Mat temp2Desc, Point2f& centerSURF, cv::Rect& roiOut, Mat& drawImg)
{
    if(temp1Desc.empty() || temp2Desc.empty())
    {
//        cerr << "Descriptors size is zero" << endl;
        return 0;
    }

    vector<int> queryIdxs,  trainIdxs;
    vector<DMatch> filteredMatches;

    Ptr<DescriptorMatcher> descriptorMatcher;
    descriptorMatcher = DescriptorMatcher::create( "FlannBased" );


    crossCheckMatching( descriptorMatcher, temp1Desc, temp2Desc, filteredMatches, 1 );

    for( size_t i = 0; i < filteredMatches.size(); i++ )
    {
        queryIdxs.push_back(filteredMatches[i].queryIdx);
        trainIdxs.push_back(filteredMatches[i].trainIdx);
    }

    Mat pointsTransed; // Points for inverse homography
    Mat H12;

    if( RANSAC_THREHOLD >= 0 )
    {
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp2Keypoint, points2, trainIdxs);
        if (points2.size() < 4 )
            return 0;
        H12 = findHomography( Mat(points1), Mat(points2), cv::RANSAC, RANSAC_THREHOLD );
    }

    //    Mat drawImg;

    double percentage_match = 0.0;

    if( !H12.empty() )
    {
        vector<char> matchesMask( filteredMatches.size(), 0 );
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp2Keypoint, points2, trainIdxs);

        Mat points1t; perspectiveTransform(Mat(points1), points1t, H12);


        int count = 0;
        for( size_t i1 = 0; i1 < points1.size(); i1++ )
        {
            if( ( norm(points2[i1] - points1t.at<Point2f>((int)i1,0)) <= 5 ))
            {
                count++;
                matchesMask[i1] = 1;
            }
        }

        vector<Point2f> centerVec;
        Mat centerVecOut;
        centerVec.push_back(centerSURF);
        centerVec.push_back(cv::Point2f(0,0));
        centerVec.push_back(cv::Point2f(img1.cols,img1.rows));

        perspectiveTransform((Mat)centerVec, centerVecOut, H12);

        centerSURF = centerVecOut.at<Point2f>(0);
        roiOut.x = centerVecOut.at<cv::Point2f>(1).x;
        roiOut.y = centerVecOut.at<cv::Point2f>(1).y;
        roiOut.width = abs(centerVecOut.at<cv::Point2f>(2).x - centerVecOut.at<cv::Point2f>(1).x);
        roiOut.height = abs(centerVecOut.at<cv::Point2f>(2).y - centerVecOut.at<cv::Point2f>(1).y);

        percentage_match = (double)(count*100)/(temp2Desc.rows+temp1Desc.rows-count);
        points1.clear();points2.clear();
        drawMatches( img1, temp1Keypoint, img2, temp2Keypoint, filteredMatches, drawImg, Scalar(0, 255, 0), Scalar(0, 0, 255), matchesMask);

//    namedWindow("cores",WINDOW_NORMAL);
//    imshow("cores",drawImg);
//    waitKey(0);
    }

    return percentage_match;

}


int resolveTargetSurf(Mat imgSrc, Mat imgTarget, vector<Rect>& roiTargetCandidates, Rect& roiOut, float& roiConf, int& descNum2, bool& testOK)
{
    vector<Rect> targetRects;
    Mat img1 = imgSrc.clone();

    int imgArea1 = img1.rows * img1.cols;
    int heshThresh1 = 0;
//    heshThresh1 = imgArea1 < 10000 ? 0 : float(imgArea1)/300.0;
#if(OPENCV3)
    Ptr<SURF> mySurf = SURF::create(heshThresh1);
#else
    SURF* mySurf = new SURF;
    mySurf->set("hessianThreshold",heshThresh1);
#endif

    vector<KeyPoint> kp1;
    mySurf->detect(img1,kp1);
    Mat desc1;
    mySurf->compute(img1,kp1,desc1);


    vector<double> percentMatches;
    float maxMatch = 0;
    int maxMatchIndex = -1;

    for(int k=0; k<roiTargetCandidates.size(); k++)
    {
        Mat img2 = imgTarget(roiTargetCandidates[k]).clone();

        vector<KeyPoint> kp2;
        Mat desc2;
        int imgArea2 = img2.rows * img2.cols;
        int heshThresh2 = 0;
//        heshThresh2 = imgArea2 < 10000 ? 0 : float(imgArea2)/300.0;
#if(OPENCV3)
        mySurf->setHessianThreshold(heshThresh2);
#else
        mySurf->set("hessianThreshold",heshThresh2);
#endif
        mySurf->detect(img2,kp2);
        mySurf->compute(img2,kp2,desc2);

        descNum2 = desc2.rows;
        Point2f center;
        center.x = roiTargetCandidates[k].x + roiTargetCandidates[k].width/2;
        center.y = roiTargetCandidates[k].y + roiTargetCandidates[k].height/2;

        Mat imgDraw;
        roiOut;
        double percentMatch = surfDesc_Matching1(img2,img1,kp2,kp1,desc2,desc1,center,roiOut,imgDraw);

        Rect roiIn = Rect(0,0,img1.cols,img1.rows);
        float ARerr=0,scale=0;
        testOK = verifyResultantRoi(roiIn,roiOut,ARerr,scale,roiConf);

        if(percentMatch > maxMatch && testOK)
        {
            maxMatch = percentMatch;
            maxMatchIndex = k;
        }

        Rect targetRect(0,0,0,0);

        if(percentMatch > 20 && testOK)
        {
            targetRect.width = roiTargetCandidates[k].width;
            targetRect.height = roiTargetCandidates[k].height;
            targetRect.x = center.x - targetRect.width/2 ;//+ roiSrc.x;
            targetRect.y = center.y - targetRect.height/2 ;//+ roiSrc.y;
        }

        percentMatches.push_back(percentMatch);
        targetRects.push_back(targetRect);

//        cout << "percent match \t" << percentMatch << "\t" << testOK << endl;

        //            char textString[100];
        //            sprintf(textString,"%d.jpg",k+1);
        //            imwrite(textString,imgDraw);
    }

    roiTargetCandidates.clear();
    for(int k=0; k<targetRects.size(); k++)
        roiTargetCandidates.push_back(targetRects[k]);

    return maxMatchIndex;
}


Mat findTransform(Mat img1, Mat img2, Rect roi)
{
    Mat status,err,img1G,img2G;
    vector<Point2f> points[2];
    Size subPixWinSize(10,10), winSize(31,31);
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);

    //    Rect roi(0,0,250,img1.rows);
    img1 = img1(roi).clone();
    cvtColor(img1,img1G,CV_BGR2GRAY);
    img2 = img2(roi).clone();
    cvtColor(img2,img2G,CV_BGR2GRAY);

    cv::goodFeaturesToTrack(img1G,points[0],100,0.01,10.0,Mat(),3,false,0.04);
    cv::goodFeaturesToTrack(img2G,points[1],100,0.01,10.0,Mat(),3,false,0.04);

    cv::calcOpticalFlowPyrLK(img1G,img2G,points[0],points[1],status,err,winSize,3,termcrit,0,0.001);

    Mat corresImg(img1.rows,img1.cols*2,CV_8UC3,Scalar::all(0));
    img1.copyTo(corresImg.colRange(0,img1.cols));
    img2.copyTo(corresImg.colRange(img1.cols,corresImg.cols));


    for(int k=0; k<points[0].size(); k++)
    {
        if(status.at<uchar>(k) != 1)
            continue;
        circle(corresImg,points[0][k],1,Scalar(0,0,255),2);
        Point2f pt2 ;
        pt2.x = points[1][k].x + img1.cols;
        pt2.y = points[1][k].y;
        circle(corresImg,pt2,1,Scalar(0,255,0),2);


        line(corresImg,points[0][k],pt2,Scalar(255,0,0),1,LINENAME);
    }

//        imshow("123",corresImg);
//        waitKey(0);

    Mat transform = findHomography(Mat(points[0]),Mat(points[1]),cv::RANSAC);
    //    cout << transform << endl;
    return transform;
}



void getImageParts(Mat img1, vector<Rect> detections, vector<Rect>& scanRegions)
{
    scanRegions.clear();

    if(detections.empty())
    {
        Rect region = Rect(0,0,img1.cols,img1.rows);

        scanRegions.push_back(region);
    }
    else
    {
        vector<int> rectX;
        for(int k=0; k<detections.size(); k++)
        {
            rectX.push_back(detections[k].x);
            //        rectangle(img1,detections[k],Scalar(0,255,0),2,LINENAME);
        }

        vector<int> sortedIdx;
        sortIdx(rectX,sortedIdx,0);


        for(int k=0; k<detections.size()+1; k++)
        {
            int startX = (k == 0 ? 0 : detections[sortedIdx[k-1]].br().x);
            int endX = (k == detections.size() ? img1.cols : detections[sortedIdx[k]].x);


            int width = endX - startX;

            if(width <= 0)
                continue;

            Rect region = Rect(startX,0,width,img1.rows);

            scanRegions.push_back(region);
        }

        for(int k=0; k<scanRegions.size(); k++)
        {
            //        rectangle(img1,scanRegions[k],Scalar(0,0,255),2,LINENAME);
        }


//            imshow("imgParts",img1);
//            waitKey(0);
    }
}

/*!
  @brief Finds a random color for a given seed, defualt seed id current timestamp in nanoseconds.
  @param[in]    Input seed for random number generator.
  @return       Random Scalar color value.
  */
Scalar getRandColor(/*uint64_t seed = ros::Time::now().toNSec()*/)
{
    timespec t1;
    clock_gettime(0,&t1);
    RNG randNum(t1.tv_nsec);
//    RNG randNum(seed);
    Scalar randColor;

    int t = randNum.uniform(0,256);
    for(int k=0; k<3; k++)
    {
        t = randNum.uniform(0,256);
        randColor[k] = t;
    }

    return randColor;
}

/*!
  @brief Finds the delaunay triangulation and then Euclidean Minimum Spanning Tree using Prim's algorithm. Marks the graph on image.
  @param[in]    img         Image to draw the graph.
  @param[in]    points1     Input points on which the graph/tree has to made.
  @param[out]   adjMat      Adjacency Matrix for the graph. Value is the index of the edge. -1 if no edge exists for the pair.
  @param[out]   nbrList     List of neighbours for the given points in form of a vector. It needs to be inititated with points size.
  @param[in]    EMST        Set to true if Euclidean Minimum Spanning Tree is required, else Delaunay Trainagulation is done.
  */
void draw_subdiv( Mat img, vector<Point2f> points1, Mat_<int>& adjMat, vector< vector<int> >& nbrList, bool EMST = false )
{
    if(points1.size() < 2)
    {
//        cerr << "Not enough points for drawing Delaunay Traingualation" << endl;
        return;
    }

    int counter = 0;

    adjMat = Mat_<int>(points1.size(),points1.size(),-1);

    Rect rect = Rect(0,0,img.cols*1.2,img.rows*1.2);
    Subdiv2D subdiv = Subdiv2D(rect);

    subdiv.insert(points1);

#if 0
    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    vector<Point> pt(3);

    for( size_t i = 0; i < triangleList.size(); i++ )
    {
        Vec6f t = triangleList[i];
        pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = Point(cvRound(t[4]), cvRound(t[5]));
        line(img, pt[0], pt[1], delaunay_color, 1, LINENAME, 0);
        line(img, pt[1], pt[2], delaunay_color, 1, LINENAME, 0);
        line(img, pt[2], pt[0], delaunay_color, 1, LINENAME, 0);
    }
#else
    vector<Vec4f> edgeList;
    vector<float> edgeLength;
    vector<Point2f> edgePoints;
    subdiv.getEdgeList(edgeList);
    for( size_t i = 0; i < edgeList.size(); i++ )
    {
        Vec4f e = edgeList[i];
        Point pt0 = Point(cvRound(e[0]), cvRound(e[1]));
        Point pt1 = Point(cvRound(e[2]), cvRound(e[3]));
        if(e[0] < 0 || e[0] > img.cols || e[1] < 0 || e[1] > img.rows || e[2] < 0 || e[2] > img.cols || e[3] < 0 || e[3] > img.rows)
            continue;
        //        if(norm(pt0-pt1) < 50)
        edgeLength.push_back(norm(pt0-pt1));
        edgePoints.push_back(Point2f(e[0],e[1]));
        edgePoints.push_back(Point2f(e[2],e[3]));

        if(!EMST)
        {
            int vtx1, vtx2, eg;
            subdiv.locate(pt0,eg,vtx1);
            subdiv.locate(pt1,eg,vtx2);

            adjMat(vtx1-4, vtx2-4) = counter;
            adjMat(vtx2-4, vtx1-4) = counter;

            nbrList[vtx1-4].push_back(vtx2-4);
            nbrList[vtx2-4].push_back(vtx1-4);

            counter++;

            line(img, pt0, pt1, Scalar(255,255,255), 2, LINENAME, 0);
        }
    }
#endif


    if(EMST)
    {
        vector<int> sortedEdgeIdx;
        sortIdx(edgeLength,sortedEdgeIdx,CV_SORT_ASCENDING);

        //**********************************************************************************************************
        // Prim's Algorithm for EMST
        vector< vector< int > > vertexEdges(points1.size());
        vector< vector< float > > vertexEdgeLength(points1.size());

        // Store all the Delaunay neoghbors in a sorted order along with the corresponding edgeLengths
        for(uint i=0; i<edgeLength.size(); i++)
        {
            int vtx1, vtx2, eg;
            subdiv.locate(edgePoints[2*sortedEdgeIdx[i]],eg,vtx1);
            subdiv.locate(edgePoints[2*sortedEdgeIdx[i]+1],eg,vtx2);

            vertexEdges[vtx1-4].push_back(vtx2-4);
            vertexEdges[vtx2-4].push_back(vtx1-4);

            vertexEdgeLength[vtx1-4].push_back(edgeLength[sortedEdgeIdx[i]]);
            vertexEdgeLength[vtx2-4].push_back(edgeLength[sortedEdgeIdx[i]]);
        }

        // Stores the vertices that have been parsed
        // and search for a minimum weight edge is done in the neighbors of these only
        vector<int> vertexCounter;
        vertexCounter.push_back(0); // Choose a vertex arbitrarily


        // Mark the vertices that have been parsed
        vector<int> vertexDone(points1.size(),0);
        vertexDone[0] = 1;

        while(vertexCounter.size() < points1.size())
        {
            float minVal = 9999;
            Point2i minValIdx;

            // Loop over all the traversed vertices
            for(uint j=0; j<vertexCounter.size(); j++)
            {
                // Loop over all the neighbors of the traversed vertex
                for(uint k=0; k<vertexEdges[vertexCounter[j]].size(); k++)
                {
                    if(!vertexDone[vertexEdges[vertexCounter[j]][k]])
                    {
                        if(vertexEdgeLength[vertexCounter[j]][k] < minVal)
                        {
                            minVal = vertexEdgeLength[vertexCounter[j]][k];
                            minValIdx = Point(vertexEdges[vertexCounter[j]][k], vertexCounter[j]);
                        }
                        break;
                    }
                }
            }

            vertexCounter.push_back(minValIdx.x);
            vertexDone[minValIdx.x] = 1;

            adjMat(minValIdx.y, minValIdx.x) = counter;
            adjMat(minValIdx.x, minValIdx.y) = counter;

            nbrList[minValIdx.y].push_back(minValIdx.x);
            nbrList[minValIdx.x].push_back(minValIdx.y);

            counter ++;

            line(img,points1[minValIdx.x], points1[minValIdx.y], Scalar(0,0,255), 2, LINENAME);

        }
    }
    //**********************************************************************************************************
}


/*!
  @brief Adaptive K-Means clustering of the data.
  @param[in]    input               Input Samples to be clustered
  @param[in]    threshold_radius    Radius Threshold for clustering
  @param[out]   clusterCenters      Cluster Centers.
  @param[out]   labels              Cluster labels for the input data
  @param[out]   clusterData         Clustered data stored in form of a vector.
  */
void adaptiveKmeans(Mat input, int threshold_radius, Mat& clusterCenters, Mat& labels, vector< vector<Mat> >& clusterData)
{
    int noc=0;

    // For all the points in the sample set
    for(unsigned int iter=0;iter<input.rows;iter++)
    {
        int label=0;

        Mat p1 = input.row(iter);

        float minDistance=999;

        // Find minimum distance of the given point from the available cluster centers
        for(int j=0; j<noc; j++)
        {

            Mat p2 = clusterCenters.row(j);

            float distance = norm(p1-p2);

            if(distance<minDistance)
            {
                minDistance=distance;
                label=j;
            }

        }

        // If the minimum distance is in range of the radius threshold
        if(minDistance<threshold_radius)
        {
            // Update the data
            labels.push_back(label);
            clusterData[label].push_back(p1);

            int clusterMemberCount = clusterData[label].size();

            Mat sumVal = Mat::zeros(1,p1.cols,CV_32FC1);

            for(int k1=0; k1<clusterMemberCount; k1++)
            {
                sumVal += clusterData[label][k1];
            }

            sumVal /= clusterMemberCount;

            clusterCenters.row(label) = sumVal.clone();
        }
        // Else create a new cluster
        else
        {
            noc++;
            labels.push_back(noc);

            vector<Mat> temp;
            temp.push_back(p1);
            clusterData.push_back(temp);

            clusterCenters.push_back(p1);
        }
    }
}

/*!
  @brief Image segmentation based on K-Means using color as well as pixel position.
  @param[in]    segmentImg  Image to be segmented
  @param[in]    K           Number of clusters to be formed
  @param[out]   imgLabels   Labels correspoding to pixels of the image.
  @param[out]   imgMeans    Cluster Centers.
  @param[out]   outImg      Output clustered image with each region marked as mean color value.
  */
void clusterImgWithPos(Mat segmentImg, int K, vector<Point2f>& clusterCenters, vector<Vec3b>& colors, Mat& imgMeans, Mat& outImg)
{
    Rect roi1(0,0,segmentImg.cols,segmentImg.rows);

    medianBlur(segmentImg,segmentImg,5);

    Mat samples;
    Mat imgColor = segmentImg.clone();

//    cvtColor(segmentImg,segmentImg,CV_BGR2HSV);

    for(int j=0; j<segmentImg.cols; j++)
    {
        for(int k=0; k<segmentImg.rows; k++)
        {
            Mat temp(1,5,CV_64FC1);
            temp.at<double>(0,0) = 100*double(segmentImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(segmentImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(segmentImg.at<Vec3b>(k,j)[2])/255.0;
            temp.at<double>(0,3) = 100*double(j)/roi1.width;
            temp.at<double>(0,4) = 100*double(k)/roi1.height;

            samples.push_back(temp.row(0));
        }
    }

    samples.convertTo(samples,CV_32FC1);
    Mat imglabels;
    kmeans(samples,K,imglabels,TermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 300, 0.1 ), 1,KMEANS_RANDOM_CENTERS,imgMeans);

//    vector< vector<Mat> > clDa;
//    adaptiveKmeans(samples,75,imgMeans,imglabels,clDa);

    vector<Mat> clusteredImages(K);

    for(int k=0; k<imgMeans.rows; k++)
    {
        Vec3b color1;
        color1[0] = uchar(2.55 * imgMeans.at<float>(k,0));
        color1[1] = uchar(2.55 * imgMeans.at<float>(k,1));
        color1[2] = uchar(2.55 * imgMeans.at<float>(k,2));

        colors.push_back(color1);

        clusteredImages[k] = Mat::zeros(segmentImg.rows,segmentImg.cols,CV_8UC3);
    }

    int counter = 0;
    for(int j=0; j<imgColor.cols; j++)
    {
        for(int i=0; i<imgColor.rows; i++)
        {
            int label = imglabels.at<int>(counter);
            imgColor.at<Vec3b>(i,j) = colors[label];
            counter++;
            clusteredImages[label].at<Vec3b>(i,j) = colors[label];
        }
    }

    for(int k=0; k<imgMeans.rows; k++)
    {
        Point2f pt1;
        pt1.x = roi1.width * imgMeans.at<float>(k,3) / 100;
        pt1.y = roi1.height * imgMeans.at<float>(k,4) / 100;
        char text[100];
        sprintf(text,"%d",k);
//        putText(imgColor,text,pt1,1,1,Scalar(0,0,0),2);

        clusterCenters.push_back(pt1);
    }

    Mat_<int> adjMat(clusterCenters.size(), clusterCenters.size(), -1);
    vector< vector<int> > nbrList(clusterCenters.size());
    draw_subdiv(imgColor,clusterCenters,adjMat,nbrList);

    // Commented part below is for background subtraction

//    int diff = 5;

//    Mat imgColor3 = imgColor.clone();

//    Rect reg(0,0,segmentImg.cols,segmentImg.rows);
//    reg.x += cvRound(reg.width*0.2);
//    reg.width = cvRound(reg.width*0.6);
//    reg.y += cvRound(reg.height*0.1);
//    reg.height = cvRound(reg.height*0.8);

//    vector<Point2f> polyPoints;
//    polyPoints.push_back(Point2f(reg.x,reg.y));
//    polyPoints.push_back(Point2f(reg.x+reg.width,reg.y));
//    polyPoints.push_back(Point2f(reg.x+reg.width,reg.y+reg.height));
//    polyPoints.push_back(Point2f(reg.x,reg.y+reg.height));

//    for(int k=0; k<clusteredImages.size(); k++)
//    {
////        imshow("11",clusteredImages[k]);
//        for(int j=0; j<imgColor3.cols; j++)
//        {
//            for(int i=0; i<imgColor3.rows; i++)
//            {
//                Point2f pt1(j,i);
//                //            if(reg.contains(pt1) != 0)
//                //                continue;
//                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
//                    continue;
//                if(pointPolygonTest(polyPoints,pt1,false) == 1)
//                    continue;
//                else
//                {
////                    waitKey(0);
//                    cv::Rect r1;
//                    cv::floodFill(imgColor,pt1,cv::Scalar(0,0,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));
//                }
//            }
//        }
////        imshow("12",clusteredImages[k]);
////        waitKey(0);
//    }


    medianBlur(imgColor,imgColor,3);

    imshow("imgShowColor",imgColor);
    waitKey(0);

    outImg = imgColor.clone();
}


void clusterImg(Mat img1, Rect roi1, int K, Mat imglabels, Mat imgMeans)
{
    Mat segmentImg = img1(roi1).clone();
    medianBlur(segmentImg,segmentImg,5);

    Mat samples;
    Mat imgColor = segmentImg.clone();

    cvtColor(segmentImg,segmentImg,CV_BGR2HSV);

    for(int j=0; j<segmentImg.cols; j++)
    {
        for(int k=0; k<segmentImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = 100*double(segmentImg.at<Vec3b>(k,j)[0])/180.0;
            temp.at<double>(0,1) = 100*double(segmentImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(segmentImg.at<Vec3b>(k,j)[2])/255.0;
//            temp.at<double>(0,3) = 100*double(j)/roi1.width;
//            temp.at<double>(0,4) = 100*double(k)/roi1.height;

            samples.push_back(temp.row(0));
        }
    }

    samples.convertTo(samples,CV_32FC1);
    kmeans(samples,K,imglabels,TermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 300, 0.1 ), 1,KMEANS_RANDOM_CENTERS,imgMeans);

    vector<Vec3b> colors(imgMeans.rows);
    vector<Mat> clusteredImages(K);

    for(int k=0; k<imgMeans.rows; k++)
    {
        colors[k][0] = 255 * ( rand() / (double)RAND_MAX );
        colors[k][1] = 255 * ( rand() / (double)RAND_MAX );
        colors[k][2] = 255 * ( rand() / (double)RAND_MAX );

        clusteredImages[k] = Mat::zeros(segmentImg.rows,segmentImg.cols,CV_8UC3);
    }

    int counter = 0;
    for(int j=0; j<imgColor.cols; j++)
    {
        for(int i=0; i<imgColor.rows; i++)
        {
            int label = imglabels.at<int>(counter);
            imgColor.at<Vec3b>(i,j) = colors[label];
            counter++;
            clusteredImages[label].at<Vec3b>(i,j) = colors[label];
        }
    }

    int diff = 25;

    Mat imgColor3 = imgColor.clone();

    Rect reg(0,0,segmentImg.cols,segmentImg.rows);
    reg.x += cvRound(reg.width*0.2);
    reg.width = cvRound(reg.width*0.6);
    reg.y += cvRound(reg.height*0.1);
    reg.height = cvRound(reg.height*0.8);

    vector<Point2f> polyPoints;
    polyPoints.push_back(Point2f(reg.x,reg.y));
    polyPoints.push_back(Point2f(reg.x+reg.width,reg.y));
    polyPoints.push_back(Point2f(reg.x+reg.width,reg.y+reg.height));
    polyPoints.push_back(Point2f(reg.x,reg.y+reg.height));

//    for(int k=0; k<clusteredImages.size(); k++)
//    {
////        imshow("11",clusteredImages[k]);
//        for(int j=0; j<imgColor3.cols; j++)
//        {
//            for(int i=0; i<imgColor3.rows; i++)
//            {
//                Point2f pt1(j,i);
//                //            if(reg.contains(pt1) != 0)
//                //                continue;
//                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
//                    continue;
//                if(pointPolygonTest(polyPoints,pt1,false) == 1)
//                    continue;
//                else
//                {
////                    waitKey(0);
//                    cv::Rect r1;
//                    cv::floodFill(clusteredImages[k],pt1,cv::Scalar(0,0,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));
//                }
//            }
//        }
////        imshow("12",clusteredImages[k]);
////        waitKey(0);
//    }

    Mat imgColor2 = Mat::zeros(segmentImg.rows,segmentImg.cols,CV_8UC3);

    for(int k=0; k<clusteredImages.size(); k++)
    {
        Mat tempImg = clusteredImages[k].clone();
        Mat maskImg = Mat::zeros(clusteredImages[k].rows+2,clusteredImages[k].cols+2,CV_8UC1);

        for(int j=0; j<clusteredImages[k].cols; j++)
        {
            for(int i=0; i<clusteredImages[k].rows; i++)
            {
                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
                    continue;

                cv::Rect r1;
                clusteredImages[k] = tempImg.clone();
                int area = cv::floodFill(clusteredImages[k],maskImg,Point2f(j,i),cv::Scalar(0,255,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));

                Vec3b color1 =  segmentImg.at<Vec3b>(i,j);//getRandColor();

                if(area < 0.1*reg.area())
                    continue;

                for(int l=0; l<clusteredImages[k].cols; l++)
                {
                    for(int m=0; m<clusteredImages[k].rows; m++)
                    {
                        if(clusteredImages[k].at<Vec3b>(m,l) == Vec3b(0,255,0))
                            imgColor2.at<Vec3b>(m,l) = color1;//Vec3b(color1[0],color1[1],color1[2]);
                    }
                }
            }
        }
    }


//    dilate(imgColor2,imgColor3,Mat());
    medianBlur(imgColor2,imgColor2,3);

//    imshow("imgShowColor3",imgColor3);
//    imshow("imgShowColor1",imgColor);
//    imshow("imgShowColor",imgColor2);
//    waitKey(0);

    imgColor2.copyTo(img1(roi1));
}


void findRoifloodFill(cv::Mat img1, cv::Rect roi, int diff = 5)
{
    checkBoundary(img1,roi);
    cv::Mat fillImg = img1(roi).clone();
    cv::cvtColor(fillImg,fillImg,CV_BGR2HSV);

    Mat imgColor = fillImg.clone();

    Mat maskImg = Mat::zeros(fillImg.rows+2,fillImg.cols+2,CV_8UC1);

    for(int j=0; j<fillImg.cols; j++)
    {
        for(int i=0; i<fillImg.rows; i++)
        {
            fillImg = img1(roi).clone();
            cv::Rect r1;

            cv::floodFill(fillImg,maskImg,Point2f(j,i),cv::Scalar(0,255,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));

            Scalar color1 = getRandColor();

            for(int j=0; j<fillImg.cols; j++)
            {
                for(int i=0; i<fillImg.rows; i++)
                {
                    if(fillImg.at<Vec3b>(i,j) == Vec3b(0,255,0))
                        imgColor.at<Vec3b>(i,j) = Vec3b(color1[0],color1[1],color1[2]);
                }
            }

        }
    }

    cv::imshow("filled",imgColor);
    cv::waitKey(0);
}

void findKNN(Mat imgMeansSrc, Mat imgMeansTarget, int KNN, vector< vector<int> >& nbrIndex)
{
    vector <vector<float> > distances(imgMeansSrc.rows);
    for(int i=0; i<imgMeansSrc.rows; i++)
    {
        for(int j=0; j<imgMeansTarget.rows; j++)
        {
            float dist = norm(imgMeansSrc.row(i) - imgMeansTarget.row(j));
            distances[i].push_back(dist);
        }

        vector<int> sortedIndex;
        sortIdx(distances[i],sortedIndex,CV_SORT_EVERY_ROW || CV_SORT_ASCENDING);

        vector<int> temp; temp.assign(sortedIndex.begin(),sortedIndex.begin()+KNN);
        nbrIndex.push_back(temp);
    }

}


void findTargetCandidates(Mat segmentImg, Mat imgMeansSrc, int K, vector< vector<Point2f> >& targetCandidates)
{
    Rect roi1(0,0,segmentImg.cols,segmentImg.rows);

    medianBlur(segmentImg,segmentImg,5);

    Mat samples;
    Mat imgColor = segmentImg.clone();

//    cvtColor(segmentImg,segmentImg,CV_BGR2HSV);

    for(int j=0; j<segmentImg.cols; j++)
    {
        for(int k=0; k<segmentImg.rows; k++)
        {
            Mat temp(1,5,CV_64FC1);
            temp.at<double>(0,0) = 100*double(segmentImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(segmentImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(segmentImg.at<Vec3b>(k,j)[2])/255.0;
            temp.at<double>(0,3) = 100*double(j)/roi1.width;
            temp.at<double>(0,4) = 100*double(k)/roi1.height;

            samples.push_back(temp.row(0));
        }
    }

    samples.convertTo(samples,CV_32FC1);
    Mat imglabels, imgMeansTarget;
    kmeans(samples,K,imglabels,TermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 300, 0.1 ), 1,KMEANS_RANDOM_CENTERS,imgMeansTarget);

    vector<Vec3b> colors(imgMeansSrc.rows);

    for(int k=0; k<imgMeansSrc.rows; k++)
    {
        colors[k][0] = uchar(2.55 * imgMeansSrc.at<float>(k,0));
        colors[k][1] = uchar(2.55 * imgMeansSrc.at<float>(k,1));
        colors[k][2] = uchar(2.55 * imgMeansSrc.at<float>(k,2));
    }

    int counter = 0;
    for(int j=0; j<imgColor.cols; j++)
    {
        for(int i=0; i<imgColor.rows; i++)
        {
            int label = imglabels.at<int>(counter);
            imgColor.at<Vec3b>(i,j) = colors[label];
            counter++;
        }
    }


    vector< vector<int> > nbrIndex;
    findKNN(imgMeansSrc,imgMeansTarget,2,nbrIndex);

    for(int i=0; i<nbrIndex.size(); i++)
    {
        vector<Point2f> tempPoints;
        for(int j=0; j<nbrIndex[i].size(); j++)
        {
            int index = nbrIndex[i][j];

            Point2f pt1;
            pt1.x = roi1.width * imgMeansTarget.at<float>(0,3) / 100;
            pt1.y = roi1.height * imgMeansTarget.at<float>(0,4) / 100;

            tempPoints.push_back(pt1);
        }

        targetCandidates.push_back(tempPoints);
    }


    vector<Point2f> clusterCenters;
    for(int k=0; k<imgMeansTarget.rows; k++)
    {
        Point2f pt1;
        pt1.x = roi1.width * imgMeansTarget.at<float>(k,3) / 100;
        pt1.y = roi1.height * imgMeansTarget.at<float>(k,4) / 100;

        clusterCenters.push_back(pt1);
    }

    Mat_<int> adjMat(clusterCenters.size(), clusterCenters.size(), -1);
    vector< vector<int> > nbrList(clusterCenters.size());
    draw_subdiv(imgColor,clusterCenters,adjMat,nbrList);

    imshow("targetRaw",segmentImg);
    imshow("target",imgColor);
    waitKey(0);

}

void searchBin ( Mat segmentImg, Mat clusters, vector<Vec3b> colors, Mat& labels)
{
    Mat samples;
    medianBlur(segmentImg,segmentImg,5);

    cvtColor(segmentImg,segmentImg,CV_BGR2HSV);

    for(int j=0; j<segmentImg.cols; j++)
    {
        for(int k=0; k<segmentImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = double(segmentImg.at<Vec3b>(k,j)[0]);
            temp.at<double>(0,1) = double(segmentImg.at<Vec3b>(k,j)[1]);
            temp.at<double>(0,2) = double(segmentImg.at<Vec3b>(k,j)[2]);

            samples.push_back(temp.row(0));
        }
    }

    samples.convertTo(samples,CV_32FC1);

    float distFromCluster, minDis=999, clusterThresh ;

    for (int i = 0; i < samples.rows; i++)
    {
        int index = -1;
        minDis = 999;
        for (int j = 0; j < colors.size(); j++)
        {
            clusterThresh = 50;

            distFromCluster = norm(samples.row(i) - Mat(colors).row(j));

            if (distFromCluster < minDis && distFromCluster < clusterThresh)
            {
                minDis = distFromCluster;
                index = j;
            }
        }

        labels.push_back(index);
    }



    vector<Mat> clusteredImages(clusters.rows);

    for(int k=0; k<clusters.rows; k++)
    {
        clusteredImages[k] = Mat::zeros(segmentImg.rows,segmentImg.cols,CV_8UC3);
    }

    Mat imgColor = segmentImg.clone();
    int counter = 0;
    for(int j=0; j<imgColor.cols; j++)
    {
        for(int i=0; i<imgColor.rows; i++)
        {
            int label = labels.at<int>(counter);
            if(label == -1)
                imgColor.at<Vec3b>(i,j) = Vec3b(0,0,0);
            else
            {
                imgColor.at<Vec3b>(i,j) = colors[label];
                clusteredImages[label].at<Vec3b>(i,j) = colors[label];
            }
            counter++;
        }
    }

    imshow("clssc",segmentImg);

    Rect reg(0,0,segmentImg.cols,segmentImg.rows);

    Mat imgColor2 = Mat::zeros(segmentImg.rows,segmentImg.cols,CV_8UC3);

    int diff = 5;
    for(int k=0; k<clusteredImages.size(); k++)
    {
        Mat tempImg = clusteredImages[k].clone();
        Mat maskImg = Mat::zeros(clusteredImages[k].rows+2,clusteredImages[k].cols+2,CV_8UC1);

        for(int j=0; j<clusteredImages[k].cols; j++)
        {
            for(int i=0; i<clusteredImages[k].rows; i++)
            {
                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
                    continue;

                cv::Rect r1;
                clusteredImages[k] = tempImg.clone();
                int area = cv::floodFill(clusteredImages[k],maskImg,Point2f(j,i),cv::Scalar(0,255,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));

                Vec3b color1 =  segmentImg.at<Vec3b>(i,j);//getRandColor();

//                if(area < 0.01*reg.area())
//                    continue;

                for(int l=0; l<clusteredImages[k].cols; l++)
                {
                    for(int m=0; m<clusteredImages[k].rows; m++)
                    {
                        if(clusteredImages[k].at<Vec3b>(m,l) == Vec3b(0,255,0))
                            imgColor2.at<Vec3b>(m,l) = color1;//Vec3b(color1[0],color1[1],color1[2]);
                    }
                }
            }
        }
    }

    medianBlur(imgColor2,imgColor2,3);

    imshow("seg",imgColor2);
    waitKey(0);
}

/*!
  @brief Finds differential values for the components in reasoning matrix
  */
void findDifferentialValues_reasonMatComp(vector< vector<float> > assigned_reasonMat_comp, vector<float>& diffValues)
{
    for(int k=0; k<assigned_reasonMat_comp.size(); k++)
    {
        float diffValue = 0;
        for(int j=0; j<assigned_reasonMat_comp[k].size()-1; j++)
        {
            for(int i=j+1; i<assigned_reasonMat_comp[k].size(); i++)
            {
                float assign1 = assigned_reasonMat_comp[k][j];
                float assign2 = assigned_reasonMat_comp[k][i];

                diffValue += abs(assign1 - assign2);
            }
        }

        diffValues.push_back(diffValue/assigned_reasonMat_comp[k].size());
    }
}

/*!
  @brief Updates the weights of parameters based on EM formulas (and alternately some naive override)
  */
void updateWeights_reasonMatComp(vector<float>& weights_reasoning, vector< vector<float> > diffValues_reasoning)
{
    if(diffValues_reasoning.empty())
    {
        cerr << "Differential Component Values are empty" << endl;
        return;
    }

    // Membership Probabilities, rows = number of components, cols = number of samples
    Mat Tij(diffValues_reasoning.size(),diffValues_reasoning[0].size(),CV_32FC1,Scalar::all(0));

    for(int j=0; j<Tij.cols; j++)
    {
        float sum = 0;
        for(int i=0; i<Tij.rows; i++)
        {
            Tij.at<float>(i,j) = weights_reasoning[i] * diffValues_reasoning[i][j];
            sum += Tij.at<float>(i,j);
        }

        for(int i=0; i<Tij.rows; i++)
        {
            if(sum!=0)
                Tij.at<float>(i,j) /= sum;
            else
                Tij.at<float>(i,j) = 0;
        }
    }

    //Override-----
    //-----
    vector<float> sums;
    for(int i=0; i<Tij.rows; i++)
    {
        Scalar sum1 = sum(Mat(diffValues_reasoning[i]));
        sums.push_back(sum1[0]);
    }

    Scalar superSum = sum(Mat(sums));
    //-----
    //-----

    for(int i=0; i<Tij.rows; i++)
    {
        weights_reasoning[i] = 0;

        for(int j=0; j<Tij.cols; j++)
        {
            weights_reasoning[i] += Tij.at<float>(i,j);
        }

        weights_reasoning[i] /= Tij.cols;

        // This is an override over what EM is supposed to do
        weights_reasoning[i] = sums[i]/superSum[0];
    }
}



/*!
@brief Scales a Rectangle times the given value (1.1 (expansion) or 0.6 (contraction) say). Changes the input Rectangle itself.
  */
void scaleRect(Rect& ioRoi, float percentScale)
{
    Point2f center = 0.5 * (ioRoi.tl() + ioRoi.br());

    ioRoi.width = float(ioRoi.width) * percentScale;
    ioRoi.height = float(ioRoi.height) * percentScale;

    ioRoi.x = center.x - ioRoi.width/2;
    ioRoi.y = center.y - ioRoi.height/2;
}


/*!
  @brief Segments the human body pixels using the GMM learnt models.
  */
void segmentBodyParts(Ptr<EM> em1, Ptr<EM> em2, Ptr<EM> em3, Ptr<EM> em4, Mat samples, Mat& labels, int no_head)
{
    samples.convertTo(samples,CV_64FC1);

    RNG randIndex;
    Mat samples2;
    Mat_<int> flag(samples.rows,1,-1);
    for(int k=0; k<samples.rows/2; k++)
    {
        int index = randIndex.uniform(0,samples.rows);
        samples2.push_back(samples.row(index));
        flag(index) = 1;
    }

//    samples = samples2.clone();
//    cout << samples << endl;
//    cout << samples.rows << endl;

    for(int k=0; k<samples.rows; k++)
    {
        if(flag(k)==-1)
        {
            labels.push_back(-1);
            continue;
        }

        Mat probs;

        Vec2d out1 = em1->predict(samples.row(k), probs);
        Vec2d out2 = em2->predict(samples.row(k),probs);
        Vec2d out3 = em3->predict(samples.row(k), probs);
        Vec2d outBG = em4->predict(samples.row(k), probs);

        double prob1 = exp(out1[0])/(exp(out1[0])+exp(outBG[0]));
        double prob2 = exp(out2[0])/(exp(out2[0])+exp(outBG[0]));
        double prob3 = exp(out3[0])/(exp(out3[0])+exp(outBG[0]));

        //        cout << prob1 << "\t" << prob2 << "\t" << prob3 << endl;
        //        cout << out1[0] << "\t" << out2[0] << "\t" << out3[0] << "\t" << outBG[0] << endl;

        double prob1_ = exp(out1[0])/(exp(out1[0])+exp(outBG[0])+exp(out2[0])+exp(out3[0]));
        double prob2_ = exp(out2[0])/(exp(out1[0])+exp(outBG[0])+exp(out2[0])+exp(out3[0]));
        double prob3_ = exp(out3[0])/(exp(out1[0])+exp(outBG[0])+exp(out2[0])+exp(out3[0]));

        if(no_head)
        {
            if(prob2 > prob3 && prob2 > 0.8)
            {
                labels.push_back(0);
            }
            else if(prob3 > prob2 && prob3 > 0.8)
            {
                labels.push_back(1);
            }
            else
            {
                labels.push_back(-1);
            }
        }
        else
        {
//            if( prob1 > prob2 && prob1 > prob3 && prob1 > 0.8)
//            {
//                labels.push_back(0);
//            }
//            else if( prob2 > prob1 && prob2 > prob3 && prob2 > 0.8)
//            {
//                labels.push_back(1);
//            }
//            else if( prob3 > prob1 && prob3 > prob2 && prob3 > 0.8)
//            {
//                labels.push_back(2);
//            }
//            else
//            {
//                labels.push_back(-1);
//            }

            float thresh = 0.25;
            if( prob1_ > thresh)
            {
                labels.push_back(0);
            }
            else if( prob2_ > thresh)
            {
                labels.push_back(1);
            }
            else if( prob3_ > thresh)

            {
                labels.push_back(2);
            }
            else
            {
                labels.push_back(-1);
            }

        }
    }
}


/*!
  @brief Segments the sample pixels based on fg and bg model
  */
void segmentBodyParts_fgbg(Ptr<EM> em1, Ptr<EM> em4, Mat samples, Mat& labels, Mat& probVals)
{
    samples.convertTo(samples,CV_64FC1);

    RNG randIndex;
    Mat samples2;
    Mat_<int> flag(samples.rows,1,-1);
    for(int k=0; k<samples.rows/2; k++)
    {
        int index = randIndex.uniform(0,samples.rows);
        samples2.push_back(samples.row(index));
        flag(index) = 1;
    }

//    samples = samples2.clone();
//    cout << samples << endl;
//    cout << samples.rows << endl;

    for(int k=0; k<samples.rows; k++)
    {
        if(flag(k)==-1)
        {
            labels.push_back(-1);
            probVals.push_back(float(0.0));
            continue;
        }

        Mat probs;

#if(OPENCV3)
        Vec2d out1 = em1->predict2(samples.row(k), probs);
        Vec2d outBG = em4->predict2(samples.row(k), probs);
#else
        Vec2d out1 = em1->predict(samples.row(k), probs);
        Vec2d outBG = em4->predict(samples.row(k), probs);
#endif

        double prob1_ = exp(out1[0])/(exp(out1[0])+exp(outBG[0]));

        float thresh = float(GMM_FG_BG_THRESH);
        if( prob1_ > thresh)
        {
            labels.push_back(int(out1[1]));
            probVals.push_back(float(prob1_));
        }
        else
        {
            labels.push_back(-1);
            probVals.push_back(float(0.0));
        }
    }
}

#if(USELESS_FOR_NOW)
/*!
  @brief Creates a GMM model for the agent for head, torso, legs and background. Tests it on the input image as well.
  */
void createGMModel(Mat inputImg, Rect agentRoi, Ptr<EM>& m1, Ptr<EM>& m2, Ptr<EM>& m3, Ptr<EM>& m4)
{
    // Create an expanded ROI for bg image
    Rect expandedRoi = agentRoi;
    scaleRect(expandedRoi,1.3);
    checkBoundary(inputImg,expandedRoi);
    Mat bgImg = inputImg(expandedRoi).clone();

    Point2f bgCenter(expandedRoi.width/2,expandedRoi.height/2);

    Rect contractedRoi = agentRoi;
    scaleRect(contractedRoi,0.9);
    Rect agentRoi_bg = contractedRoi;        // agent ROI wrt BG image
    agentRoi_bg.x = bgCenter.x-agentRoi_bg.width/2;
    agentRoi_bg.y = bgCenter.y-agentRoi_bg.height/2;

    Mat fgImg = bgImg(agentRoi_bg).clone();
    RotatedRect fgEllipse(bgCenter,Size(agentRoi_bg.width,agentRoi_bg.height),0);

    vector<Point> polyPoints;
    ellipse2Poly(bgCenter,Size(agentRoi_bg.width/2,agentRoi_bg.height/2),0,0,360,10,polyPoints);

    // Find different ROIs for head, torso and legs as per the standard human aspect ratio
    Rect B1, B2, B3;
    B1.x = agentRoi_bg.x; B1.y = agentRoi_bg.y; B1.width= agentRoi_bg.width; B1.height =  int(0.15*agentRoi_bg.height);
    B2.x = agentRoi_bg.x; B2.y = agentRoi_bg.y+int(0.15*agentRoi_bg.height); B2.width= agentRoi_bg.width; B2.height =  int(0.4*agentRoi_bg.height);
    B3.x = agentRoi_bg.x; B3.y = agentRoi_bg.y+int(0.55*agentRoi_bg.height); B3.width= agentRoi_bg.width; B3.height =  int(0.45*agentRoi_bg.height);

    // Training samples for head, torso, legs and background, and total fg dsamples
    Mat samples1, samples2, samples3, samples4, samplesTest;

    for(int j=0; j<bgImg.cols; j++)
    {
        for(int k=0; k<bgImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = 100*double(bgImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(bgImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(bgImg.at<Vec3b>(k,j)[2])/255.0;

            Point2f thisPoint(j,k);
            if(agentRoi_bg.contains(thisPoint))
                samplesTest.push_back(temp.row(0));

            int check = pointPolygonTest(polyPoints,thisPoint,false);
            if(check == 1)
            {

                if(B1.contains(thisPoint))
                {
                    samples1.push_back(temp.row(0));
                    bgImg.at<Vec3b>(k,j) = Vec3b(0,0,255);
                }
                else if(B2.contains(thisPoint))
                {
                    samples2.push_back(temp.row(0));
                    bgImg.at<Vec3b>(k,j) = Vec3b(0,255,0);
                }
                else if(B3.contains(thisPoint))
                {
                    samples3.push_back(temp.row(0));
                    bgImg.at<Vec3b>(k,j) = Vec3b(255,0,0);
                }
            }
            else
            {
                samples4.push_back(temp.row(0));
            }
        }
    }

    // Learn the respective models
    int clusNum = 1;
    vector<Mat> covs1,covs2,covs3,covs4;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);
    Mat means1 = Mat::zeros(clusNum, samples1.cols, CV_64FC1), means2 = means1.clone(), means3 = means1.clone();
    Mat weights1 = Mat::ones(1, clusNum, CV_64FC1)/clusNum, weights2 = weights1.clone(), weights3 = weights1.clone();
    Mat out1, out2, out3;

#if(OPENCV3)
    EM::Params emParams(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
    m1->create(emParams);
    m2->create(emParams);
    m3->create(emParams);
#else
    *m1 = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
    *m2 = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
    *m3 = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
#endif

    m1->train(samples1);//, means1, covs1, weights1, out1, out2, out3);
    m2->train(samples2);//, means2, covs2, weights2, out1, out2, out3);
    m3->train(samples3);//, means3, covs3, weights3, out1, out2, out3);

    clusNum = 5;
    Mat means4 = Mat::zeros(clusNum, samples4.cols, CV_64FC1);
    Mat weights4 = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

    EM::Params emParams2(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
    m4->create(emParams2);
    m4->train(samples4);//, means4, covs4, weights4, out1, out2, out3);


    // A test to check if models are learnt correctly and able to differentiate (test samples same as training samples)
    Mat labelsTest;
    segmentBodyParts(m1,m2,m3,m4,samplesTest,labelsTest,0);
    vector<Vec3b> testColors;
    for(int k=0; k<3; k++)
    {
        testColors.push_back(Vec3b(0,0,0));
        testColors[k][0] = 255 * ( rand() / (double)RAND_MAX );
        testColors[k][1] = 255 * ( rand() / (double)RAND_MAX );
        testColors[k][2] = 255 * ( rand() / (double)RAND_MAX );

        cout << Mat(testColors[k]) << endl;
    }

    Mat testClustering = fgImg.clone();
    int counter1=0;
    for(int j=0; j<testClustering.cols; j++)
    {
        for(int i=0; i<testClustering.rows; i++)
        {
            if(labelsTest.at<int>(counter1)!=-1)
                testClustering.at<Vec3b>(i,j) = testColors[labelsTest.at<int>(counter1)];
            else
                testClustering.at<Vec3b>(i,j) = Vec3b(0,0,0);

            counter1++;
        }
    }



    // Display the segmentation
    rectangle(bgImg,B1,Scalar(255,255,0),1,LINENAME);
    rectangle(bgImg,B2,Scalar(255,255,0),1,LINENAME);
    rectangle(bgImg,B3,Scalar(255,255,0),1,LINENAME);
    ellipse(bgImg,fgEllipse,Scalar(0,255,255),1,LINENAME);

    imshow("segimggmm",bgImg);
    dilate(testClustering,testClustering,Mat());
    imshow("testClustering",testClustering);
    waitKey(0);

}
#endif
/*!
  @brief Finds blobs in the source image based on GMM.
  */
void findSourceBlobs_GMM(Mat srcImg, Ptr<EM> m1FG, Ptr<EM> m4BG, Mat& segmentedImgSelectedROIs, Mat& graphImg, Mat_<int>& adjMat,
                         vector<Point2f>& blobCenters, vector<Vec3b>& ccColors, vector<int>& selectedComponentIndex,
                         Mat& segmentedImg)
{
    Mat samplesTest;
    Rect imgROI(0,0,srcImg.cols,srcImg.rows);
    graphImg = srcImg.clone();

    for(int j=0; j<srcImg.cols; j++)
    {
        for(int k=0; k<srcImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = 100*double(srcImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(srcImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(srcImg.at<Vec3b>(k,j)[2])/255.0;

            samplesTest.push_back(temp.row(0));
        }
    }

    Mat labelsTest, probs;
    segmentBodyParts_fgbg(m1FG,m4BG,samplesTest,labelsTest,probs);


    // Create a set of images for different components of GMM
    int clusNumFG = GMM_COMPONENTS_FG;

    vector<Mat> clusteredImages(clusNumFG);
    vector<Vec3b> testColors;
    map<vector<uchar>,int> colorLabelMap;   // Stores the mapping from cluster colors to labels

    for(int k=0; k<clusNumFG; k++)
    {
        testColors.push_back(Vec3b(0,0,0));
        testColors[k][0] = 255 * ( rand() / (double)RAND_MAX );
        testColors[k][1] = 255 * ( rand() / (double)RAND_MAX );
        testColors[k][2] = 255 * ( rand() / (double)RAND_MAX );

        clusteredImages[k] = Mat::zeros(srcImg.rows,srcImg.cols,CV_8UC3);

        vector<uchar> tempColor;
        tempColor.push_back(testColors[k][0]);
        tempColor.push_back(testColors[k][1]);
        tempColor.push_back(testColors[k][2]);

        colorLabelMap[tempColor] = k;
    }

    Mat testClustering = srcImg.clone();
    int counter1=0;
    for(int j=0; j<testClustering.cols; j++)
    {
        for(int i=0; i<testClustering.rows; i++)
        {
            int label = labelsTest.at<int>(counter1);

            if(label!=-1)
            {
                testClustering.at<Vec3b>(i,j) = testColors[label];
            }
            else
                testClustering.at<Vec3b>(i,j) = Vec3b(0,0,0);

            counter1++;
        }
    }

    // Querying lesser pixels in GMM and dilating the resultant image takes less time than querying all the pixels
    dilate(testClustering,testClustering,Mat());

    // Image containing only FG pixels
    segmentedImg = Mat::zeros(srcImg.rows,srcImg.cols,srcImg.type());

    // Update the image cluster
    for(int j=0; j<testClustering.cols; j++)
    {
        for(int i=0; i<testClustering.rows; i++)
        {
            Vec3b color1 = testClustering.at<Vec3b>(i,j);

            if(color1 == Vec3b(0,0,0))
                continue;

            vector<uchar> tempColor;
            tempColor.push_back(color1[0]);
            tempColor.push_back(color1[1]);
            tempColor.push_back(color1[2]);

            int label = colorLabelMap[tempColor];
            clusteredImages[label].at<Vec3b>(i,j) = color1;//testColors[minDisIndex];
            segmentedImg.at<Vec3b>(i,j) = srcImg.at<Vec3b>(i,j);
        }
    }

    // Difference threshold for flood fill
    int diff = int(FLOOD_FILL_DIFF);

    // Use Connected Component Analysis (flood fill) for separating out connected blobs for each GMM component
    Mat imgColor2 = Mat::zeros(srcImg.rows,srcImg.cols,CV_8UC3);
    vector<Rect> blobROIs;                      // ROIs of blobs
    Mat blobImgTrueColors = imgColor2.clone();  // Contains pixels of original image that are selected as FG

    // Iterate over all the different components images
    for(int k=0; k<clusteredImages.size(); k++)
    {
        Mat tempImg = clusteredImages[k].clone();
        Mat maskImg = Mat::zeros(clusteredImages[k].rows+2,clusteredImages[k].cols+2,CV_8UC1);

        vector<Point2f> blobCentersPerComponent;
        vector<Rect> blobROIsPerComponent;
        vector<Vec3b> blobColorsPerComponent;

        // Iterate over all the pixels
        for(int j=0; j<clusteredImages[k].cols; j++)
        {
            for(int i=0; i<clusteredImages[k].rows; i++)
            {
                // Pixels belonging to bg are not required
                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
                    continue;

                cv::Rect r1;
                clusteredImages[k] = tempImg.clone();

                // Apply flood fill
                int pixelCount = cv::floodFill(clusteredImages[k],maskImg,Point2f(j,i),cv::Scalar(0,255,0),&r1,
                                               cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));

                Vec3b color1 =  srcImg.at<Vec3b>(i,j);//getRandColor();
                Vec3f meanColor = color1;

                // Remove blobs of small size
                if(r1.area() < 0.05*imgROI.area())
                    continue;

                // Re-paint the image blobs with mean color
                int colorValCounter = 0;
                for(int l=0; l<clusteredImages[k].cols; l++)
                {
                    for(int m=0; m<clusteredImages[k].rows; m++)
                    {
                        if(clusteredImages[k].at<Vec3b>(m,l) == Vec3b(0,255,0))
                        {
                            imgColor2.at<Vec3b>(m,l) = color1;//Vec3b(color1[0],color1[1],color1[2]);
                            blobImgTrueColors.at<Vec3b>(m,l) = srcImg.at<Vec3b>(m,l);
                            meanColor += srcImg.at<Vec3b>(m,l);
                            colorValCounter++;
                        }
                    }
                }

                rectangle(blobImgTrueColors,r1,getRandColor(),2);


                meanColor /= colorValCounter;

                blobROIsPerComponent.push_back(r1);
                blobCentersPerComponent.push_back(0.5*(r1.tl()+r1.br()));
                blobColorsPerComponent.push_back(Vec3b(meanColor));
            }
        }

        // Select only the largest blob per component
        if(blobROIsPerComponent.size() > 1)
        {
            vector<float> roiAreas;
            for(int j=0; j<blobROIsPerComponent.size(); j++)
            {
                roiAreas.push_back(blobROIsPerComponent[j].area());
            }

            double minVal = 0, maxVal = 0;
            int minValIdx = -1, maxValIdx = -1;
            minMaxIdx(Mat(roiAreas),&minVal,&maxVal,&minValIdx,&maxValIdx);

            blobROIs.push_back(blobROIsPerComponent[maxValIdx]);
            blobCenters.push_back(blobCentersPerComponent[maxValIdx]);
            ccColors.push_back(blobColorsPerComponent[maxValIdx]);
            selectedComponentIndex.push_back(k);
        }
        else if(blobCentersPerComponent.size() == 1)
        {
            blobROIs.push_back(blobROIsPerComponent[0]);
            blobCenters.push_back(blobCentersPerComponent[0]);
            ccColors.push_back(blobColorsPerComponent[0]);
            selectedComponentIndex.push_back(k);
        }
    }

    // Draw Delaunay Triangulation for all the blobs found in the image
    vector< vector<int> > nbrList(blobCenters.size());
    draw_subdiv(graphImg,blobCenters,adjMat,nbrList);

//    cerr << "For sources " <<  adjMat << endl;

    // Display the segmentation
//    ellipse(bgImg,fgEllipse,Scalar(0,255,255),1,LINENAME);

//    imshow("segimggmm",bgImg);
//    imshow("ccImg",imgColor2);
//    imshow("blobImgTrueColors",blobImgTrueColors);
//    imshow("testClustering",testClustering);
//    imshow("graph",graphImg);
//    waitKey(0);

    segmentedImgSelectedROIs = blobImgTrueColors.clone();
}


/*!
  @brief Finds target candidate blobs wrt to the blobs of the src image.
  */
void findTargetCandidateBlobs(Mat fgImg, Ptr<EM> emModelFG, Ptr<EM> emModelBG, vector<int> selectedCompInd,
                              vector< vector<Point2f> >& targetCandidates, vector<vector<Vec3b> >& targetCandColors,
                              Mat& segmentedImgSelectedROIs, Mat& segmentedImg )
{
    Mat samples;
    Rect imgRoi(0,0,fgImg.cols,fgImg.rows);

    for(int j=0; j<fgImg.cols; j++)
    {
        for(int k=0; k<fgImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = 100*double(fgImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(fgImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(fgImg.at<Vec3b>(k,j)[2])/255.0;

            samples.push_back(temp.row(0));
        }
    }


    Mat labelsTest, probs;
    segmentBodyParts_fgbg(emModelFG,emModelBG,samples,labelsTest,probs);

    int clusNumFG = GMM_COMPONENTS_FG;

    // Create a set of images for different components of GMM
    vector<Mat> clusteredImages(clusNumFG);
    vector<Vec3b> testColors;
    map<vector<uchar>,int> colorLabelMap;   // Stores the mapping from cluster colors to labels

    for(int k=0; k<clusNumFG; k++)
    {
        testColors.push_back(Vec3b(0,0,0));
        testColors[k][0] = 255 * ( rand() / (double)RAND_MAX );
        testColors[k][1] = 255 * ( rand() / (double)RAND_MAX );
        testColors[k][2] = 255 * ( rand() / (double)RAND_MAX );

        clusteredImages[k] = Mat::zeros(fgImg.rows,fgImg.cols,CV_8UC3);

        vector<uchar> tempColor;
        tempColor.push_back(testColors[k][0]);
        tempColor.push_back(testColors[k][1]);
        tempColor.push_back(testColors[k][2]);

        colorLabelMap[tempColor] = k;
    }

    Mat testClustering = fgImg.clone();
    int counter1=0;
    for(int j=0; j<testClustering.cols; j++)
    {
        for(int i=0; i<testClustering.rows; i++)
        {
            int label = labelsTest.at<int>(counter1);

            if(label!=-1)
            {
                testClustering.at<Vec3b>(i,j) = testColors[label];
            }
            else
                testClustering.at<Vec3b>(i,j) = Vec3b(0,0,0);

            counter1++;
        }
    }

    // Querying lesser pixels in GMM and dilating the resultant image takes less time than querying all the pixels
    dilate(testClustering,testClustering,Mat());

    // Image containing only FG pixels
    segmentedImg = Mat::zeros(fgImg.rows,fgImg.cols,fgImg.type());

    // Update the image cluster
    for(int j=0; j<testClustering.cols; j++)
    {
        for(int i=0; i<testClustering.rows; i++)
        {
            Vec3b color1 = testClustering.at<Vec3b>(i,j);
            if(color1 == Vec3b(0,0,0))
                continue;

            vector<uchar> tempColor;
            tempColor.push_back(color1[0]);
            tempColor.push_back(color1[1]);
            tempColor.push_back(color1[2]);

            int label = colorLabelMap[tempColor];

            clusteredImages[label].at<Vec3b>(i,j) = color1;//testColors[minDisIndex];
            segmentedImg.at<Vec3b>(i,j) = fgImg.at<Vec3b>(i,j);
//            clusteredImages[label].at<Vec3b>(i,j) = testClustering.at<Vec3b>(i,j);//testColors[minDisIndex];
        }
    }

    // Difference threshold for flood fill
    int diff = int(FLOOD_FILL_DIFF);

    // Use Connected Component Analysis (flood fill) for separating out connected blobs for each GMM component
    Mat imgColor2 = Mat::zeros(fgImg.rows,fgImg.cols,CV_8UC3);
    vector<Rect> blobROIs;          // ROIs of blobs
    vector<Point2f> blobCenters;    // Centers of blobs
    Mat blobImgTrueColors = imgColor2.clone();    // Contains pixels of original image that are selected as FG

    int tempCounter = 0;
    // Iterate over all the different components images
    for(int k=0; k<clusteredImages.size(), tempCounter<selectedCompInd.size(); k++)
    {
        if(selectedCompInd[tempCounter] != k)
        {
            continue;
        }

        tempCounter++;

        Mat tempImg = clusteredImages[k].clone();
        Mat maskImg = Mat::zeros(clusteredImages[k].rows+2,clusteredImages[k].cols+2,CV_8UC1);

        vector<Point2f> targetBlobs;    // Target Candidate blobs for kth component of GMM
        vector<Vec3b> targetColors;     // Target Candidate blobs mean color values

        // Iterate over all the pixels
        for(int j=0; j<clusteredImages[k].cols; j++)
        {
            for(int i=0; i<clusteredImages[k].rows; i++)
            {
                // Pixels belonging to bg are not required
                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
                    continue;

                cv::Rect r1;
                clusteredImages[k] = tempImg.clone();

                // Apply flood fill
                int pixelCount = cv::floodFill(clusteredImages[k],maskImg,Point2f(j,i),cv::Scalar(0,255,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));

                Vec3b color1 = fgImg.at<Vec3b>(i,j);//getRandColor();
                Vec3f meanColor = color1;

                // Remove blobs of small size
                if(r1.area() < 0.05*imgRoi.area())
                    continue;

                // Re-paint the image blobs with mean color
                int colorValCounter = 0;
                for(int l=0; l<clusteredImages[k].cols; l++)
                {
                    for(int m=0; m<clusteredImages[k].rows; m++)
                    {
                        if(clusteredImages[k].at<Vec3b>(m,l) == Vec3b(0,255,0))
                        {
                            imgColor2.at<Vec3b>(m,l) = color1;//Vec3b(color1[0],color1[1],color1[2]);
                            blobImgTrueColors.at<Vec3b>(m,l) = fgImg.at<Vec3b>(m,l);
                            meanColor += fgImg.at<Vec3b>(m,l);
                            colorValCounter++;
                        }
                    }
                }

                rectangle(blobImgTrueColors,r1,getRandColor(),2);

                meanColor /= colorValCounter;

                blobROIs.push_back(r1);
                blobCenters.push_back(0.5*(r1.tl()+r1.br()));
                targetBlobs.push_back(0.5*(r1.tl()+r1.br()));
                targetColors.push_back(Vec3b(meanColor));
            }
        }

        targetCandidates.push_back(targetBlobs);
        targetCandColors.push_back(targetColors);
    }

    // Draw Delaunay Triangulation for all the blobs found in the image
    Mat_<int> adjMat;
    vector< vector<int> > nbrList(blobCenters.size());
//    draw_subdiv(fgImg,blobCenters,adjMat,nbrList);

//    imshow("targetFGimg",fgImg);
//    imshow("target_CCimg",imgColor2);
//    imshow("target_blobImgTrueColors",blobImgTrueColors);
//    imshow("testClustering",testClustering);
//    waitKey(0);

    segmentedImgSelectedROIs = blobImgTrueColors.clone();
}


// Function for finding the factors for a given adjacenecy matrix of a graph
// Parameters:
// factorFile -     Input/Output, stores the factors in particular format
// adjMat -         Input adjacency matrix of the graph
// candidates -     Input candidates for the nodes of graph
// edgePotentials - Output, stores edge potentials for each possible edge, in order to calculate the error of the selected edge
void getFactors(ofstream& factorFile, Mat_<int> adjMat, vector<Point2f> points1, vector<Vec3b> ccColors,
                vector< vector<Point2f> > candidates, vector< vector<Vec3b> > candColors, vector< Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1 && (candidates[i].size() * candidates[j].size()) != 0 )
            {
                Mat_<float> truthTable(candidates[i].size(),candidates[j].size(),0.0);
                // for this particular case, the required angle between nodes is -90
//                float theta1 = -90;

                // Find the angle between source nodes
                float theta1 = float(180 * 7.0/22.0) *
                        ( atan2( (points1[i].y - points1[j].y), (points1[i].x - points1[j].x) ) );

                if(isnan(theta1))
                    theta1 = -90;

                float r1 = norm(points1[i]-points1[j]);

                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << candidates[i].size() << " " << candidates[j].size() << endl; // Cardinality of each variable
                factorFile <<  candidates[i].size() * candidates[j].size() << endl; // Non Zero states

                int factorCounter=0;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<candidates[j].size(); m++)
                {
                    //                    candidateError[j][m] = 0;
                    for(int l=0; l<candidates[i].size(); l++)
                    {
                        //                        candidateError[i][l] = 0;
                        {
                            // Find the angle between candidate nodes
                            float theta2 = float(180 * 7.0/22.0) *
                                    ( atan2( (candidates[i][l].y - candidates[j][m].y), (candidates[i][l].x - candidates[j][m].x) ) );

                            if(isnan(theta2))
                                theta2 = -90;

                            float r2 = norm(candidates[i][l]-candidates[j][m]);
                            float rErr = r1 > r2 ? r2/r1 : r1/r2;

                            float phi1 = norm(ccColors[i]-candColors[i][l]);
                            float phi2 = norm(ccColors[j]-candColors[j][m]);

                            // Calcualte edge potential
                            float psi = exp(- 5*abs(theta1-theta2)/100 - 5*(phi1)/100 - 5*(phi2)/100 /*- 2.5*abs(1-r2/r1)/2 */);

                            // if it is too less, punish further, possibly an outlier
                            if(psi < 0.5)
                            {
                                psi = 0.0001;
                            }

                            factorFile << factorCounter << " " << psi << endl; // factor state index and value

                            truthTable(l,m) = psi;

//                            cout << i << "\t" << j << "\t" << l << "\t" << m << "\t" << theta1 << "\t" <<
//                                    theta2 << "\t" << psi << "\t" << abs(theta1-theta2) << "\t" << phi1 << "\t" <<
//                                    phi2 << "\t" << exp(-5*(phi1)/100) << "\t" <<
//                                    r1 << "\t" << abs(1-rErr) << "\t" << r2 << "\t" << exp(-2.5*abs(1-rErr)) <<
//                                    endl;

                            factorCounter++;
                        }
                    }
                }
                edgePotentials.push_back(truthTable);

                factorFile << "\n" << endl;
            }
        }
}

// Finds graph matching based on MAP solution using BP for a given MRF model of an underlying graph
// Parameters:
// adjMat -     Input adjacency matrix of the graph (n x n, n is number of nodes of graph)
// candidates - Input possible candidates for each node of graph
// outpoints -  Output selected candidates
// partsFound - Output flags vector for parts found or not found
// matchIndex - Output indicies of selected candidates
bool findMatch_MRF_MAP(Mat_<int> adjMat, vector<Point2f> points1, vector<Vec3b> ccColors, vector< vector<Point2f> > candidates,
                       vector< vector<Vec3b> > candColors, vector<int>& matchIndex, Mat_<int>& adjMatOut, float& graphMatchProb,
                       string outputPath)
{
    bool atLeastOneEdge = false;
    int factorCounter = 0;
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                if(candidates[i].size() * candidates[j].size() > 0)
                {
                    atLeastOneEdge = true;
                    factorCounter++;
                }
            }
        }

    if(!atLeastOneEdge)
    {
//        cerr << "No edge possible in target graph, hence no graph matching" << endl;
        return false;
    }

    // File to store the factors in a particular format
    ofstream factorFile;
    string factorFileName = outputPath;
    factorFileName.append("factor.fg");
    factorFile.open(factorFileName.c_str());

    factorFile << factorCounter << "\n" << endl;    // Pass the total number of factors

    vector< Mat_<float> > edgePotentials;

    // Function for calculating factors
    getFactors(factorFile,adjMat,points1,ccColors,candidates,candColors,edgePotentials);

    // Reading factors from the file (written above)
    dai::FactorGraph fg;
    fg.ReadFromFile(factorFileName.c_str());

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 0;

    // Store the constants in a PropertySet object
    dai::PropertySet opts;
    opts.set("maxiter",maxiter);  // Maximum number of iterations
    opts.set("tol",tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)

    // Construct a BP (belief propagation) object from the FactorGraph fg
    // using the parameters specified by opts and two additional properties,
    // specifying the type of updates the BP algorithm should perform and
    // whether they should be done in the real or in the logdomain
    //
    // Note that inference is set to MAXPROD, which means that the object
    // will perform the max-product algorithm instead of the sum-product algorithm
//    dai::BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));
    // Initialize max-product algorithm
//    mp.init();
    // Run max-product algorithm
//    mp.run();

    // Construct another JTree (junction tree) object that is used to calculate
    // the joint configuration of variables that has maximum probability (MAP state)
    dai::JTree mp( fg, opts("updates",string("HUGIN"))("inference",string("MAXPROD")) );
    // Initialize junction tree algorithm
    mp.init();
    // Run junction tree algorithm
    mp.run();
    // Calculate joint state of all variables that has maximum probability

    // Calculate joint state of all variables that has maximum probability
    // based on the max-product result
    vector<size_t> mpstate = mp.findMaximum();


    // Initialize the output variables
    matchIndex.assign(adjMat.rows,-1);

    for( size_t i = 0; i < mpstate.size(); i++ )
    {
        int nodeLabel = fg.var(i).label();

        matchIndex[nodeLabel] = mpstate[i];
    }

    adjMatOut = Mat_<int>(adjMat.rows,adjMat.cols,-1);

    bool matchOK = false;
    double logScore = 0;
    for( size_t I = 0; I < fg.nrFactors(); I++ )
    {
        // Find labels of the variables involved in the factor
        int nodeLabel1 = dai::Var(fg.factor(I).vars().begin()[0]).label();
        int nodeLabel2 = dai::Var(fg.factor(I).vars().begin()[1]).label();

        // Find indicies of the variables involved in the factor
        int nodeIndex1 = fg.findVar(fg.factor(I).vars().begin()[0]);
        int nodeIndex2 = fg.findVar(fg.factor(I).vars().begin()[1]);

        // Report max-product factor marginals
//        cout << "Approximate (max-product) MAP factor marginals:" << endl;
//        cout << mp.belief(fg.factor(I).vars()) << " == " << mp.beliefF(I) << endl;

        double assignedEdgePotential = edgePotentials[I](mpstate[nodeIndex1],mpstate[nodeIndex2]);
        // Mark the successful matched edges
        if(assignedEdgePotential > 0.1)
        {
            adjMatOut[nodeLabel1][nodeLabel2] = 1;
            matchOK = true;
        }

        logScore += log(assignedEdgePotential);

        Scalar edgePotentialSum = sum(edgePotentials[I]);

    }

    // Report log partition sum of fg, approximated by the belief propagation algorithm
//    cout << "Approximate (loopy belief propagation) log partition sum: " << mp.logZ() << endl;
//    cout << "Approximate (max-product) MAP state (log score = " << fg.logScore( mpstate ) << "):" << endl;

    graphMatchProb = exp(fg.logScore(mpstate))/exp(mp.logZ());
    assert(graphMatchProb <= 1);

    return matchOK;
}

/*!
  @brief Computes histogram of an image without counting the pixels of color value as (0,0,0).
  */
void calcHistogram(Mat srcImg, Mat& hist, int bins=HIST_BIN_SIZE )
{
    hist = Mat(1, pow(bins,3), CV_32FC1, Scalar::all(0));

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for(int i=0; i<srcImg.rows; i++)
    {
        for(int j=0; j<srcImg.cols; j++)
     {
            Vec3b color = srcImg.at<Vec3b>(i,j);

            if(color == Vec3b(0,0,0))
                continue;

            // Compute the Bin
            int redBin = (int) (invBinSize * ( (unsigned char) color[2]));
            int greenBin = (int) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (int) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = (redBin * binSqr ) + ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > 0 )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}


void segmentImg_GMM(Mat inputImg, Ptr<EM> emModelFG, Ptr<EM> emModelBG, Mat& outImg, Point2f& meanVal)
{
    Mat samples;
    Rect imgRoi(0,0,inputImg.cols,inputImg.rows);

    for(int j=0; j<inputImg.cols; j++)
    {
        for(int k=0; k<inputImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = 100*double(inputImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(inputImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(inputImg.at<Vec3b>(k,j)[2])/255.0;

            samples.push_back(temp.row(0));
        }
    }


    Mat labelsTest;
    Mat probVals;
    segmentBodyParts_fgbg(emModelFG,emModelBG,samples,labelsTest,probVals);

    meanVal = Point2f(0,0);
//    int meanValCounter = 0;
    float totalWeight = 0;

    outImg = inputImg.clone();
    int counter1=0;
    for(int j=0; j<outImg.cols; j++)
    {
        for(int i=0; i<outImg.rows; i++)
        {
            int label = labelsTest.at<int>(counter1);

            if(label!=-1)
            {
                float weight = probVals.at<float>(counter1);
                outImg.at<Vec3b>(i,j) = inputImg.at<Vec3b>(i,j);
                meanVal += ( weight * Point2f(j,i));
//                meanValCounter++;
                totalWeight += weight;
            }
            else
                outImg.at<Vec3b>(i,j) = Vec3b(0,0,0);

            counter1++;
        }
    }

    meanVal *= (1.0/totalWeight);
}

/*!
  @brief Attaches two images horizontally as default, set the flag false to append vertically. Images don't need to be of same size,
    but same type.
  */
void attachImages(Mat img1, Mat img2, Mat& imgOut, bool horizontal = true)
{
    assert(img1.type() == img2.type());

    if(horizontal)
    {
        int numRows = img1.rows > img2.rows ? img1.rows : img2.rows;
        int numCols = img1.cols + img2.cols;

        imgOut = Mat::zeros(numRows,numCols,img1.type());

        img1.copyTo(imgOut.rowRange(0,img1.rows).colRange(0,img1.cols));
        img2.copyTo(imgOut.rowRange(0,img2.rows).colRange(img1.cols,numCols));
    }
    else
    {
        int numCols = img1.cols > img2.cols ? img1.cols : img2.cols;
        int numRows = img1.rows + img2.rows;

        imgOut = Mat::zeros(numRows,numCols,img1.type());

        img1.copyTo(imgOut.rowRange(0,img1.rows).colRange(0,img1.cols));
        img2.copyTo(imgOut.rowRange(img1.rows,numRows).colRange(0,img2.cols));
    }
}


/*!
  @brief Applies Connected Component Analysis on an image using Flood Fill technique. Not optimized.
  */
void connectedComponents_FloodFill(Mat inputImg, Mat& outImg, vector<Point2f>& blobCenters, vector<Vec3b>& blobColors)
{
    blobCenters.clear();
    blobColors.clear();

    Mat tempImg = inputImg.clone();
    Mat maskImg = Mat::zeros(inputImg.rows+2,inputImg.cols+2,CV_8UC1);
    Rect imgRect(0,0,inputImg.cols,inputImg.rows);
    outImg = Mat::zeros(inputImg.rows,inputImg.cols,inputImg.type());

    int diff = 25;

    // Iterate over all the pixels
    for(int j=0; j<inputImg.cols; j++)
    {
        for(int i=0; i<inputImg.rows; i++)
        {
            cv::Rect r1;
            inputImg = tempImg.clone();

            // Apply flood fill
            int pixelCount = cv::floodFill(inputImg,maskImg,Point2f(j,i),cv::Scalar(0,255,0),&r1,
                                           cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff),FLOODFILL_FIXED_RANGE);

            Vec3b color1 =  tempImg.at<Vec3b>(i,j);//getRandColor();
            Vec3f meanColor = color1;

            // Remove blobs of small size
            if(r1.area() < 0.1*imgRect.area())
                continue;

//            imshow("inputImg",inputImg);
//            waitKey(0);

            // Re-paint the image blobs with mean color
            int colorValCounter = 0;
            for(int l=0; l<inputImg.cols; l++)
            {
                for(int m=0; m<inputImg.rows; m++)
                {
                    if(inputImg.at<Vec3b>(m,l) == Vec3b(0,255,0))
                    {
                        outImg.at<Vec3b>(m,l) = color1;//Vec3b(color1[0],color1[1],color1[2]);
                        meanColor += tempImg.at<Vec3b>(m,l);
                        colorValCounter++;
                    }
                }
            }

            meanColor /= colorValCounter;

            blobCenters.push_back(0.5*(r1.tl()+r1.br()));
            blobColors.push_back(Vec3b(meanColor));
        }
    }
}

/*!
  @brief Generates samples for training FG and BG. Takes an expannded and contracted area for selecting FG and BG.
  It further adds samples to BG from other active agents windows.
  */
void generateSamples_GMM(Mat inputImg, Rect agentRoi, vector<Rect> activeAgentsWin, Mat& samplesFG, Mat& samplesBG)
{
    // Create an expanded ROI for bg image
    Rect expandedRoi = agentRoi;
    scaleRect(expandedRoi,1.4);
    checkBoundary(inputImg,expandedRoi);
    Mat bgImg = inputImg(expandedRoi).clone();

    Point2f bgCenter(expandedRoi.width/2,expandedRoi.height/2);

    Rect contractedRoi = agentRoi;
    scaleRect(contractedRoi,0.9);
    Rect agentRoi_bg = contractedRoi;        // agent ROI wrt BG image
    agentRoi_bg.x = bgCenter.x-agentRoi_bg.width/2;
    agentRoi_bg.y = bgCenter.y-agentRoi_bg.height/2;

    Mat fgImg = bgImg(agentRoi_bg).clone();
    RotatedRect fgEllipse(bgCenter,Size(agentRoi_bg.width,agentRoi_bg.height),0);

    vector<Point> polyPoints;
    ellipse2Poly(bgCenter,Size(agentRoi_bg.width/2,agentRoi_bg.height/2),0,0,360,10,polyPoints);

    // Training samples for foreground and background
    for(int j=0; j<bgImg.cols; j++)
    {
        for(int k=0; k<bgImg.rows; k++)
        {
            Mat temp(1,3,CV_64FC1);
            temp.at<double>(0,0) = 100*double(bgImg.at<Vec3b>(k,j)[0])/255.0;
            temp.at<double>(0,1) = 100*double(bgImg.at<Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(bgImg.at<Vec3b>(k,j)[2])/255.0;

            Point2f thisPoint(j,k);

            int check = pointPolygonTest(polyPoints,thisPoint,false);
            if(check == 1)
            {
                samplesFG.push_back(temp.row(0));
                bgImg.at<Vec3b>(k,j) = Vec3b(0,0,255);
            }
            else
            {
                samplesBG.push_back(temp.row(0));
            }
        }
    }


//    imshow("bgIMg",bgImg);
//    imshow("fgImg",inputImg(agentRoi));
//    waitKey(0);
/*
    int samplesPerWindow = 0;
    if(activeAgentsWin.size() > 1)
        samplesPerWindow = samplesBG.rows/(activeAgentsWin.size()-1);

    // Add more samples to background model corresponding to other agents
    for(int k2=0; k2<activeAgentsWin.size(); k2++)
    {
        RNG uniSamplerX, uniSamplerY;
        for(int j2=0; j2<samplesPerWindow; j2++)
        {
            Point2i pt1;
            pt1.x = uniSamplerX.uniform(activeAgentsWin[k2].x,activeAgentsWin[k2].br().x);
            pt1.y = uniSamplerY.uniform(activeAgentsWin[k2].y,activeAgentsWin[k2].br().y);

            if(agentRoi.contains(pt1))
                continue;
            else
            {
                Mat temp(1,3,CV_64FC1);
                temp.at<double>(0,0) = 100*double(inputImg.at<Vec3b>(pt1.y,pt1.x)[0])/255.0;
                temp.at<double>(0,1) = 100*double(inputImg.at<Vec3b>(pt1.y,pt1.x)[1])/255.0;
                temp.at<double>(0,2) = 100*double(inputImg.at<Vec3b>(pt1.y,pt1.x)[2])/255.0;

                samplesBG.push_back(temp.row(0));
            }
        }
    }
*/
}

/*!
  @brief Updates the sample set of FG and BG. It uses some percentage of samples from previous set
  and rest from the new set.
  */
void updateSamples_GMM(Mat& samplesFG, Mat& samplesBG, Mat samplesFGNew, Mat samplesBGNew, float oldSamplesRatio)
{
    Mat samples1, samples2;

    // Generate samples for FG
    int count1 = samplesFG.rows * oldSamplesRatio;
    int count2 = samplesFG.rows - count1;

    RNG randSamplerFG1;
    for(int i=0; i<count1; i++)
    {
        int index = randSamplerFG1.uniform(0,samplesFG.rows);
        samples1.push_back(samplesFG.row(index));
    }

    RNG randSamplerFG2;
    for(int j=0; j<count2; j++)
    {
        int index = randSamplerFG2.uniform(0,samplesFGNew.rows);
        samples1.push_back(samplesFGNew.row(index));
    }

    // Generate sampples for BG
    count1 = samplesBG.rows * oldSamplesRatio;
    count2 = samplesBG.rows - count1;

    RNG randSamplerBG1;
    for(int i=0; i<count1; i++)
    {
        int index = randSamplerBG1.uniform(0,samplesBG.rows);
        samples2.push_back(samplesBG.row(index));
    }

    RNG randSamplerBG2;
    for(int j=0; j<count2; j++)
    {
        int index = randSamplerBG2.uniform(0,samplesBGNew.rows);
        samples2.push_back(samplesBGNew.row(index));
    }

    samplesFG = samples1.clone();
    samplesBG = samples2.clone();
}


/*!
  @brief Trains the GMM model using OpenCv function. It takes minimum input and avoids unwanted variables.
  */
void trainGMM(Ptr<EM>& emModel, Mat samples, int components)
{
    vector<Mat> covs1;//(clusNumFG, Mat::eye(dims, dims, CV_64FC1)/10);
    Mat means1 = Mat::zeros(components, samples.cols, CV_64FC1);
    Mat weights1 = Mat::ones(1, components, CV_64FC1)/components;
    Mat out1, out2, out3;

#if(OPENCV3)
//    EM::Params emParams(components, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 100, 0.1));
//    emModel->create(emParams);
//    emModel->train(samples);//, means1, covs1, weights1, out1, out2, out3);
    emModel = EM::train( samples, noArray(), noArray(), noArray(),
            EM::Params(components, EM::COV_MAT_SPHERICAL,
                       TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1)));

#else
    *emModel = EM(components, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
    emModel->train(samples);//, means1, covs1, weights1, out1, out2, out3);
#endif
}

// Needs to be revisited
void filterDetections(Mat img1, vector<Rect>& detROIs, bool minAreaFlag = false)
{
    vector<Rect> filteredROIs;

    for(int i=0; i<detROIs.size(); i++)
    {
        bool dontAdd = false;

        Rect r1 = detROIs[i];
        for(int j=i+1; j<detROIs.size(); j++)
        {
            Rect r2 = detROIs[j];

            float overlap = 0;
            calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
            float minArea;
            if(minAreaFlag)
                minArea = r1.area() < r2.area() ? r1.area() : r2.area();
            else
                minArea = r1.area();

            overlap = overlap/float(minArea);

            if(overlap > 0.5)
            {
//                MultiTracker tempTracker;
//                Mat hist1, hist2;
//                tempTracker.computeColorDistributionRGB(img1,r1,tempTracker.hist);
//                hist1 = tempTracker.hist.clone();
//                tempTracker.computeColorDistributionRGB(img1,r2,tempTracker.hist);
//                hist2 = tempTracker.hist.clone();

//                float matchVal = 0;
//                computeBhattacharyaCoefficient(hist1,hist2,&matchVal);

//                if(matchVal > 0.8)
//                {
                    dontAdd = true;
//                }
            }
        }

        if(!dontAdd)
            filteredROIs.push_back(detROIs[i]);
    }

    detROIs.clear();
    for(int i=0; i<filteredROIs.size(); i++)
        detROIs.push_back(filteredROIs[i]);
}



/*!
  @brief Graph of color blobs based tracking.
  */
void track_colorBlobGraph(Mat img, MultiTracker tracker, string outputPath, Rect& outRect)
{
    Mat_<int> adjMatSrc;
    vector<Point2f> ccPoints;
    vector<Vec3b> ccColors;

    Mat segmentedImgSelectedROIs, graphImgOut,segmentedImgSrc;
    vector<int> selectedCompInd;

    findSourceBlobs_GMM(tracker.agentImgUpdated.clone(),tracker.m1,tracker.m4,
                        segmentedImgSelectedROIs,graphImgOut,adjMatSrc,ccPoints,ccColors,selectedCompInd,
                        segmentedImgSrc);

    Rect r2 = tracker.trackWindow;
    vector< vector<Point2f> > targetCandidates;
    vector< vector<Vec3b> > targetCandColors;
    Mat segmentedBlobsTarget, segmentedImgTarget;

    findTargetCandidateBlobs(img(r2).clone(),tracker.m1,tracker.m4,
                             selectedCompInd,targetCandidates,targetCandColors,segmentedBlobsTarget,
                             segmentedImgTarget);

    vector<int> matchIndex;
    Mat_<int> adjMatOut;
    Mat outImgtemp = img(r2).clone();
    float graphMatchProb = 0;

    bool matchOK = findMatch_MRF_MAP(adjMatSrc,ccPoints,ccColors,targetCandidates,targetCandColors,matchIndex,adjMatOut,
                                     graphMatchProb,outputPath);

    if(matchOK)
    {
        for(uint i=0; i<adjMatOut.rows; i++)
            for(uint j=i+1; j<adjMatOut.cols; j++)
            {
                if(adjMatOut[i][j]==1)
                {
                    cv::line(outImgtemp,targetCandidates[i][matchIndex[i]],targetCandidates[j][matchIndex[j]],
                             cv::Scalar(0,255,255),2);
                }
            }

//        graphMatchVal = int(matchOK);
        outRect = r2;
    }
    else
        outRect = Rect(0,0,0,0);

//    imshow("outimg",outImgtemp);
//    imshow("graphImgSrc",graphImgOut);
//    imshow("segmentTarget",segmentedBlobsTarget);
//    imshow("srcBlobs",segmentedImgSelectedROIs);
//    imshow("srcSegmentImg",segmentedImgSrc);
//    imshow("targetSegment",segmentedImgTarget);
//    waitKey(0);
}


/*!
  @brief Finds the most overlapping window for the given window in an image.
  */
void findMostOverlappingWindow(Rect r1, vector<Rect> windows, int& maxOverlapIndex, float& maxOverlap, int skipIndex = -1)
{
    maxOverlap = 0;
    maxOverlapIndex = -1;
    for(int i1=0; i1<windows.size(); i1++)
    {
        if(skipIndex == i1)
            continue;

        Rect r2 = windows[i1];
        float overlap = 0;

        calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
        float minArea = r1.area() < r2.area() ? r1.area() : r2.area();
        overlap = overlap/minArea;

        if(overlap > maxOverlap)
        {
            maxOverlap = overlap;
            maxOverlapIndex = i1;
        }
    }
}

#if(HUNGARIAN)

/*!
  @brief Solve associations between trackers and detections.
  */
void solveAssociation(Mat img, vector<Rect> trackers, vector<Rect> detections, vector<Rect>& associatedRect)
{
    Mat reasonMat(trackers.size(),detections.size(),CV_32FC1,Scalar::all(0));
    associatedRect.assign(trackers.begin(),trackers.end());

    // Calculate Reasoning Matrix elements
    for(int i1=0; i1<reasonMat.rows; i1++)
    {
        Rect r1 = trackers[i1];
        MultiTracker tempTracker;
        Mat hist1;
        tempTracker.computeColorDistributionRGB(img,r1,tempTracker.hist);
        hist1 = tempTracker.hist.clone();

        for(int j1=0; j1<reasonMat.cols; j1++)
        {
            Rect r2 = detections[j1];
            Mat hist2;
            tempTracker.computeColorDistributionRGB(img,r2,tempTracker.hist);
            hist2 = tempTracker.hist.clone();

            float matchVal = 0;
            computeBhattacharyaCoefficient(hist1,hist2,&matchVal);

            float overlap = 0;
            calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
            overlap = overlap/(float)(r1.area());

            float scale = (float)(r1.area())/((float)r2.area());

            if(overlap>0 && scale>0.5 && scale<2)
            {
                reasonMat.at<float>(i1,j1) = 0.5 * overlap + 0.5 * matchVal;
            }
        }
    }

    // Solve the association problem using Hunagrian Method
    if(reasonMat.rows > 0 && reasonMat.cols > 0)
    {
        Matrix<double> reasonMat_HM(reasonMat.rows, reasonMat.cols);
        for(int i=0; i<reasonMat.rows; i++)
            for(int j=0; j<reasonMat.cols; j++)
            {
                reasonMat_HM(i,j) = reasonMat.at<float>(i,j) == 0 ? 100: 1-reasonMat.at<float>(i,j);
            }

        Munkres m_HM;
        m_HM.solve(reasonMat_HM);



        for(int i=0; i<reasonMat.rows; i++)
            for(int j=0; j<reasonMat.cols; j++)
                reasonMat.at<float>(i,j) = reasonMat_HM(i,j) == -1 || reasonMat.at<float>(i,j) == 0 ? 0 : 1;
    }


    // Mark the trackers that have been assigned a detection
    for(int i1=0; i1<reasonMat.rows; i1++)
    {
        for(int j1=0; j1<reasonMat.cols; j1++)
        {
            if(reasonMat.at<float>(i1,j1) == 1)
            {
                associatedRect[i1] = detections[j1];
                break;
            }
        }
    }
}

#endif

/*!
  @brief Track the target locally using SURF features
  */
void track_SURF(Mat img, MultiTracker tracker, Rect& surfRoiOut)
{
#if(OPENCV3)
    Ptr<SURF> mySurf;
    mySurf->setExtended(0);
    mySurf->setUpright(1);
#else
    SURF* mySurf;
#endif

    Mat surfSrcImg = tracker.agentImgUpdated;
    Rect r1(0,0,surfSrcImg.cols,surfSrcImg.rows);
    vector<KeyPoint> kp1;
    Mat desc1;

    mySurf->detect(surfSrcImg,kp1);
    mySurf->compute(surfSrcImg,kp1,desc1);

    Rect r2 = tracker.KF_RectPredicted;
    Mat surfDstImg = img(r2);
    vector<KeyPoint> kp2;
    Mat desc2;

    mySurf->detect(surfDstImg,kp2);
    mySurf->compute(surfDstImg,kp2,desc2);

    Point2f surfCenter = 0.5 * (r1.tl() + r1.br());
    Mat surfImgDraw;
    double surfPercentMatch = surfDesc_Matching1(surfSrcImg,surfDstImg,kp1,kp2,desc1,desc2,surfCenter,
                                                 surfRoiOut,surfImgDraw);

    float surfARerr=0,surfScale=0,roiConf=0;
    bool surfTestOK = verifyResultantRoi(r1,surfRoiOut,surfARerr,surfScale,roiConf);

    if(!surfTestOK || surfPercentMatch < 30)
    {
        surfPercentMatch = 0;
        surfRoiOut = Rect(0,0,0,0);
    }
    else if(surfPercentMatch > 30)
    {
        surfRoiOut.x += r2.x;
        surfRoiOut.y += r2.y;
    }
}

void predictAgent(MultiTracker& tracker, int frameNum)
{
    if(tracker.initialPrediction.area() == 0)
    {
        setIdentity(tracker.KF_CV.measurementMatrix,Scalar::all(0));
        Mat measure = Mat::zeros(2, 1, CV_32F);
        measure.convertTo(measure,CV_32FC1);
        tracker.KF_CV.correct(measure);

        tracker.KF_CV.predict();

        tracker.KF_RectPredicted.x = tracker.KF_CV.statePost.at<float>(0)-tracker.KF_RectPredicted.width/2;
        tracker.KF_RectPredicted.y = tracker.KF_CV.statePost.at<float>(1)-tracker.KF_RectPredicted.height/2;
#if(KF_4)
        tracker.KF_RectPredicted.width = tracker.KF_CV.statePost.at<float>(2);
        tracker.KF_RectPredicted.height = tracker.KF_CV.statePost.at<float>(3);
#else
        tracker.KF_RectPredicted.width = tracker.trackWindow.width;
        tracker.KF_RectPredicted.height = tracker.trackWindow.height;
#endif

        tracker.trackWindow = tracker.KF_RectPredicted;

        tracker.exitTimeSpan++;
    }
    else
    {
        tracker.trackWindow = tracker.initialPrediction;
#if(SLC_PRED)
        tracker.exitTimeSpan++;
#endif
    }

    if(tracker.agentState != 5)
    {
        tracker.frameIds.push_back(frameNum);
        tracker.frameROIs.push_back(tracker.KF_RectPredicted);
        tracker.frameObservationFlag.push_back(0);
    }
    tracker.initialPrediction = Rect(0,0,0,0);
}


/*!
  @brief Finds factors edge potential for graph matching.
  */
void getFactors_TS3(ofstream& factorFile,Mat_<int> adjMat,vector<Point2f> points1, vector<Point2f> points2,
                    vector<Mat> hists1, vector<Mat> hists2, vector<Rect> rects1, vector<Rect> rects2,
                    vector<int> dummyFlag, vector<int> dummyVarIndex, vector< Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                Mat_<float> truthTable(points2.size(),points2.size(),0.0);
                // for this particular case, the required angle between nodes is -90

                // Find the angle between source nodes
                float theta1 = float(180 * 7.0/22.0) *
//                        ( atan2( (points1[i].y - points1[j].y), (points1[i].x - points1[j].x) ) );
                        ( atan( (points1[i].y - points1[j].y) / (points1[i].x - points1[j].x) ) );

                bool right = false; // to determine if j is in left or right of i
                if(points1[j].x > points1[i].x)
                    right = true;

                if(isnan(theta1))
                    theta1 = -90;

                float r1 = norm(points1[i]-points1[j]);

                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << points2.size() << " " << points2.size() << endl; // Cardinality of each variable

                int factorCounter=0;
                vector<float> nonZeroStateVal;
                vector<int> nonZeroStateIndex;

                float allZeroStateVal;
                int allZeroStateIndex;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<points2.size(); m++) // corresponds to j
                {
                    for(int l=0; l<points2.size(); l++) // corresponds to i
                    {
                        // Find the angle between candidate nodes
                        float theta2 = float(180 * 7.0/22.0) *
//                                ( atan2( (points2[l].y - points2[m].y), (points2[l].x - points2[m].x) ) );
                                ( atan( (points2[l].y - points2[m].y) / (points2[l].x - points2[m].x) ) );

                        if(isnan(theta2))
                            theta2 = -90;

                        float r2 = norm(points2[l]-points2[m]);
                        float rErr = r1 > r2 ? r2/r1 : r1/r2;

                        float phi1 = 0;
                        computeBhattacharyaCoefficient(hists1[i],hists2[l],&phi1);
                        float phi2 = 0;
                        computeBhattacharyaCoefficient(hists1[j],hists2[m],&phi2);

                        float scale1 = float(rects1[i].area())/float(rects2[l].area());
                        float scale2 = float(rects1[j].area())/float(rects2[m].area());

                        scale1 = scale1 > 1 ? 1.0/scale1 : scale1;
                        scale2 = scale2 > 1 ? 1.0/scale2 : scale2;

                        // Calcualte edge potential
                        float psi = exp(- 5*abs(theta1-theta2)/100 - 2.5*abs(1-r2/r1)/2 ) * phi1 * phi2 * scale1 * scale2;

                        if(isnan(psi))
                            psi = 0.00001;

                        bool testRight = false;
                        if(points2[m].x > points2[l].x)
                            testRight = true;

                        if(dummyFlag[l] == 1)
                            psi *= 0.25;
                        if(dummyFlag[m] == 1)
                            psi *= 0.25;

//                        cout << factorCounter << "\t" << i << "\t" << j << "\t" << l << "\t" << m << "\t" << theta1 << "\t" <<
//                                theta2 << "\t" << psi << "\t" << abs(theta1-theta2) << "\t" << phi1 << "\t" <<
//                                phi2 << "\t" << exp(-5*(phi1)/100) << "\t" <<
//                                r1 << "\t" << abs(1-r2/r1) << "\t" << r2 << "\t" << exp(-2.5*abs(1-r2/r1)) << "\t" <<
//                                scale1 << "\t" << scale2 << endl;


                        bool dummyEntry = false;
                        if(dummyVarIndex[i] == l && dummyVarIndex[j] == m )
                        {
                            if(psi == 0)
                                psi = 0.00001;  // Test if it is required and why?
                            allZeroStateVal = psi;
                            allZeroStateIndex = factorCounter;
                            dummyEntry = true;
                        }

                        // if it is too less, punish further, possibly an outlier
                        if( !dummyEntry && (psi < 0.1 || l==m || right != testRight) )
                        {
                            psi = 0.0;
                        }

                        if(psi != 0 || dummyEntry)
                        {
                            nonZeroStateVal.push_back(psi);
                            nonZeroStateIndex.push_back(factorCounter);
                        }

//                        cout << psi << endl;

                        truthTable(l,m) = psi;

                        factorCounter++;
                    }
                }
                edgePotentials.push_back(truthTable);

                if(nonZeroStateVal.size() != 0)
                {
                factorFile <<  nonZeroStateVal.size() << endl; // Non Zero states
                for(int i1=0; i1<nonZeroStateVal.size(); i1++)
                    factorFile << nonZeroStateIndex[i1] << " " << nonZeroStateVal[i1] << endl; // factor state index and value
                }
                else
                {
                    factorFile << 1 << endl; // Dummy state
                    factorFile << allZeroStateIndex << " " << allZeroStateVal << endl; // factor state index and value
                }
                factorFile << "\n" << endl;
            }
        }
}


/*!
  @brief Finds MRF-MAP solution for the graph.
  */
bool matchGraph_TS3(vector<Point2f> points1, vector<Point2f> points2, vector<Mat> hists1, vector<Mat> hists2,
                    vector<Rect> rects1, vector<Rect> rects2, Mat_<int> adjMat, string outputPath, vector<int> dummyFlag,
                    vector<int> dummyVarIndex, vector<int>& matchIndex, Mat_<int>& adjMatOut, float& graphMatchProb)
{
    int factorCounter = 0;
    // Find the number of factors in the graph
    for(int i1=0; i1<adjMat.rows; i1++)
        for(int j1=i1+1; j1<adjMat.cols; j1++)
        {
            if(adjMat[i1][j1] != -1)
                factorCounter++;
        }

    // File to store the factors in a particular format
    ofstream factorFile;
    string factorFileName = outputPath;
    factorFileName.append("factor_TS3.fg");
    factorFile.open(factorFileName.c_str());

    factorFile << factorCounter << "\n" << endl;    // Pass the total number of factors

    vector< Mat_<float> > edgePotentials;

    // Function for calculating factors
    getFactors_TS3(factorFile,adjMat,points1,points2,hists1,hists2,rects1,rects2,dummyFlag,dummyVarIndex,edgePotentials);

    // Reading factors from the file (written above)
    dai::FactorGraph fg;
    fg.ReadFromFile(factorFileName.c_str());

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 0;

    // Store the constants in a PropertySet object
    dai::PropertySet opts;
    opts.set("maxiter",maxiter);  // Maximum number of iterations
    opts.set("tol",tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)

    // Construct another JTree (junction tree) object that is used to calculate
    // the joint configuration of variables that has maximum probability (MAP state)
    dai::JTree mp( fg, opts("updates",string("HUGIN"))("inference",string("MAXPROD")) );
//    dai::BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));

    // Initialize junction tree algorithm
    mp.init();
    // Run junction tree algorithm
    mp.run();
    // Calculate joint state of all variables that has maximum probability

    // Calculate joint state of all variables that has maximum probability
    // based on the max-product result
    vector<size_t> mpstate = mp.findMaximum();

    // Initialize the output variables
    matchIndex.assign(adjMat.rows,-1);

    for( size_t i = 0; i < mpstate.size(); i++ )
    {
        int nodeLabel = fg.var(i).label();

        matchIndex[nodeLabel] = mpstate[i];
    }

    adjMatOut = Mat_<int>(adjMat.rows,adjMat.cols,-1);

    bool matchOK = false;
    double logScore = 0;
    for( size_t I = 0; I < fg.nrFactors(); I++ )
    {
        // Find labels of the variables involved in the factor
        int nodeLabel1 = dai::Var(fg.factor(I).vars().begin()[0]).label();
        int nodeLabel2 = dai::Var(fg.factor(I).vars().begin()[1]).label();

        // Find indicies of the variables involved in the factor
        int nodeIndex1 = fg.findVar(fg.factor(I).vars().begin()[0]);
        int nodeIndex2 = fg.findVar(fg.factor(I).vars().begin()[1]);

        // Report max-product factor marginals
//        cout << "Approximate (max-product) MAP factor marginals:" << endl;
//        cout << mp.belief(fg.factor(I).vars()) << " == " << mp.beliefF(I) << endl;
//        cout << mp.beliefV(nodeLabel1) << "\t" << mp.beliefV(nodeLabel2) << endl;

        double assignedEdgePotential = edgePotentials[I](mpstate[nodeIndex1],mpstate[nodeIndex2]);
        // Mark the successful matched edges
        if(assignedEdgePotential > 0.1)
        {
            adjMatOut[nodeLabel1][nodeLabel2] = 1;
            matchOK = true;
        }

        logScore += log(assignedEdgePotential);

        Scalar edgePotentialSum = sum(edgePotentials[I]);

    }

    // Report log partition sum of fg, approximated by the belief propagation algorithm
//    cout << "Approximate (loopy belief propagation) log partition sum: " << mp.logZ() << endl;
//    cout << "Approximate (max-product) MAP state (log score = " << fg.logScore( mpstate ) << "):" << endl;

//    graphMatchProb = exp(fg.logScore(mpstate))/exp(mp.logZ());
//    assert(graphMatchProb <= 1);

    return matchOK;
}


/*!
  @brief Initialize the Kalman Filter for new pairs
  */
void pairKFinit(KalmanFilter& KFtemp, float initVal)
{
    KFtemp.init(2,1,0);
    int dt1 = 0.05;

    KFtemp.transitionMatrix = (Mat_<float>(2, 2) << 1,dt1,0,1); // A

    setIdentity(KFtemp.measurementMatrix); // H
    setIdentity(KFtemp.processNoiseCov, Scalar::all(0.1)); // Q
    setIdentity(KFtemp.measurementNoiseCov, Scalar::all(1)); // R
    setIdentity(KFtemp.errorCovPost, Scalar::all(0.1)); // P

    Mat measure(1,1,CV_32FC1,Scalar(initVal));
    KFtemp.statePre.at<float>(0) = initVal;
    KFtemp.correct(measure);
    KFtemp.predict();

}

#if(SLC_PRED)

/*!
  @brief Finds factors edge potential for graph matching for SLC Prediction.
  */
void getFactors_SLC(ofstream& factorFile,Mat_<int> adjMat,vector<Point2f> points1, vector< vector<Point2f> > points2,
                    vector<Mat> hists1, vector< vector<Mat> > hists2, vector< Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                Mat_<float> truthTable(points2[i].size(),points2[j].size(),0.0);

                // Find the angle between source nodes
                float theta1 = float(180 * 7.0/22.0) *
//                        ( atan2( (points1[i].y - points1[j].y), (points1[i].x - points1[j].x) ) );
                        ( atan( (points1[i].y - points1[j].y) / (points1[i].x - points1[j].x) ) );

                bool right = false; // to determine if j is in left or right of i
                if(points1[j].x > points1[i].x)
                    right = true;

                if(isnan(theta1))
                    theta1 = -90;

                float r1 = norm(points1[i]-points1[j]);

                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << points2[i].size() << " " << points2[j].size() << endl; // Cardinality of each variable

                int factorCounter=0;
                vector<float> nonZeroStateVal;
                vector<int> nonZeroStateIndex;

                float allZeroStateVal=0;
                int allZeroStateIndex=-1;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<points2[j].size(); m++) // corresponds to j
                {
                    for(int l=0; l<points2[i].size(); l++) // corresponds to i
                    {
                        // Find the angle between candidate nodes
                        float theta2 = float(180 * 7.0/22.0) *
//                                ( atan2( (points2[l].y - points2[m].y), (points2[l].x - points2[m].x) ) );
                                ( atan( (points2[i][l].y - points2[j][m].y) / (points2[i][l].x - points2[j][m].x) ) );

                        if(isnan(theta2))
                            theta2 = -90;

                        float r2 = norm(points2[i][l]-points2[j][m]);
                        float rErr = r1 > r2 ? r2/r1 : r1/r2;

                        float phi1 = 0;
                        computeBhattacharyaCoefficient(hists1[i],hists2[i][l],&phi1);
                        float phi2 = 0;
                        computeBhattacharyaCoefficient(hists1[j],hists2[j][m],&phi2);

                        // Calcualte edge potential
                        float psi = /*exp(- 5*abs(theta1-theta2)/100 - 2.5*abs(1-r2/r1)/2 ) **/ phi1 * phi2;

//                        cout << factorCounter << "\t" << i << "\t" << j << "\t" << l << "\t" << m << "\t" << theta1 << "\t" <<
//                                theta2 << "\t" << psi << "\t" << abs(theta1-theta2) << "\t" << phi1 << "\t" <<
//                                phi2 << "\t" << exp(-5*(phi1)/100) << "\t" <<
//                                r1 << "\t" << abs(1-r2/r1) << "\t" << r2 << "\t" << exp(-2.5*abs(1-r2/r1)) << "\t" <<
//                                endl;

                        if(isnan(psi))
                            psi = 0.00001;

                        bool testRight = false;
                        if(points2[j][m].x > points2[i][l].x)
                            testRight = true;

                        // if it is too less, punish further, possibly an outlier
                        if( psi < 0.01 || right != testRight)
                        {
                            psi = 0.0;
                        }

                        if(psi != 0)
                        {
                            nonZeroStateVal.push_back(psi);
                            nonZeroStateIndex.push_back(factorCounter);
                        }
                        else
                        {
                            allZeroStateVal = 0.001;
                            allZeroStateIndex = factorCounter;
                        }

//                        cout << psi << endl;

                        truthTable(l,m) = psi;

                        factorCounter++;
                    }
                }
                edgePotentials.push_back(truthTable);

                if(nonZeroStateVal.size() != 0)
                {
                factorFile <<  nonZeroStateVal.size() << endl; // Non Zero states
                for(int i1=0; i1<nonZeroStateVal.size(); i1++)
                    factorFile << nonZeroStateIndex[i1] << " " << nonZeroStateVal[i1] << endl; // factor state index and value
                }
                else
                {
                    factorFile << 1 << endl; // Dummy state
                    factorFile << allZeroStateIndex << " " << allZeroStateVal << endl; // factor state index and value
                }
                factorFile << "\n" << endl;
            }
        }
}


/*!
  @brief Finds MRF-MAP solution for the graph for SLC Prediction.
  */
bool matchGraph_SLC(vector<Point2f> points1, vector< vector<Point2f> > points2, vector<Mat> hists1, vector< vector<Mat> > hists2,
                    Mat_<int> adjMat, string outputPath, vector<int>& matchIndex, Mat_<int>& adjMatOut, float& graphMatchProb)
{
    int factorCounter = 0;
    // Find the number of factors in the graph
    for(int i1=0; i1<adjMat.rows; i1++)
        for(int j1=i1+1; j1<adjMat.cols; j1++)
        {
            if(adjMat[i1][j1] != -1)
                factorCounter++;
        }

    // File to store the factors in a particular format
    ofstream factorFile;
    string factorFileName = outputPath;
    factorFileName.append("factor_SLC.fg");
    factorFile.open(factorFileName.c_str());

    factorFile << factorCounter << "\n" << endl;    // Pass the total number of factors

    vector< Mat_<float> > edgePotentials;

    // Function for calculating factors
    getFactors_SLC(factorFile,adjMat,points1,points2,hists1,hists2,edgePotentials);

    // Reading factors from the file (written above)
    dai::FactorGraph fg;
    fg.ReadFromFile(factorFileName.c_str());

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 0;

    // Store the constants in a PropertySet object
    dai::PropertySet opts;
    opts.set("maxiter",maxiter);  // Maximum number of iterations
    opts.set("tol",tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)

    // Construct another JTree (junction tree) object that is used to calculate
    // the joint configuration of variables that has maximum probability (MAP state)
    dai::JTree mp( fg, opts("updates",string("HUGIN"))("inference",string("MAXPROD")) );
//    dai::BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));

    // Initialize junction tree algorithm
    mp.init();
    // Run junction tree algorithm
    mp.run();
    // Calculate joint state of all variables that has maximum probability

    // Calculate joint state of all variables that has maximum probability
    // based on the max-product result
    vector<size_t> mpstate = mp.findMaximum();

    // Initialize the output variables
    matchIndex.assign(adjMat.rows,-1);

    for( size_t i = 0; i < mpstate.size(); i++ )
    {
        int nodeLabel = fg.var(i).label();

        matchIndex[nodeLabel] = mpstate[i];
    }

    adjMatOut = Mat_<int>(adjMat.rows,adjMat.cols,-1);

    bool matchOK = false;
    double logScore = 0;
    for( size_t I = 0; I < fg.nrFactors(); I++ )
    {
        // Find labels of the variables involved in the factor
        int nodeLabel1 = dai::Var(fg.factor(I).vars().begin()[0]).label();
        int nodeLabel2 = dai::Var(fg.factor(I).vars().begin()[1]).label();

        // Find indicies of the variables involved in the factor
        int nodeIndex1 = fg.findVar(fg.factor(I).vars().begin()[0]);
        int nodeIndex2 = fg.findVar(fg.factor(I).vars().begin()[1]);

        // Report max-product factor marginals
//        cout << "Approximate (max-product) MAP factor marginals:" << endl;
//        cout << mp.belief(fg.factor(I).vars()) << " == " << mp.beliefF(I) << endl;
//        cout << mp.beliefV(nodeLabel1) << "\t" << mp.beliefV(nodeLabel2) << endl;

        double assignedEdgePotential = edgePotentials[I](mpstate[nodeIndex1],mpstate[nodeIndex2]);
        // Mark the successful matched edges
        if(assignedEdgePotential > 0.1)
        {
            adjMatOut[nodeLabel1][nodeLabel2] = 1;
            matchOK = true;
        }

        logScore += log(assignedEdgePotential);

        Scalar edgePotentialSum = sum(edgePotentials[I]);

    }

    // Report log partition sum of fg, approximated by the belief propagation algorithm
//    cout << "Approximate (loopy belief propagation) log partition sum: " << mp.logZ() << endl;
//    cout << "Approximate (max-product) MAP state (log score = " << fg.logScore( mpstate ) << "):" << endl;

//    graphMatchProb = exp(fg.logScore(mpstate))/exp(mp.logZ());
//    assert(graphMatchProb <= 1);

    return matchOK;
}

/*!
  @brief Generates random locations for target prediction.
  */
bool getRandomTargets(Mat img, map<int,MultiTracker>& Tracker,  vector< vector<Point2f> >& points2_pred,
                      vector< vector<Mat> >& hists2_pred, vector< vector<Rect> >& rects2_pred)
{
    Mat testTempImg = img.clone();
    bool doPred = false;

    int loopCounter = 0;
    for(map<int,MultiTracker>::iterator i3=Tracker.begin(); i3!=Tracker.end(); i3++)
    {
        int id1 = i3->first;
//        if(Tracker[id1].activeTimeSpan < 1)
//            continue;
        Rect testRect = Tracker[id1].trackWindow;
        checkBoundary(img,testRect);
        Point2f tempPt = 0.5 * (testRect.tl() + testRect.br());
        Mat histPred;
        calcHistogram(img(testRect).clone(),histPred);
        points2_pred[loopCounter].push_back(tempPt);
        hists2_pred[loopCounter].push_back(histPred);
        rects2_pred[loopCounter].push_back(testRect);

        if(Tracker[id1].status == false)
        {
            doPred = true;
            // Generate random points for each
            int numPoints = 9;

            RNG randNum;
            float varX = 0.2*testRect.width/2;
            float varY = 0.1*testRect.height/2;
            float scaleVar = 0.2;

            for(int j3=0; j3<numPoints; j3++)
            {
                float x1 = randNum.uniform(tempPt.x-varX,tempPt.x+varX);
                float y1 = randNum.uniform(tempPt.y-varY,tempPt.y+varY);
                //            float scaleFactor =  randNum.uniform(0.05,scaleVar);
                //            float w1 = testRect.width *

                Rect rectPred = testRect;
                rectPred.x = x1 - testRect.width/2;
                rectPred.y = y1 - testRect.height/2;
                checkBoundary(img,rectPred);

                calcHistogram(img(rectPred).clone(),histPred);

                points2_pred[loopCounter].push_back(Point2f(x1,y1));
                hists2_pred[loopCounter].push_back(histPred);
                rects2_pred[loopCounter].push_back(rectPred);

                rectangle(testTempImg,rectPred,getRandColor(),2,LINE_AA);
            }
        }
        loopCounter++;
    }

//    namedWindow("predOptions",WINDOW_NORMAL);
//    imshow("predOptions",testTempImg);

    return doPred;
}

/*!
  @brief Predicts a position of object for given options and spatial constraints
  */
void prediction_SLC(Mat img, map<int,MultiTracker>& Tracker, vector<Point2f> points1, Mat_<int> adjMat, vector<Mat> hists1,
                    string outputPath)
{
    vector< vector<Point2f> > points2_pred(points1.size());
    vector< vector<Mat> > hists2_pred(points1.size());
    vector< vector<Rect> > rects2_pred(points1.size());
    bool doPred = getRandomTargets(img,Tracker,points2_pred,hists2_pred,rects2_pred);

    if(doPred)
    {
        vector<int> matchIndex;
        Mat_<int> adjMatOut;
        float graphProb=0;
        bool matchOK = matchGraph_SLC(points1,points2_pred,hists1,hists2_pred,adjMat,outputPath,matchIndex,adjMatOut,graphProb);

        Mat outImgtemp = img.clone();
        if(matchOK)
        {
            // Draw the matching edges
            for(uint i=0; i<adjMatOut.rows; i++)
                for(uint j=i+1; j<adjMatOut.cols; j++)
                {
                    if(adjMatOut[i][j]==1)
                    {
                        cv::line(outImgtemp,points2_pred[i][matchIndex[i]],points2_pred[j][matchIndex[j]],
                                 cv::Scalar(0,255,255),2,LINENAME);
                    }
                }

            // Draw the match points
            for(int i1=0; i1<matchIndex.size(); i1++)
            {
                if(matchIndex[i1] != -1)
                {
                    circle(outImgtemp,points2_pred[i1][matchIndex[i1]],2,Scalar(0,255,0),2,LINENAME);
                    rectangle(outImgtemp,rects2_pred[i1][matchIndex[i1]],Scalar(255,255,255),2,LINENAME);
                }
            }
            //        graphMatchVal = int(matchOK);

            int loopCounter = 0;
            for(map<int,MultiTracker>::iterator i3=Tracker.begin(); i3!=Tracker.end(); i3++)
            {
                int id1 = i3->first;
                if(Tracker[id1].status == false)
                {
                    Tracker[id1].initialPrediction = rects2_pred[loopCounter][matchIndex[loopCounter]];
                }
                loopCounter++;
            }
        }

//        namedWindow("graphMatch",WINDOW_NORMAL);
//        imshow("graphMatch",outImgtemp);
//        waitKey(0);
    }
}

#endif

/*!
  @brief Updates the Node Relations (i.e. dij, occ_prob and dead agents) for all the trackers.
  */
void updateNodeRelations(map<int,MultiTracker>& Tracker, map< pair<int,int>, KalmanFilter>& relation2)
{
    map<int,MultiTracker>::iterator i1,j1;

    for(i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = (*i1).first;
        if(Tracker[id1].agentState == 5)
            continue;
        Rect r1 = Tracker[id1].trackWindow;
        Point2f c1 = 0.5 * (r1.tl() + r1.br());

        j1 = i1;
        j1++;
        for(; j1!=Tracker.end(); j1++)
        {
            int id2 = j1->first;
            if(Tracker[id2].agentState == 5)
                continue;
            Rect r2 = Tracker[id2].trackWindow;
            Point2f c2 = 0.5 * (r2.tl() + r2.br());
\
            // Don't consider the node relations until trackers start overlapping each other
//            float olap = findOverlap(r1,r2);
//            if(olap == 0)
//                continue;

            float dijNow = norm(c1-c2);
#if(KF_PAIRS)
            Point3f c1_(c1.x,c1.y,r1.area()/10000.f);
            Point3f c2_(c2.x,c2.y,r2.area()/10000.f);
            float dijNow_ = norm(c1_-c2_);
#endif
            if(Tracker[id1].dead || Tracker[id2].dead)
            {
                Tracker[id1].relation.erase(id2);
                Tracker[id2].relation.erase(id1);

                Tracker[id1].relationROIs.erase(id2);
                Tracker[id2].relationROIs.erase(id1);
#if(KF_PAIRS)
                relation2.erase(pair<int,int>(id1,id2));
                relation2.erase(pair<int,int>(id2,id1));
#endif
                continue;
            }
            else if(Tracker[id1].activeTimeSpan > 1 && Tracker[id2].activeTimeSpan > 1)
            {
                float dijOld = Tracker[id1].relation[id2].first;
                float prior = Tracker[id1].relation[id2].second;

                float delta_dij = dijNow - dijOld;
                float deno = dijNow > dijOld ? dijNow : dijOld;

                float likelihood = (1-delta_dij/deno);
                float evidence = likelihood * prior + (2-likelihood) * (1-prior);

                float posterior = (likelihood * prior) / evidence;

                Tracker[id1].relation[id2].second = posterior;
                Tracker[id2].relation[id1].second = posterior;

                Tracker[id1].relation[id2].first = dijNow;
                Tracker[id2].relation[id1].first = dijNow;

#if(KF_PAIRS)
//                cout << id1 << "\t" << id2 << "\t" <<
//                        relation2[pair<int,int>(id1,id2)].statePost.at<float>(0)
//                        << "\t" << Tracker[id2].relation[id1].first << "\t" << Tracker[id2].relation[id1].second << endl;

                if(Tracker[id1].coPedId == -1)
                {
                    Mat measure(1,1,CV_32FC1,Scalar(dijNow_));

                    relation2[pair<int,int>(id1,id2)].correct(measure);
                    relation2[pair<int,int>(id2,id1)].correct(measure);

                    relation2[pair<int,int>(id1,id2)].predict();
                    relation2[pair<int,int>(id2,id1)].predict();
                }
                // Test ioo
//                else if(Tracker[id1].activeTimeSpan > 2 &&  Tracker[id2].activeTimeSpan > 2)
//                {
//                    KalmanFilter tempKF;
//                    pairKFinit(tempKF,relation2[pair<int,int>(id1,id2)].statePost.at<float>(0));
//                    for(int i2=0; i2<5; i2++)
//                    {
//                        tempKF.predict();
//                        float dval = tempKF.statePre.at<float>(0) ;
//                        if(dval<=0)
//                        {
//                            cerr << "Test Success for " << id1 << "\t" << id2 << endl;
//                            break;
//                        }
//                        tempKF.correct(Mat());
//                    }
//                }

                if(Tracker[id1].activeTimeSpan > 10 && Tracker[id2].activeTimeSpan > 10)
                {
                    // Initial windows
                    Rect r1 = Tracker[id2].relationROIs[id1];
                    Rect r2 = Tracker[id1].relationROIs[id2];

                    Point2f c1 = 0.5 * (r1.tl() + r1.br());
                    Point2f c2 = 0.5 * (r2.tl() + r2.br());

                    Point3f c1_(c1.x,c1.y,r1.area()/10000.f);
                    Point3f c2_(c2.x,c2.y,r2.area()/10000.f);

                    // Current windows
                    Rect r1_ = Tracker[id1].trackWindow;
                    Rect r2_ = Tracker[id2].trackWindow;

                    // Scale change over time
                    float scale1 = float(r1.area())/float(r1_.area());
                    float scale2 = float(r2.area())/float(r2_.area());

                    // Scale change between two agents
                    float scaleChange = float(r1_.area()) / float(r2_.area());
                    scaleChange = scaleChange > 1.0 ? 1/scaleChange : scaleChange;

                    if( ((scale1 > 1 && scale2 > 1) || (scale1 < 1 && scale2 < 1) ) && scaleChange > 0.5
                            && abs(r1_.x-r2_.x) < r1_.width*1.2 )
                    {
                        float dVal = norm(c1-c2);
                        float dValNow = relation2[pair<int,int>(id1,id2)].statePost.at<float>(0);

                        if(abs(dVal-dValNow) < 0.1*r1_.width)
                        {
//                            cerr << "------>>> C0-Peds " << id1 << "\t" << id2 << endl;
                            Tracker[id1].coPedId = id2;
                            Tracker[id2].coPedId = id1;
                        }

//                        float coef1 = (dVal/r1.area())*r2.area();
//                        float coef2 = (dValNow/r1_.area())*r2_.area();

//                        float diffCoef = abs(coef1-coef2)/coef1;
//                        if(diffCoef < 0.2)
//                        {
//                            cerr << "------>>> C0-Peds " << id1 << "\t" << id2  << "\t" << diffCoef << endl;
//                        }
                    }

                }
#endif
            }
            else
            {
                Tracker[id1].relation[id2].second = 0.5;
                Tracker[id2].relation[id1].second = 0.5;

                Tracker[id1].relation[id2].first = dijNow;
                Tracker[id2].relation[id1].first = dijNow;

                Tracker[id1].relationROIs[id2] = Tracker[id2].trackWindow;
                Tracker[id2].relationROIs[id1] = Tracker[id1].trackWindow;

#if(KF_PAIRS)

                pairKFinit(relation2[pair<int,int>(id1,id2)],dijNow);
                pairKFinit(relation2[pair<int,int>(id2,id1)],dijNow);

#endif
            }

//            cout << Tracker[id1].relation[id2].second << endl;
        }
    }

}

/*!
  @brief Calculate the probability of occlusion for each pair of nodes (trackers)
  */
void get_occ_prob(vector<Point2f> nodeLoc, Mat& dij_Mat, Mat& occ_prob, vector<int> deadFlag)
{
    // Update the already existing nodes
    for(int i=0; i<occ_prob.rows; i++)
        for(int j=i+1; j<occ_prob.cols; j++)
        {
            float dij = norm(nodeLoc[i]-nodeLoc[j]);
            float prior = occ_prob.at<float>(i,j);

            float delta_dij = dij - dij_Mat.at<float>(i,j);
            float deno = dij > dij_Mat.at<float>(i,j) ? dij : dij_Mat.at<float>(i,j);

            float likelihood = (1-deadFlag[i]) * (1-delta_dij/deno);
            float evidence = likelihood * prior + (2-likelihood) * (1-prior);

            float posterior = (likelihood * prior) / evidence;

            occ_prob.at<float>(i,j) = posterior;
            occ_prob.at<float>(j,i) = posterior;

            dij_Mat.at<float>(i,j) = dij;
            dij_Mat.at<float>(j,i) = dij;
        }

    // Add new nodes
    int newNodes = nodeLoc.size()-occ_prob.rows;
    if(newNodes > 0)
    {
        // Create a new matrix with larger size
        Mat occ_prob_new(nodeLoc.size(),nodeLoc.size(),CV_32FC1,-1);
        Mat dij_Mat_new(nodeLoc.size(),nodeLoc.size(),CV_32FC1,-1);

        for(int i=0; i<occ_prob.rows; i++)
            for(int j=0; j<occ_prob.cols; j++)
            {
                occ_prob_new.at<float>(i,j) = occ_prob.at<float>(i,j);
                dij_Mat_new.at<float>(i,j) = dij_Mat.at<float>(i,j);
            }

        for(int i1=occ_prob.rows; i1<nodeLoc.size(); i1++)
            for(int j1=0; j1<nodeLoc.size(); j1++)
            {
                float dij = norm(nodeLoc[i1]-nodeLoc[j1]);
                float prior = 0.5;//1-dij/640;

                occ_prob_new.at<float>(i1,j1) = prior;
                occ_prob_new.at<float>(j1,i1) = prior;

                dij_Mat_new.at<float>(i1,j1) = dij;
                dij_Mat_new.at<float>(j1,i1) = dij;

                if(i1 == j1)
                {
                    occ_prob_new.at<float>(i1,j1) = -1;
                    dij_Mat_new.at<float>(i1,j1) = -1;
                }
            }

        occ_prob = occ_prob_new.clone();
        dij_Mat = dij_Mat_new.clone();
    }
}

/*!
  @brief Tests Inter Object Occlusion using new method.
  */
bool testIOO_(MultiTracker tracker, vector<Rect> trackWin, vector<int> trackId, int skipID, float& probOcc, int& occID)
{
    Rect r1 = tracker.trackWindow;

    bool ioo = false;
    float maxVal = 0;
    int maxIdx = -1;
    findMostOverlappingWindow(r1,trackWin,maxIdx,maxVal,skipID);

    if(maxIdx != -1 && maxVal > 0)
    {
        occID = trackId[maxIdx];
        Rect r2 = trackWin[maxIdx];
        float scaleVal = float(r2.area())/float(r1.area());
        scaleVal = scaleVal < 1.0 ? scaleVal : 1;
        probOcc = tracker.relation[occID].second * scaleVal;//occ_prob.at<float>(testID,occID);
        if(probOcc > 0.7)
            ioo = true;
    }
//    else
//    {
//        probOcc = 0;
//        occID = -1;
//        map< int, pair<float,float> >::iterator iter1;

//        for(iter1=tracker.relation.begin(); iter1!=tracker.relation.end(); iter1++)
//        {
//            if(iter1->second.second > probOcc)
//            {
//                probOcc = iter1->second.second;
//                occID = iter1->first;
//            }
//        }

//        if(occID != -1 && probOcc > 0.8)
//            ioo = true;
//    }

    return ioo;
}

/*!
  @brief Tests a particular id if it is occluded by an agent or not.
  */
bool testIOO(Mat occ_prob, int testID, int skipID, Rect r1, vector<Rect> trackWin, vector<int> trackId, float& probOcc, int& occID)
{
    bool ioo = false;
    float maxVal = 0;
    int maxIdx = -1;
    findMostOverlappingWindow(r1,trackWin,maxIdx,maxVal,skipID);

    if(maxIdx != -1 && maxVal > 0)
    {
        occID = trackId[maxIdx];
        probOcc = occ_prob.at<float>(testID,occID);
        ioo = true;
    }
    else
    {
        probOcc = 0;
        occID = -1;
        for(int i=0; i<occ_prob.row(testID).cols; i++)
        {
            if(occ_prob.at<float>(testID,i) > probOcc)
            {
                probOcc = occ_prob.at<float>(testID,i);
                occID = i;
            }
        }

        if(occID != -1 && probOcc > 0.8)
            ioo = true;
    }

    return ioo;
}


/*!
  @brief Tests if the new detection is some old occluded agent.
  */
int testAgentRetention(map<int,MultiTracker>& Tracker, Mat img, Rect newAgentPosn, float& finalConf)
{
    for(map<int,MultiTracker>::iterator i=Tracker.begin(); i!=Tracker.end(); i++)
    {
        int id1 = i->first;
    }
}



/*!
  @brief Tests if the new available agent is actually an old revocered agent which was occluded.
*/
int testNewAgent(map<int,MultiTracker>& Tracker, Mat img, Rect newAgentPosn, float& finalConf,
                 vector<int>& deadIds, vector<Mat>& deadHists)
{
    finalConf = 0;
    Mat hist1;
    getHist(img,newAgentPosn,hist1);
    Point2f p1 = 0.5 * (newAgentPosn.tl() + newAgentPosn.br());

#if(SURF_RESOLVER)
    vector<Rect> testRect;
    Rect testROI = newAgentPosn;
    scaleRect(testROI,1.2);
    checkBoundary(img,testROI);
    testRect.push_back(testROI);
#endif

    vector<float> confVec;
    vector<float> confIdVec;

//    float bcMax = -1;
//    int bcMaxInd = -1;
//    for(int i1=0; i1<deadIds.size(); i1++)
//    {
//        float deadBC=0;
//        computeBhattacharyaCoefficient(deadHists[i1],hist1,&deadBC);
//        cout << deadIds[i1] << "\t" << deadBC << endl;

//        if(deadBC > bcMax)
//        {
//            bcMax = deadBC;
//            bcMaxInd = i1;
//        }
//    }

//    if(bcMax > 0.8)
//    {
//        finalConf = -1;
//        return bcMaxInd;
//    }

    for(map<int,MultiTracker>::iterator i=Tracker.begin(); i!=Tracker.end(); i++)
    {
        int id1 = i->first;
        if(Tracker[id1].agentState == 5)
            continue;
        // For agents that are not active/updated and are occluded
        if(Tracker[id1].agentState != 1 && Tracker[id1].dead == false)
        {
#if(SURF_RESOLVER)
            bool testOK = false;
            Rect roiOutSurf;
            float roiConf = 0;
            int descNum2 = 0;
            cout << "testing new agent for " << id1 << endl;
            resolveTargetSurf(Tracker[id1].agentImgUpdated,img,testRect,roiOutSurf,roiConf,descNum2,testOK);
            roiOutSurf.x += testROI.x;
            roiOutSurf.y += testROI.y;
            if(testOK)
            {
                confVec.push_back(roiConf);
                confIdVec.push_back(id1);
            }
            cout << "matching conf " << roiConf << endl;
#else

            Rect r2 = Tracker[id1].KF_RectPredicted;

            float bhattCoef;
            Mat hist2 = Tracker[id1].hist.clone();
//            getHist(img,r2,hist2);
            computeBhattacharyaCoefficient(hist1,hist2,&bhattCoef);

            Point2f p2 = 0.5 * (r2.tl() + r2.br());
            float distance = exp(-norm(p1-p2)/300.0);

            float measure1 = (bhattCoef + distance)/2;

//            cout << measure1 << "\t" << distance << "\t" << bhattCoef << "\t" << newAgentPosn.x << "\t" << id1 << endl;


//            if(Tracker[id1].agentState == 2)
//            {
//                int occID = Tracker[id1].occludingAgentID;

//                Rect r3 = Tracker[occID].trackWindow;
////                Point2f p3 = 0.5 * (r3.tl() + r3.br());

//                float rightDist = abs(r3.br().x-newAgentPosn.x);
//                float leftDist = abs(r3.tl().x-newAgentPosn.br().x);
//                float lessDist = leftDist < rightDist ? leftDist : rightDist;
//                float dist2 = exp(-leftDist/500.0);

//                measure1 = (bhattCoef + distance + dist2)/3;

//                confVec.push_back(measure1);
////                cout << measure1 << "\t" << dist2 << "\t" << occID << "\t" << id1 << endl;
//            }
//            else
                confVec.push_back(measure1);

            confIdVec.push_back(id1);
#endif
        }
    }

    float maxVal = 0;
    int maxIdx = -1;

    for(int i1=0; i1<confVec.size(); i1++)
    {
        if(confVec[i1] > maxVal)
        {
            maxVal = confVec[i1];
            maxIdx = i1;
        }
    }

#if(SURF_RESOLVER)
    if(maxIdx != -1)
    {
        finalConf = confVec[maxIdx];
        return confIdVec[maxIdx];
    }
    else
        return -1;
#else
        if(maxIdx != -1)
        {
        Rect trackerRect = Tracker[confIdVec[maxIdx]].KF_RectPredicted;
        float scaleVal = (float)newAgentPosn.width / (float)trackerRect.width;
        scaleVal = scaleVal > 1 ? 1/scaleVal : scaleVal;
//        scaleVal = scaleVal > 0.7 ? scaleVal : 0;

        // Calculate drift in assignemnet
        bool drift = false;
        if( abs(trackerRect.x - newAgentPosn.x) > trackerRect.width )
            drift = true;


//        cout << scaleVal << "\t" << drift << endl;
        if( maxVal > 0.7 && scaleVal > 0.7)
        {
//            maxVal = (maxVal + scaleVal)/2;
//            if(maxVal > 0.8)
//            {
//            cerr << "Occluding ID " << confIdVec[maxIdx] << " recovered " << confVec[maxIdx] << endl;
                finalConf = 0.5 * (maxVal + scaleVal);
                return confIdVec[maxIdx];
//            }
        }
        else if(maxVal > 0.5 && Tracker[confIdVec[maxIdx]].agentState == 3 && scaleVal > 0.7 && !drift)
        {
//            maxVal = (maxVal + scaleVal)/2;
//            if(maxVal > 0.5)
//            {
            cerr << "Occluding ID " << confIdVec[maxIdx] << " recovered from Agent State 3 " << confVec[maxIdx] << endl;
                finalConf = 0.5 * (maxVal + scaleVal);
                return confIdVec[maxIdx];
//            }
        }
//        else if(maxVal > 0.5 && Tracker[confIdVec[maxIdx]].agentState == 5 && scaleVal > 0.7 )
//        {
//            cerr << "Occluding ID " << confIdVec[maxIdx] << " recovered from Agent State 5 " << confVec[maxIdx] << endl;
//                finalConf = 0.5 * (maxVal + scaleVal);
//                return confIdVec[maxIdx];
//        }
        else
            return -1;
    }
    else
    {
        return -1;
    }
#endif
}

/*!
  @brief Writes result data on TiXML document (before dead tracker is deleted).
  */
void writeResultFile(MultiTracker tracker, TiXmlElement* root)
{
    if(tracker.activeTimeSpan > 1)
    {
        TiXmlElement* traj_ids = new TiXmlElement("Trajectory");
        root->LinkEndChild(traj_ids);

        int numDetectionFrames = tracker.frameIds.size();

        traj_ids->SetAttribute("obj_id",tracker.id);
        traj_ids->SetAttribute("start_frame",tracker.frameIds[0]);
        traj_ids->SetAttribute("end_frame",tracker.frameIds[numDetectionFrames-1]);

        for(int k1=0; k1<numDetectionFrames; k1++)
        {
            TiXmlElement* id_frames = new TiXmlElement("Frame");
            traj_ids->LinkEndChild(id_frames);

            id_frames->SetAttribute("frame_no",tracker.frameIds[k1]);
            id_frames->SetAttribute("x",tracker.frameROIs[k1].x);
            id_frames->SetAttribute("y",tracker.frameROIs[k1].y);
            id_frames->SetAttribute("width",tracker.frameROIs[k1].width);
            id_frames->SetAttribute("height",tracker.frameROIs[k1].height);
            id_frames->SetAttribute("observation",tracker.frameObservationFlag[k1]);
        }
    }
}


/*!
  @brief Verifies if the tracker should be declared to be dead or not.
  */
bool verifyDeath(MultiTracker& tracker, Mat img)
{
//    if(tracker.exitTimeSpan >= exitThre)
//        tracker.agentState = 5;

    if(tracker.exitTimeSpan >= exitThre /*&& tracker.agentState != 2*/)
        tracker.dead = true;

    if(tracker.trackWindow.x <= 0 || tracker.trackWindow.br().x >= img.cols
            || tracker.trackWindow.y <= 0 || tracker.trackWindow.br().y >= img.rows
            || (tracker.status == false && tracker.activeTimeSpan == 1)) // Transients in detection
    {
        tracker.dead = true;
    }

//    if(tracker.activeTimeSpan < 3 && (tracker.agentState == 3 || tracker.agentState == 4))  // These shall persih, right?
//        tracker.dead = true;

    if(tracker.dead)
    {
        tracker.agentState = 4; // give any state other than 5
        cerr << tracker.id << " has expired." << endl;
    }

    return tracker.dead;
}

/*!
  @brief Whenever a tracker dies, these rituals must be performed.
  */
void afterDeathRituals(map<int,MultiTracker>& Tracker, int id, TiXmlElement* root)
{
    map<int,MultiTracker>::iterator i1;

    for(i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].occludingAgentID == id)
        {
            Tracker[id1].occludingAgentID == -1;
            Tracker[id1].agentState = 4;
        }

#if(KF_PAIRS_PRED)
        if(Tracker[id1].coPedId == id)
        {
            Tracker[id1].coPedId = -1;
        }
#endif

    }

    writeResultFile(Tracker[id],root);
    Tracker.erase(id);
}

/*!
  @brief Decides the final ROI out of the two available based on BC weighted average.
  */
void decideROI(Mat img, Rect r1, Rect r2, Mat hist, Rect& roiOut)
{
    Mat hist1, hist2;
    float bc1 = 0, bc2 = 0;
    getHist(img,r1,hist1);
    getHist(img,r2,hist2);

    computeBhattacharyaCoefficient(hist,hist1,&bc1);
    computeBhattacharyaCoefficient(hist,hist2,&bc2);

    float normFactor = 1.0/(bc1 + bc2);

    roiOut.width = normFactor * (bc1 * r1.width + bc2 * r2.width);
    roiOut.height = normFactor * (bc1 * r1.height + bc2 * r2.height);

    Point2f center1 = 0.5 * (r1.tl() + r1.br());
    Point2f center2 = 0.5 * (r2.tl() + r2.br());

    Point2f center = normFactor * (bc1 * center1 + bc2 * center2);

    roiOut.x = center.x - roiOut.width/2;
    roiOut.y = center.y - roiOut.height/2;
}


/*!
  @brief Tracking based on Spatial Layout Matching of the available trackers in the frame.
  */
void track_TS3(Mat img, map<int,MultiTracker>& Tracker, vector<Rect> detWin, string outputPath, Mat& occ_prob,
               Mat& dij_Mat, int frameId, Mat prevImg, vector<int>& deadIndicies, TiXmlElement* xmlRoot,
               Ptr<RTREESNAME>& rf_g, Mat& rf_samples, Mat& rf_labels, ofstream& dataFile,
               map< pair<int,int>, KalmanFilter>& relation2, vector<Mat>& deadHists, vector<int>& deadIDs, bool gmm_training = false)
{

//    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
//    {
//        int id1 = i1->first;
//        Rect r1 = Tracker[id1].trackWindow;
//        Mat hist1;
//        getHist(img,r1,hist1);
//        rf_samples.push_back(hist1);
//        rf_labels.push_back(id1);
//        Ptr<TrainData> tdata = prepare_train_data(rf_samples, rf_labels, rf_samples.rows);
//        int minSampleCount = 0.01*rf_samples.rows;
//        minSampleCount = minSampleCount >= 1 ? minSampleCount : 1;
//        rf_g = StatModel::train<RTREESNAME>(tdata,RTREESNAME::Params(30,minSampleCount,0,false,20,Mat(),false,0,
//                                                           TermCriteria(TermCriteria::MAX_ITER + TermCriteria::EPS, 100, 0.01f)));
//    }

    Mat trackedRegion = Mat::zeros(img.rows,img.cols,img.type());
    vector<Rect> trackWin;
    vector<int> trackId;
    vector<Rect> activeTrackWin;    // Only those who are active i.e. updated by tracker (not the predicted ones)
    vector<int> activeTrackId;
    Mat tldImage = img.clone();
    // Get the track windows to be associated
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].agentState != 5 /*&& Tracker[id1].agentState != 2*/)
        {
            Rect r1;
            r1 = Tracker[id1].trackWindow;
            trackWin.push_back(r1);

            trackId.push_back(id1);

            if(Tracker[id1].status == true)
            {
                activeTrackWin.push_back(r1);
                activeTrackId.push_back(id1);
            }

#if(TEST_FF_CC)
            findRoifloodFill(img.clone(),r1);
#endif

#if(TS2)
            // This part is buggged for r1 width as zero
            scaleRect(r1,1.3);
            checkBoundary(img,r1);
            img(r1).copyTo(trackedRegion(r1));
#endif
        }
    }

//    namedWindow("tldimage",WINDOW_NORMAL);
//    imshow("tldimage",tldImage);
#if(TS2)
    // Get the detection windows from inside the tracked image region only
    HogDetector(trackedRegion,detWin,0.0);
#endif

    vector<Point2f> points1;
    vector<Mat> hists1;
    vector<Rect> rects1;
    // Get the centers of track windows
    for(int i1=0; i1<trackWin.size(); i1++)
    {
        Rect r1 = trackWin[i1];
        checkBoundary(img,r1);
        Point2f p1 = 0.5 * (r1.tl() + r1.br());
        points1.push_back(p1);

        Mat hist1 = Tracker[trackId[i1]].hist.clone();
        hists1.push_back(hist1);
        rects1.push_back(r1);
    }

    // Draw a graph on points (EMST in this case)
    Mat_<int> adjMat;
    vector< vector<int> > nbrList(points1.size());
    Mat imgGraph = img.clone();
    draw_subdiv(imgGraph,points1,adjMat,nbrList,true);

    // Create the target points pool
    vector<Point2f> points2;
    vector<Rect> targetRects;
    vector<int> dummyFlag;  // a dummy point is added in points2 for each available track window which will be identified using this flag
    vector<int> dummyVarIndex;  // for each available tracker in the source, it contains the index of dummy variable in target
    vector<Mat> hists2;

    for(int i1=0; i1<detWin.size(); i1++)
    {
        Point p2 = 0.5 * (detWin[i1].tl() + detWin[i1].br());
        points2.push_back(p2);
        dummyFlag.push_back(0);
        targetRects.push_back(detWin[i1]);

        Mat hist2;
        getHist(img,detWin[i1],hist2);
        hists2.push_back(hist2);
    }

    // Append the dummmy data
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].agentState != 5 /*&& Tracker[id1].agentState != 2*/)
        {
            Rect r1 = Tracker[id1].trackWindow;
            Point p1 = 0.5 * (r1.tl() + r1.br());
            points2.push_back(p1);
            dummyFlag.push_back(1);
            targetRects.push_back(r1);
            dummyVarIndex.push_back(dummyFlag.size()-1);

            Mat hist2 = Tracker[id1].hist;
//            getHist(img,r1,hist2);
            hists2.push_back(hist2);
        }
    }

    vector<int> matchIndex(points1.size(),-1);
    Mat_<int> adjMatOut;
    float graphMatchProb = 0;
    bool matchOK = false;
    // Find graph matching solution
    if(points1.size() > 1)
    {
//        double tempt11 = cv::getTickCount();
        matchOK = matchGraph_TS3(points1,points2,hists1,hists2,rects1,targetRects,adjMat,outputPath,dummyFlag,dummyVarIndex,
                                      matchIndex,adjMatOut,graphMatchProb);
//        cout << (cv::getTickCount() - tempt11)/cv::getTickFrequency();
    }
    else if(points1.size() == 1)
    {
        Rect r1 = trackWin[0];
        float maxOverlap = -1;
        int maxIndex = -1;
        for(int i1=0; i1<detWin.size(); i1++)
        {
            Rect r2 = detWin[i1];
            float olap = 0;
            calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&olap);

            if(olap > maxOverlap)
            {
                maxOverlap = olap;
                maxIndex = i1;
            }
        }

        if(maxIndex != -1)
        {
            matchIndex[0] = maxIndex;
        }
    }
    else
    {
        // probably nothing to do
    }

    Mat outImgtemp = img.clone();
    if(matchOK)
    {
        // Draw the matching edges
        for(uint i=0; i<adjMatOut.rows; i++)
            for(uint j=i+1; j<adjMatOut.cols; j++)
            {
                if(adjMatOut[i][j]==1)
                {
                    cv::line(outImgtemp,points2[matchIndex[i]],points2[matchIndex[j]],cv::Scalar(0,255,255),2,LINENAME);
                }
            }

        // Draw the match points
        for(int i1=0; i1<matchIndex.size(); i1++)
        {
            if(matchIndex[i1] != -1)
                circle(outImgtemp,points2[matchIndex[i1]],2,Scalar(0,255,0),2,LINENAME);
        }
//        graphMatchVal = int(matchOK);
    }

    // Draw the target candidates
    Mat targetRectsImg = img.clone();
    for(int i1=0; i1<points2.size(); i1++)
    {
        if(i1<detWin.size())
        {
            circle(targetRectsImg,points2[i1],2,Scalar(0,255,0),2,LINENAME);
            rectangle(targetRectsImg,targetRects[i1],Scalar(0,255,0),2,LINENAME);
        }
        else
        {
            circle(targetRectsImg,points2[i1],2,Scalar(0,255,255),2,LINENAME);
            rectangle(targetRectsImg,targetRects[i1],Scalar(0,255,255),2,LINENAME);
        }
    }

//    namedWindow("spatialLayout",WINDOW_NORMAL);
//    imshow("spatialLayout",imgGraph);
//    namedWindow("targetRectImg",WINDOW_NORMAL);
//    imshow("targetRectImg",targetRectsImg);
//    namedWindow("graphMatch",WINDOW_NORMAL);
//    imshow("graphMatch",outImgtemp);
//    waitKey(0);

    vector<Rect> foundTrackWins;
    vector<int> foundTrackIDs;
    trackedRegion = img.clone();
    Mat blackImg = Mat::zeros(img.rows,img.cols,img.type());
    vector<int> assignedDetFlag(detWin.size(),-1);    // Flag for detections which are assigned to some tracker
    Mat finalTracksImg = img.clone();
    Mat tempColorImg = img.clone();
    vector<int> occIDs(trackId.size(),-1);
    vector< pair<int,int> > occPairs;
    for(int i1=0; i1<trackId.size(); i1++)
    {
//        float rfID = rf_g->predict(hists1[i1]);
//        cerr << "rfID \t" << trackId[i1] << "\t" << rfID << endl;

        // Update the agents for whom match was found
        if(matchIndex[i1] != -1 && matchIndex[i1] < detWin.size())
        {
            Tracker[trackId[i1]].initialAssignment = detWin[matchIndex[i1]];
            assignedDetFlag[matchIndex[i1]] = 1;
        }
        // Predict others?
        else if(matchIndex[i1] >= detWin.size())
        {
            // Test for inter object occlusion
            float probOcc = 0;
            int occID = -1;

            bool ioo = testIOO_(Tracker[trackId[i1]],trackWin,trackId,i1,probOcc,occID);

            if(ioo)
            {
                Tracker[trackId[i1]].agentState = 2;
                Tracker[trackId[i1]].occludingAgentID = occID;
//                cerr << "ID " << trackId[i1] << " occluded by " << occID << " with probability = " << probOcc << endl;
                dataFile << trackId[i1] << "\t" << occID << "\t" << probOcc << endl;
                if(Tracker[occID].occludingAgentID == trackId[i1])
                {
                    occPairs.push_back(pair<int,int>(occID,trackId[i1]));
                    Tracker[trackId[i1]].agentState = 4;
                    Tracker[occID].agentState = 4;
                }
                Tracker[trackId[i1]].status = false;
            }
            // For BG occlusion or lack of confirmation
            else
            {
                int maxIdx = -1;
                float maxVal = 0;
                findMostOverlappingWindow(trackWin[i1],trackWin,maxIdx,maxVal,i1);
                if(maxIdx == -1)
                {
                    Tracker[trackId[i1]].agentState = 3; // either occluded by BG or are not occluded, but definitely not ioo

//                    cerr << "Agent " << trackId[i1] << " State is 3" << endl; // Check if it even ever happens

                }
                else
                {
                    Tracker[trackId[i1]].agentState = 4; // may or may not be ioo
//                    cerr << "Agent " << trackId[i1] << " State is 4" << endl; // Check if it even ever happens
                }

                Tracker[trackId[i1]].status = false;
            }
        }


        // Finalize the window to be updated
        if(Tracker[trackId[i1]].initialAssignment.area() != 0)
        {
            Rect r2 = Tracker[trackId[i1]].initialAssignment;
            Rect rOut(0,0,0,0);

            MultiTracker tempTracker = Tracker[trackId[i1]];

            float matchVal_MS = 0;
            tempTracker.trackWindow = r2;
            tempTracker.localizeAppearanceByMeanShift(img,0.5,&matchVal_MS);
            rOut = tempTracker.MS_Rect;


        // Finalize using SURF matching
            if(rOut.area() != 0)
            {
                checkBoundary(img,rOut);
                Rect finalROI;
                decideROI(img,r2,rOut,Tracker[trackId[i1]].hist,finalROI);
                rectangle(finalTracksImg,r2,Scalar(0,255,0),2,LINENAME);
                rectangle(finalTracksImg,rOut,Scalar(0,0,255),2,LINENAME);
                r2 = finalROI;
//                r2.width = detWin[matchIndex[i1]].width;
//                r2.height = detWin[matchIndex[i1]].height;
            }

            checkBoundary(img,r2);
            rectangle(finalTracksImg,r2,Scalar(255,255,255),2,LINENAME);

            Tracker[trackId[i1]].updateAgent(img,r2,frameId,trackWin,gmm_training);

            foundTrackWins.push_back(r2);
            foundTrackIDs.push_back(trackId[i1]);

//            scaleRect(r2,0.8);
//            checkBoundary(img,r2);
//            blackImg(r2).copyTo(trackedRegion(r2));
        }
    }

//    imshow("finaltrack",finalTracksImg);
//    imshow("tepColor",tempColorImg);



#if(SLC_PRED)
    prediction_SLC(img,Tracker,points1,adjMat,hists1,outputPath);
#endif

    // Initiate new agents
    for(int i1=0; i1<detWin.size(); i1++)
    {
#if(!TS2)
        if(assignedDetFlag[i1] == -1)
#endif
        {

            int maxIdx = -1;
            float maxVal = 0;
            findMostOverlappingWindow(detWin[i1],detWin,maxIdx,maxVal,i1);
            if(maxIdx != -1 && maxVal > 0.4)
            {
                continue;
            }

#if(RF_LEARN)
                Mat rfHist;
                getHist(img,detWin[i1],rfHist);
                float rfResponse = Tracker[matchID].randForest->predict(rfHist);
                cout << "rfrespo \t" << i1 << "\t" << matchID << "\t" << rfResponse << endl;
 #endif
            float testConf = 0, testConfRetention=0;
            int matchID = testNewAgent(Tracker,img,detWin[i1],testConf,deadIDs,deadHists);
            if(matchID != -1 && testConf > 0)
            {

#if(!SURF_RESOLVER)
                vector<Rect> testRect;
                Rect testROI = detWin[i1];
                scaleRect(testROI,1.2);
                checkBoundary(img,testROI);
                testRect.push_back(testROI);
                bool testOK = false;
                Rect roiOutSurf;
                float roiConf = 0;
                int descNum2 = 0;
                resolveTargetSurf(Tracker[matchID].agentImgUpdated,img,testRect,roiOutSurf,roiConf,descNum2,testOK);
                roiOutSurf.x += testROI.x;
                roiOutSurf.y += testROI.y;
#endif


#if(!SURF_RESOLVER)
                if(testOK || testConf > 0.9 /*|| (Tracker[matchID].agentState == 5)*/ )
#endif
                {
//                    Rect roiUpdate = detWin[i1];

//                    Point2f surfCenter = 0.5 * (roiOutSurf.tl() + roiOutSurf.br());
//                    roiUpdate.x = surfCenter.x - roiUpdate.width/2;
//                    roiUpdate.y = surfCenter.y - roiUpdate.height/2;

//                    checkBoundary(img,roiUpdate);
//                    if(roiUpdate.area() > 0)
//                        Tracker[matchID].updateAgent(img,roiUpdate,frameId,trackWin,gmm_training);
                    Tracker[matchID].trackWindow = detWin[i1];
                    Tracker[matchID].updateAgent(img,detWin[i1],frameId,trackWin,gmm_training);
//                    cerr << "SURF confirmed assignement id " << matchID << endl;
                }

            }
            else if(testConf == -1)
            {
                int setID = deadIDs[matchID];
                Tracker[setID].initAgent(img,detWin[i1],frameId,trackWin,gmm_training);
                Tracker[setID].id = setID;
                cout << "retained agent ID - " << setID << endl;

                vector<Mat> deadHistsTemp;
                vector<int> deadIDsTemp;
                for(int i2=0; i2<deadIDs.size(); i2++)
                {
                    if(i2 != matchID)
                    {
                        deadHistsTemp.push_back(deadHists[i2]);
                        deadIDsTemp.push_back(deadIDs[i2]);
                    }
                }
                deadHists.assign(deadHistsTemp.begin(),deadHistsTemp.end());
                deadIDs.assign(deadIDsTemp.begin(),deadIDsTemp.end());
            }
            else
            {
//                int maxIdx = -1;
//                float maxVal = 0;
//                findMostOverlappingWindow(detWin[i1],trackWin,maxIdx,maxVal,i1);
//                if(maxIdx != -1 && maxVal > 0.4)
//                {
//                    continue;
//                }


                Tracker[globalID].initAgent(img,detWin[i1],frameId,trackWin,gmm_training);
                Tracker[globalID].id = globalID;
                globalID++;
                cout << "new agent ID - " << globalID-1 << endl;
                trackWin.push_back(detWin[i1]);
                trackId.push_back(globalID-1);
            }
        }
    }


    vector<int> deadIDsCurr;

    // Predict the agents not available
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].status == false && Tracker[id1].agentState!=5)
        {
            // Correct the predictor using SURF matching
//#if(SURF_PREDICTOR)
            if(Tracker[id1].activeTimeSpan > 1)
            {

#if(RF_LEARN)
                Mat rfHist;
                getHist(img,Tracker[id1].trackWindow,rfHist);
                float rfResponse = Tracker[id1].randForest->predict(rfHist);
                cout << "rfrespo Pred \t" << id1 << "\t" << rfResponse << endl;
 #endif

                vector<Rect> testRect;
                Rect testROI = Tracker[id1].trackWindow;

#if(IMG_APPEARANCE_MODEL)
//                if(testROI.area() < 0.2 * (img.rows*img.cols))
                {
                    Rect outRect_cbg;
                    track_colorBlobGraph(img.clone(),Tracker[id1],outputPath,outRect_cbg);
                    if(outRect_cbg.area() != 0)
                    {
                        cerr << "Blob tracked " << id1 << endl;
                    }
                }
#endif
                scaleRect(testROI,1.2);
                checkBoundary(img,testROI);
                testRect.push_back(testROI);
                bool testOK = false;
                Rect roiOutSurf;
                float roiConf = 0;
                int descNum2 = 0;
                resolveTargetSurf(Tracker[id1].agentImgUpdated,img,testRect,roiOutSurf,roiConf,descNum2,testOK);
                roiOutSurf.x += testROI.x;
                roiOutSurf.y += testROI.y;
                if(testOK)
                {
                    Rect roiUpdate = testROI;//Tracker[id1].trackWindow;

                    Point2f surfCenter = 0.5 * (roiOutSurf.tl() + roiOutSurf.br());
                    roiUpdate.x = surfCenter.x - roiUpdate.width/2;
                    roiUpdate.y = surfCenter.y - roiUpdate.height/2;

                    checkBoundary(img,roiUpdate);
                    if(roiUpdate.area() > 0)
                    {
                        Tracker[id1].initialPrediction = roiUpdate;
                        Tracker[id1].exitTimeSpan = 0;
                    }
//                    if(roiConf > 0.9)
//                    {
//                        cerr << "SURF updated predictor window id " << id1 << " conf " << roiConf << endl;
//                        checkBoundary(img,roiOutSurf);
//                        Tracker[id1].updateAgent(img,roiOutSurf,frameId,trackWin,gmm_training);
//                        continue;
//                    }
                }
//                else
//                {
//                    Rect rOut(0,0,0,0);

//                    MultiTracker tempTracker = Tracker[id1];

//                    float matchVal_MS = 0;
//                    tempTracker.localizeAppearanceByMeanShift(img,0.5,&matchVal_MS);
//                    rOut = tempTracker.MS_Rect;

//                    if(matchVal_MS > 0.8)
//                    {
//                        checkBoundary(img,rOut);
//                        Tracker[id1].initialPrediction = rOut;
//                        Tracker[id1].exitTimeSpan = 0;
//                    }
//                }

#if(KF_PAIRS_PRED)
                if(Tracker[id1].coPedId != -1)
                {
                    int id2 = Tracker[id1].coPedId;
                    cerr << "Predicting via pairs for " << id1 << "\t" << id2 << endl;
                    float dist = relation2[pair<int,int>(id1,id2)].statePre.at<float>(0);
                    bool left = false;
                    if(Tracker[id1].trackWindow.x < Tracker[id2].trackWindow.x)
                        left = true;
                    Tracker[id1].initialPrediction = Tracker[id1].trackWindow;
                    Tracker[id1].initialPrediction.x = Tracker[id2].trackWindow.x - (left == true ? 1 : -1) * dist;
                }
#endif
            }
//#endif


            // Update the ioo occluding ID status
            if(Tracker[id1].agentState == 2)
            {
                float probOcc = 0;
                int occID = -1;
//                bool ioo = testIOO(occ_prob,i1,-1,Tracker[i1].trackWindow,foundTrackWins,foundTrackIDs,probOcc,occID);
                bool ioo = testIOO_(Tracker[id1],foundTrackWins,foundTrackIDs,-1,probOcc,occID);

                if(ioo && occID != Tracker[id1].occludingAgentID)
                {
//                    cerr << "Update: ID " << id1 << " occluded by " << occID << " with probability = " << probOcc << endl;
                    Tracker[id1].occludingAgentID = occID;
                }
                else if(!ioo)
                {
//                    cerr << "Occluding ID " << i1 << " terminated." << endl;
//                    Tracker[i1].exitTimeSpan = exitThre;
                }

            }
            else if(Tracker[id1].agentState == 3)
            {

            }
            predictAgent(Tracker[id1],frameId);
            checkBoundary(img,Tracker[id1].KF_RectPredicted);
            bool dead = verifyDeath(Tracker[id1],img);
            if(dead)
            {
                deadIDsCurr.push_back(id1);
                if(Tracker[id1].activeTimeSpan > 1 && Tracker[id1].exitTimeSpan >= exitThre)
                {
                    deadIDs.push_back(id1);
                    deadHists.push_back(Tracker[id1].hist);
                }
            }
        }
    }

    for(int i1=0; i1<deadIDsCurr.size(); i1++)
        afterDeathRituals(Tracker,deadIDsCurr[i1],xmlRoot);

    updateNodeRelations(Tracker,relation2);
}

#if(RF_LEARN)
static Ptr<TrainData>
prepare_train_data(const Mat& data, const Mat& responses, int ntrain_samples)
{
    Mat sample_idx = Mat::zeros( 1, data.rows, CV_8U );
    Mat train_samples = sample_idx.colRange(0, ntrain_samples);
    train_samples.setTo(Scalar::all(1));

    int nvars = data.cols;
    Mat var_type( nvars + 1, 1, CV_8U );
    var_type.setTo(Scalar::all(CV_VAR_ORDERED));
    var_type.at<uchar>(nvars) = CV_VAR_CATEGORICAL;

    return TrainData::create(data, CV_ROW_SAMPLE, responses,
                             noArray(), sample_idx, noArray(), var_type);
}
#endif

Multi-person Tracking

--Tested on Ubuntu 12.04

1. Build
	# cmake CMakeLists.txt
	# make
	
2. Dependencies
	# OpenCv
		++ Tested on version 2.4.2
	# libDAI
		++ Tested on version 0.2.7
		++ https://staff.fnwi.uva.nl/j.m.mooij/libDAI/
	# tinyXML 
		++ compiled with source code
	
3. Run
	# cd bin
	# ./humanTracking <path to the input video>
	
4. Usage
	# Path to test video is passed through command line as an argument.
	# Press 'P/p' on the image to pause and play the generated output.
	# Press 'Esc' on the image to exit the code.
	# Output is generated in the bin as an ouput video and other result files.

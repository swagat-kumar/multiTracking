cmake_minimum_required(VERSION 2.4.6)
#include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)

#rosbuild_init()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

set(CMAKE_BUILD_TYPE RelWithDebInfo)
#set(CMAKE_CXX_FLAGS "-DNDEBUG -O4")# -W -Wall -Wno-unused-parameter -fno-strict-aliasing")

#common commands for building c++ executables and libraries
#rosbuild_add_library(${PROJECT_NAME} src/example.cpp)
#target_link_libraries(${PROJECT_NAME} another_library)
#rosbuild_add_boost_directories()
#rosbuild_link_boost(${PROJECT_NAME} thread)
#rosbuild_add_executable(example examples/example.cpp)
#target_link_libraries(example ${PROJECT_NAME})

#rosbuild_add_executable(humanTracking src/main.cpp src/trackFunctions.cpp include/tracker.h)
#rosbuild_add_executable(humanTracking src/multiTrack.cpp src/Timer.cpp include/multiTrack.h)

include_directories(${PROJECT_SOURCE_DIR}/include/)
add_executable(humanTracking src/multiTrack.cpp include/multiTrack.h )#src/Timer.cpp)
#rosbuild_add_executable(humanTracking src/getResult.cpp)
#rosbuild_add_executable(humanTracking src/images_to_video_dataset.cpp)
#rosbuild_add_executable(humanTracking src/get_improvement_measure.cpp)

find_package( OpenCV REQUIRED )
#include_directories( ${PROJECT_SOURCE_DIR}/../opencv-3.0.0-beta/include/ )
include_directories(${OpenCV_DIRS})
target_link_libraries( humanTracking ${OpenCV_LIBS})

# Required for Hunagrian Algorithm
#rosbuild_add_executable(humanTracking src/hungarian/munkres/main.cpp)
#add_executable(humanTracking src/hungarian/munkres/munkres.cpp)
#include_directories(${PROJECT_SOURCE_DIR}/src/hungarian/munkres/)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

# Required for graph matching routine
include_directories(${PROJECT_SOURCE_DIR}/../libDAI-0.2.7/include/)
target_link_libraries(humanTracking ${PROJECT_SOURCE_DIR}/../libDAI-0.2.7/lib/libdai.a)

# Required for particle filter routine
#add_executable(humanTracking src/gpf.cpp include/gpf.h)
#target_link_libraries(humanTracking gsl gslcblas)

# Required for tinyXML to read the ground truth of CAVIAR dataset
add_executable(humanTracking ../tinyxml/tinyxml.cpp ../tinyxml/tinyxmlerror.cpp ../tinyxml/tinyxmlparser.cpp ../tinyxml/tinystr.cpp)
include_directories(${PROJECT_SOURCE_DIR}/../tinyxml/)

#FIND_PACKAGE(OCTAVE REQUIRED )
#include_directories( ${OCTAVE_INCLUDE}/octave )
#TARGET_LINK_LIBRARIES( esd ${OCTAVE_LIBS} )

# OpenMP for parallelizing for loops
#find_package(OpenMP)
#if (OPENMP_FOUND)
#    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
#    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
#endif()


# Link OpenCv 3 TLD
#include_directories(${PROJECT_SOURCE_DIR}/../opencv-3.0.0-beta/opencv_contrib-master/modules/)
#target_link_libraries(humanTracking ${PROJECT_SOURCE_DIR}/../opencv-3.0.0-beta/build/lib/libopencv_tracking.so.3.0.0 )

#include<multiTrack.h>

#define timingFile              1
#define runavg                  0
#define indtime                 0
#define agentImages             0
#define bhatCoefThreshold       0.6


#define showImage               1
int WAITKEY = 1;
#define SHOW_DETECTIONS         0       // Shows an image with detected rectangles marked


//#include<omp.h>

#define DATASET        0    // 0    input path to video in argument
                            // 1    ETH_bahnhof_seq_03
                            // 2    ETH_sunny_day
                            // 3    TUD_Stadtmitte
                            // 4    PETS09


#define DETECTION_RESPONSE      1   // 1 OpenCv HoG     2 Nevatia_USC_JRoG_XML
#define HOG_THRESH              0.5 // OpenCv HoG Threshold for hit_rate
#define SKIP_FRAMES             0
#define AR_OPENCV               0   // Writing algo results using Opencv filestorage format (along with default TinyXML format)

#define REASONING_MAT_COMP      2   // Number of components for calculation of each element in reasoning matrix
#define WEIGHTS_LEARN           0   // EM method for learning weights of different components in the calclation of reasoning matrix

#define GROUP_RESOLVE_SURF      1   // Enables group detection, occlusion in groups and SURF based resolution
#define GROUP_RESOLVE_SURF_2    0   // Enables resolution after above step when the occluded agent in group reappears

#define SPATIAL_LAYOUT          0   // Graph Matching Algorithm for Spatial Layout Consistency

#define SURF_MATCHING           0   // Enables SURF matching between all possible pairs in reasoning matrix

#define IMG_APPEARANCE_MODEL2   0

#define TRACKING_STRATEGY2      0   // Tries to track the object first with SURF or Color Graph in local region, and modifies DAT part acc.

#if(IMG_APPEARANCE_MODEL || TRACKING_STRATEGY2)
#define GMM_TRAINING            1
#else
#define GMM_TRAINING            0
#endif


#define GROUPS_PASSING          1

#define THETA_FILE              0

// WARNING: These parameters need to be set same in header file as well
#define IMG_SEG_GRAPH_MATCH     0

int main(int argc, char ** argv)
{
    cv::VideoCapture capInput;

    string outputPath = "./dataset_result/";
    char datasetString[2];
    sprintf(datasetString,"%d",DATASET);
    outputPath.append(datasetString);
    outputPath.append("/");

    string outTrackDocName = outputPath;
    int totalFrames;
#if(DATASET == 0)
    if(argc != 2)
    {
    cerr << "Input video path not provided" << endl;
    exit(-1);
    }
    capInput.open(argv[1]);
    outTrackDocName.append("output.xml");
    totalFrames = 9999;
#elif(DATASET == 1)
    capInput.open("../../dataset2/seq03-img-left/ETH_bahnhof_seq_03.avi");
    outTrackDocName.append("ETH_bahnhof.avi.tracking.xml");
    totalFrames = 999;
#elif(DATASET == 2)
    capInput.open("../../dataset2/sunny_day-img-left/ETH_sunny_day.avi");
    outTrackDocName.append("ETH_sunny_day.avi.tracking.xml");
    totalFrames = 354;
#elif(DATASET == 3)
    capInput.open("../../dataset2/TUD_Stadtmitte/TUD_Stadtmitte.avi");
    outTrackDocName.append("TUD_Stadtmitte.avi.tracking.xml");
    totalFrames = 179;
#elif(DATASET == 4)
    capInput.open("../../dataset2/PETS09/S2/L1/Time_12-34/View_001/PETS09.avi");
    outTrackDocName.append("PETS09.avi.tracking.xml");
    totalFrames = 795;
#endif

    string outDirSysCall = "mkdir -p ";
    outDirSysCall.append(outputPath);
    system(outDirSysCall.c_str());

    // Reading human detection response from the file

#if(DETECTION_RESPONSE == 2)

    vector< vector<Rect> > detROIs;

#if(DATASET == 0)
    cerr << "No detection input for this video" << endl;
    exit(-1);
#elif(DATASET == 1)
    TiXmlDocument detectionFile("../../dataset2/nevatia_USC/ETH_detection/bahnhof_raw.avi.detection.xml");
#elif(DATASET == 2)
    TiXmlDocument detectionFile("../../dataset2/nevatia_USC/ETH_detection/sunnyday_raw.avi.detection.xml");
#elif(DATASET == 3)
    TiXmlDocument detectionFile("../../dataset2/nevatia_USC/TUD/TUD_Stadtmitte.avi.detection.xml");
#elif(DATASET == 4)
    TiXmlDocument detectionFile("../../dataset2/nevatia_USC/PETS09/PETS09_View001_S2_L1_000to794.avi.detection.xml");
#endif

    bool loadOK = detectionFile.LoadFile();

    if(!loadOK)
    {
        cout << "Detection Response XML file loading failed, exiting" << endl;
        exit(-1);
    }

    // Create a handle
    TiXmlHandle detHandle(&detectionFile);
    TiXmlElement* detElem;
    TiXmlHandle hroot(0);

    // Get root element
    detElem = detHandle.FirstChildElement().Element();
    if(!detElem)
    {
        cout << "No root element for XML" << endl;
        exit(-1);
    }
    else
    {
        string detName = detElem->Value();
        cout << "Reading XML File - " << detElem->Attribute("fname") << endl;

        int start_frame = atoi(detElem->Attribute("start_frame"));
        int end_frame = atoi(detElem->Attribute("end_frame"));

        cout << "Start Frame for Input - " << start_frame << endl;
        cout << "End Frame for Input - " << end_frame << endl;

        hroot = TiXmlHandle(detElem);
    }

    TiXmlElement* frameElement = hroot.FirstChild("Frame").Element();

    // Iterate over all frame elements
    for(frameElement; frameElement; frameElement = frameElement->NextSiblingElement())
    {
        int frameNumber = atoi(frameElement->Attribute("no"));

        TiXmlHandle frameListRoot = TiXmlHandle(frameElement);

        vector<Rect> det_roi;

        TiXmlElement* objListElem = frameListRoot.FirstChild("ObjectList").FirstChild().Element();

        // Iterate over all objects in the object list of a frame
        for(objListElem; objListElem; objListElem = objListElem->NextSiblingElement())
        {
            TiXmlHandle objectListRoot = TiXmlHandle(objListElem);
            TiXmlElement* objectElem = objectListRoot.FirstChild("Rect").Element();

            Rect tempROI;
            tempROI.height = atoi(objectElem->Attribute("height"));
            tempROI.width = atoi(objectElem->Attribute("width"));
            tempROI.x = atoi(objectElem->Attribute("x"));
            tempROI.y = atoi(objectElem->Attribute("y"));

            det_roi.push_back(tempROI);
        }

        detROIs.push_back(det_roi);
    }

#endif

    //Declaring File Pointers for writing timing
    FILE /**fp_avg,*/*fp_ind,*fp_runavg;
    ofstream fp_avg;

    string outFileName;
    if(timingFile)
    {
        outFileName = outputPath;
        fp_runavg = fopen(outFileName.append("./runavg_time.txt").c_str(),"w");
        outFileName = outputPath;
        fp_ind = fopen(outFileName.append("./ind_time.txt").c_str(),"w");
        outFileName = outputPath;
//        fp_avg = fopen(outFileName.append("./avg_time.txt").c_str(),"w");
        fp_avg.open(outFileName.append("./avg_time.txt").c_str());
    }

    float avgTime=0.0,runavg_hog=0.0,runavg_ms=0.0,runavg_bip=0.0,runavg_surf=0.0;
    float ind_hog=0.0,ind_ms=0.0,ind_bip=0.0,ind_surf=0.0;

    int avg_count =0;

    map<int,MultiTracker> Tracker;//Vector storing agents

    vector<Rect> HogWin;//Vector storing Hog Windows in each frame
    vector<int> deadIndicies;   // Stores the indicies of Tracker vector which are dead and should be released/replaced.


    // Write the algo results in an XML using TinyXML
//******
    TiXmlDocument doc;
    TiXmlDeclaration* decl = new TiXmlDeclaration("1.0","","");
    doc.LinkEndChild(decl);

    TiXmlElement* root = new TiXmlElement("Video");
    doc.LinkEndChild(root);
    root->SetAttribute("fname","ETH");
    root->SetAttribute("start_frame",0);
    root->SetAttribute("end_frame",totalFrames);
//******

    Mat firstImg;

    for(int k1=0; k1<SKIP_FRAMES; k1++)
        capInput>> firstImg;

    capInput >> firstImg;

    if(firstImg.empty())
    {
        cout << "Empty First Image, exiting" << endl;
        exit(-1);
    }

    Mat frameData = firstImg.clone();
    Mat prevImg = firstImg.clone();

#if(DETECTION_RESPONSE == 1)

    Mat frameData2;
    float scaleFactor = 1;
    cv::resize(frameData,frameData2,Size(scaleFactor*frameData.cols,scaleFactor*frameData.rows));
    vector<Rect> HogWin2;
//    int detRect2 = HogDetector(frameData2,HogWin2,0.5);

    int detRect = HogDetector(frameData2, HogWin,HOG_THRESH);
    for(int i1=0; i1<HogWin.size(); i1++)
    {
        HogWin[i1].x /= scaleFactor;
        HogWin[i1].y /= scaleFactor;
        HogWin[i1].width /= scaleFactor;
        HogWin[i1].height /= scaleFactor;
    }

#elif(DETECTION_RESPONSE == 2)
    int detRect = detROIs[0].size();
    if(detRect !=0)
        HogWin.assign(detROIs[0].begin(),detROIs[0].end());
#endif

    filterDetections(firstImg,HogWin,true);

#if(SHOW_DETECTIONS)
        Mat detImageTempFirst = firstImg.clone();
        for(int k3=0; k3<HogWin.size(); k3++)
        {
            rectangle(detImageTempFirst,HogWin[k3],Scalar(0,255,0),2);
            char txt[20];
            sprintf(txt,"%d",k3);
            putText(detImageTempFirst,txt,HogWin[k3].tl(),2,1,Scalar(0,0,255),2);
        }
        namedWindow("firstFrameInitDetections",WINDOW_NORMAL);
        imshow("firstFrameInitDetections",detImageTempFirst);
        waitKey(0);
#endif

    string outVidName = outputPath;
    outVidName.append("outTrack.avi");

    VideoWriter video3(outVidName, CV_FOURCC('D','I','V','3'), 10,Size(frameData.cols,frameData.rows),1);

    for(int count=0; count < HogWin.size() ; count++)
        checkBoundary(frameData,HogWin[count]);//Checking boundary for Hog Windows

    Mat clusteredImg = frameData.clone();
    //Initializing agents detected in first frame
    for(int i=0 ; i< HogWin.size() ; i++ )
    {
        Tracker[globalID].initAgent(frameData , HogWin[i], 0, HogWin,GMM_TRAINING);
        Tracker[globalID].id = globalID;
        globalID++;

#if(IMG_SEG_GRAPH_MATCH)
        Tracker[i].segmentedImg.copyTo(clusteredImg(HogWin[i]));
        //        findRoifloodFill(frameData,HogWin[i]);
#endif

    }

    // Display the first image
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
        Tracker[i1->first].markAgents(frameData);

#if(IMG_SEG_GRAPH_MATCH)
    imshow("clusteredImg1",clusteredImg);
    imshow("img", frameData);
    waitKey(0);
#endif
    vector<Point2i> groups; //vector storing as point of id's of two people forming group
#if(GROUPS_PASSING)
    vector<Point3i> groupsOcc;      // all such pairs which have one member occluded, x is occluding id, y is occluded id and z is distance
    vector<Rect> occRects;      // corresponding roi of occluded member
#endif

#if(AR_OPENCV)
    vector< vector<int> > detected_agentIDs;
    vector< vector<Rect> > detected_agentROIs;
#endif

    vector<float> weights_reasoning(REASONING_MAT_COMP,1.0/float(REASONING_MAT_COMP)); // Weights for reasoning matrix components
    vector< vector<float> > diffValues_reasoning(REASONING_MAT_COMP);   // Differential values of components stored for every iteration

#if(TRACKING_STRATEGY3)
    map< pair<int,int> , KalmanFilter > relation2; // A Kalman Filter Predictor for every pair of tracker

    // Initialize the Bayesian Nodes Prior Probability
    Mat occ_prob(HogWin.size(),HogWin.size(),CV_32FC1,-1);  // Matrix contains the occlusion probability
    Mat dij_Mat(HogWin.size(),HogWin.size(),CV_32FC1,-1);
    vector<Point2f> nodeLoc;
    for(int i1=0; i1<HogWin.size(); i1++)
        nodeLoc.push_back(0.5*(HogWin[i1].tl()+HogWin[i1].br()));

    for(int i1=0; i1<nodeLoc.size(); i1++)
        for(int j1=i1+1; j1<nodeLoc.size(); j1++)
        {
            float dij = norm(nodeLoc[i1]-nodeLoc[j1]);
            float prior = 0.5;//1-dij/640;

            occ_prob.at<float>(i1,j1) = prior;
            occ_prob.at<float>(j1,i1) = prior;

            dij_Mat.at<float>(i1,j1) = dij;
            dij_Mat.at<float>(j1,i1) = dij;
        }

    updateNodeRelations(Tracker,relation2);

    Ptr<RTREESNAME> rf_g;   // Random Forest (global) for all the trackers
    Mat rf_labels, rf_samples;
    string dataFileName = outputPath;
    dataFileName.append("dataFile.txt");
    ofstream dataFile(dataFileName.c_str());

    vector<Mat> deadHists;  // Stores hists odf dead ids
    vector<int> deadIDs;
#endif
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------
    // Iteration loop starts here
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    Mat img = firstImg.clone();

    // Read frames in a loop
    //    for(int k=-1; k<4000; k++)
    int frameNum=-1+SKIP_FRAMES;
    while(1)
    {
        frameNum++;
        cout << "Frame Number - " << frameNum+1 << endl;

        double start_ =  getTickCount();//time.getElapsedTimeInMilliSec();

        float surfTime=0.0;
        avg_count++;

        if(timingFile)
        {
            fprintf(fp_runavg,"%d\t\t",frameNum+1);
            fprintf(fp_ind,"%d\t\t",frameNum+1);
//            fprintf(fp_avg,"%d\t\t",frameNum+1);
            fp_avg << frameNum+1 << "\t\t";
        }


        //HSV Image
        Mat img2 = img.clone();
        //        cvtColor(img,img2,CV_BGR2HSV);

        Mat tempImg = img.clone();

        // Displaying frame Number
        char tempText[100];
        sprintf(tempText,"%d",frameNum+1);
        putText(tempImg,tempText,Point(50,50),2, 1,Scalar(0,255,0),2,LINENAME,false);

        HogWin.clear();

        // Step - 1
        // Call HOG on the input frame
        double start =  getTickCount();//time.getElapsedTimeInMilliSec();

#if(DETECTION_RESPONSE == 1)

        Mat imgDet;
        cv::resize(img,imgDet,Size(scaleFactor*img.cols,scaleFactor*img.rows));

        detRect = HogDetector(imgDet, HogWin,HOG_THRESH);
        for(int i1=0; i1<HogWin.size(); i1++)
        {
            HogWin[i1].x /= scaleFactor;
            HogWin[i1].y /= scaleFactor;
            HogWin[i1].width /= scaleFactor;
            HogWin[i1].height /= scaleFactor;
        }


#elif(DETECTION_RESPONSE == 2)
        if(frameNum < detROIs.size())
        {
            detRect = detROIs[frameNum].size();
            HogWin.clear();
            for(int i1=0; i1<detROIs[frameNum].size(); i1++)
            {
                Rect r1 = detROIs[frameNum][i1];
                int areaInit = r1.area();
                checkBoundary(img,r1);
                int areaNow = r1.area();
                if(areaInit == areaNow)
                    HogWin.push_back(r1);
            }
            detRect = HogWin.size();
//            if(detRect != 0)
//            {
//                HogWin.assign(detROIs[frameNum].begin(),detROIs[frameNum].end());
//            }
        }
        else
            detRect = 0;
#endif

//        filterDetections(img,HogWin,true);
        detRect = HogWin.size();

        double end = getTickCount();//time.getElapsedTimeInMilliSec();

#if(SHOW_DETECTIONS)
        Mat detImageTemp = img.clone();
        for(int k3=0; k3<HogWin.size(); k3++)
        {
            rectangle(detImageTemp,HogWin[k3],Scalar(0,255,0),2);
            char txt[20];
            sprintf(txt,"%d",k3);
            putText(detImageTemp,txt,HogWin[k3].tl(),2,1,Scalar(0,0,255),2);
        }
        namedWindow("tempdet",WINDOW_NORMAL);
        imshow("tempdet",detImageTemp);
        waitKey(0);
#endif

#if(IMG_SEG_GRAPH_MATCH)
        clusteredImg = img.clone();
        for(int i=0 ; i< detRect ; i++ )
        {
            //            vector<Point2f> clusterCenters;
            //            Mat labels;
            //            searchBin(img(HogWin[i]).clone(),clusterCenters,labels)
            //            findRoifloodFill(img,HogWin[i]);
        }
#endif

        if(timingFile)
        {
            if(runavg)
            {
                runavg_hog = (runavg_hog + ((float)(end-start)/1000.0));
                fprintf(fp_runavg,"%f\t",runavg_hog/((float)avg_count));
            }
            if(indtime)
            {
                ind_hog = ((float)(end-start)/1000.0);
                fprintf(fp_ind,"%f\t",ind_hog);
            }
        }

        //Label saying whether Hog Window is associated to some MS Window
        //associated: hogLabel[i] =1;
        int hogLabel[HogWin.size()];
        for(int i =0; i < HogWin.size(); i++)
            hogLabel[i]=0;
        for(int count=0; count < HogWin.size() ; count++)
            checkBoundary(frameData,HogWin[count]);

        vector<int> removeHogWinIndex(HogWin.size(),0);  // detections to be removed from the vector that already got assigned to some tracker.
        vector<int> activeAgentID; //to store agent id's who are not dead(both active and passive)
        vector<Rect> activeAgentsWin;  //vector storing MS windows of active Agents and pred Pos of passive Agents

#if(TRACKING_STRATEGY3)

        track_TS3(img.clone(),Tracker,HogWin,outputPath,occ_prob,dij_Mat,frameNum,prevImg,deadIndicies,root,
                  rf_g,rf_samples,rf_labels,dataFile,relation2,deadHists,deadIDs,GMM_TRAINING);

#else
        if(HogWin.size() > 0)
        {
            Mat tempImg2(img.rows,img.cols,CV_8UC3,Scalar::all(0));
            vector<Rect> graphTrackROIs;
            vector<int> graphTrackIDs;
            // Step - 2
            // Run Mean Shift Tracker on the detected HOG windows of previous frame

            float ms_time =0.0;

            for(int agCount =0 ; agCount < Tracker.size() ; agCount++)
            {
                if(Tracker[agCount].status && Tracker[agCount].activeTimeSpan >= 1)
                {
                    //Initializing MS Win from predicted locations
                    Tracker[agCount].MS_Rect = Tracker[agCount].KF_RectPredicted;

                    float matchval;

                    //Func doing MS iterations and stores location in MS_Rect for active agents
                    start =  time.getElapsedTimeInMilliSec();
                    Tracker[agCount].localizeAppearanceByMeanShift( img2, bhatCoefThreshold , &matchval );
                    end =  time.getElapsedTimeInMilliSec();

                    activeAgentID.push_back(Tracker[agCount].id);
                    activeAgentsWin.push_back(Tracker[agCount].MS_Rect);

#if(TRACKING_STRATEGY2)
                    // Call the Color Blobs Graph based Tracker
                    if(Tracker[agCount].activeTimeSpan > 1)
                    {
                        Rect outRect;
//                        track_SURF(img2,Tracker[agCount],outRect);
//                        checkBoundary(img2,outRect);

//                        if(outRect.area() == 0)
                            track_colorBlobGraph(img2,Tracker[agCount],outputPath,outRect);

                        if(outRect.area() != 0)
                        {
                            Rect outRect2 = outRect;
                            scaleRect(outRect2,1.2);
                            checkBoundary(img,outRect2);
                            img(outRect2).copyTo(tempImg2(outRect2));

                            graphTrackROIs.push_back(outRect);
                            graphTrackIDs.push_back(Tracker[agCount].id);

                            activeAgentID.pop_back();
                            activeAgentsWin.pop_back();

//                            int matchedDetectionIndex;
//                            float maxOverlap;
//                            // Find closed detection for this tracker
//                            findMostOverlappingWindow(outRect,HogWin,matchedDetectionIndex,maxOverlap);
//                            if(matchedDetectionIndex != -1)
//                            {
//                                if(maxOverlap > 0.8)
//                                    Tracker[agCount].updateAgent(img,HogWin[matchedDetectionIndex],frameNum,activeAgentsWin,int(GMM_TRAINING));
//                                else
//                                    Tracker[agCount].updateAgent(img,outRect,frameNum,activeAgentsWin,int(GMM_TRAINING));

//                                removeHogWinIndex[matchedDetectionIndex]++;
//                                //                            cout << agCount << "\t" << matchedDetectionIndex << endl;
//                            }
                        }
                    }
#endif

                    ms_time = ms_time + ((float)(end-start)/1000.0);

                }
                else if(!Tracker[agCount].dead && Tracker[agCount].activeTimeSpan >=1)
                {
                    activeAgentID.push_back(Tracker[agCount].id);
                    activeAgentsWin.push_back(Tracker[agCount].KF_RectPredicted);
                }
            }

#if(TRACKING_STRATEGY2)

            vector<Rect> tempROIs;
            HogDetector(tempImg2,tempROIs);
            for(int k2=0; k2<tempROIs.size(); k2++)
                rectangle(tempImg2,tempROIs[k2],Scalar(0,0,255),2);
//            imshow("oldDet",tempImg2);
//            waitKey(0);

            vector<Rect> associatedROIs;
            // Solve association in detections and trackers (for graph matching output)
            solveAssociation(img.clone(),graphTrackROIs,tempROIs,associatedROIs);

            Mat newDetectionImg = img.clone();
            Mat blackImg(img.rows,img.cols,CV_8UC3,Scalar::all(0));
            for(int i1=0; i1<associatedROIs.size(); i1++)
            {
//                if(Tracker[graphTrackIDs[i1]].activeTimeSpan > 1)
                {
                    Tracker[graphTrackIDs[i1]].updateAgent(img,associatedROIs[i1],frameNum,activeAgentsWin,int(GMM_TRAINING));
                    scaleRect(associatedROIs[i1],0.7);
                    blackImg(associatedROIs[i1]).copyTo(newDetectionImg(associatedROIs[i1]));
                }
            }

            HogWin.clear();
            HogDetector(newDetectionImg,HogWin);

            vector<Rect> filteredHogwin;
            // Filter overlapping detections
            for(int i1=0; i1<HogWin.size(); i1++)
            {
                Rect r1 = HogWin[i1];
                bool dontAdd = false;

                for(int j1=0; j1<associatedROIs.size(); j1++)
                {
                    Rect r2 = associatedROIs[j1];

                    float overlap = 0;
                    calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
                    float minArea = r1.area() < r2.area() ? r1.area() : r2.area();
                    overlap = overlap/minArea;

                    if(overlap > 0.7)
                    {
                        dontAdd = true;
                        break;
                    }
                }

                if(!dontAdd)
                    filteredHogwin.push_back(HogWin[i1]);
            }

            HogWin.assign(filteredHogwin.begin(),filteredHogwin.end());

            for(int k2=0; k2<HogWin.size(); k2++)
                rectangle(newDetectionImg,HogWin[k2],Scalar(0,0,255),2);
//            imshow("newDet",newDetectionImg);
//            waitKey(0);

//            vector<Rect> HogWinTemp;
//            for(int i1=0; i1<HogWin.size(); i1++)
//            {
//                if(removeHogWinIndex[i1] == 0)
//                    HogWinTemp.push_back(HogWin[i1]);
//                else if(removeHogWinIndex[i1] > 1)
//                    cerr << "Many to One mappings found" << endl;
//            }
//            HogWin = HogWinTemp;
#endif

            if(timingFile)
            {
                if(runavg)
                {
                    runavg_ms = runavg_ms + ms_time;
                    fprintf(fp_runavg,"%f\t",runavg_ms/((float)avg_count));
                }

                if(indtime)
                {
                    ind_ms = ms_time;
                    fprintf(fp_ind,"%f\t",ind_ms);
                }
            }


#if(SPATIAL_LAYOUT)

            // Source locations
            vector<Point2f> srcLocs;
            for(int k3=0; k3<activeAgentsWin.size(); k3++)
            {
                Point2f cp = 0.5*(activeAgentsWin[k3].tl()+activeAgentsWin[k3].br());
                srcLocs.push_back(cp);

                // Test that no two windows have same center
                for(int j3=0; j3<srcLocs.size()-1; j3++)
                {
                    //                    cout << Mat(srcLocs[j3]) << Mat(cp) << endl;
                    // If rounding off gives same points (because Dealunay Triangualtion takes integer points as input)
                    if(int(srcLocs[j3].x) == int(cp.x) && int(srcLocs[j3].y) == int(cp.y))
                    {
                        if(activeAgentsWin[k3].x > activeAgentsWin[j3].x)
                            srcLocs[k3]  += Point2f(1,1);
                        else
                            srcLocs[k3]  -= Point2f(1,1);
                    }
                }
            }

            // Target locations
            vector<Point2f> targetLocs;
            for(int k3=0; k3<HogWin.size(); k3++)
            {
                Point2f cp = 0.5*(HogWin[k3].tl()+HogWin[k3].br());
                targetLocs.push_back(cp);
            }

            // Deliberate attempt to make a vector of vectors so that graph matching function is not required to change much
            vector< vector<Point2f> > targetLocsVector;
            for(int k3=0; k3<activeAgentsWin.size(); k3++)
            {
                targetLocsVector.push_back(targetLocs);
            }

            // Create Source Graph
            Mat imgSLC = img.clone();
            Mat_<int> adjMatSLC;
            vector< vector<int> > nbrListSLC(srcLocs.size());
            draw_subdiv(imgSLC,srcLocs,adjMatSLC,nbrListSLC);

            // Call Graph Matching Routine
            vector<int> matchIndexSLC;
            Mat_<int> adjMatOutSLC;
            vector< vector<float> > outSLC;
            float matchValSLC = 0;
            bool matchOK_SLC = findMatch_MRF_MAP_SLC(adjMatSLC,srcLocs,targetLocsVector,matchIndexSLC,adjMatOutSLC,
                                                     outSLC,matchValSLC,outputPath);


            Mat matchImg;
            attachImages(prevImg.clone(),img.clone(),matchImg);

            Mat outImgSLCtemp = img.clone();
            if(matchOK_SLC)
            {
                for(uint i=0; i<adjMatOutSLC.rows; i++)
                    for(uint j=i+1; j<adjMatOutSLC.cols; j++)
                    {
                        if(adjMatOutSLC[i][j]==1 && matchIndexSLC[i] != -1 && matchIndexSLC[j] != -1)
                        {
                            cv::line(outImgSLCtemp,targetLocsVector[i][matchIndexSLC[i]],targetLocsVector[j][matchIndexSLC[j]],
                                     cv::Scalar(0,255,255),2);
                        }
                    }

                for(int i=0; i<srcLocs.size(); i++)
                {
                    if(matchIndexSLC[i] == -1)
                        continue;
                    Point2f p2;
                    p2.x = targetLocsVector[i][matchIndexSLC[i]].x + prevImg.cols;
                    p2.y = targetLocsVector[i][matchIndexSLC[i]].y;
                    cv::line(matchImg,srcLocs[i],p2,Scalar(0,255,255),2);

                }
            }

            //            imshow("imgSLC",imgSLC);
            //            imshow("outSLC",outImgSLCtemp);
            //            imshow("matchImg",matchImg);
            //            waitKey(0);
#endif

            //Creating Reasoning Matrix
            Mat reasonMat(activeAgentsWin.size(),HogWin.size(),CV_32FC1,Scalar::all(0));

            vector<int>hogAscToAgent_Count(activeAgentsWin.size(),0);     //storing no of hog's associated to agent(MS Window)
            vector<Mat> reasonMatComps(REASONING_MAT_COMP);
            for(int k2=0; k2<REASONING_MAT_COMP; k2++)
                reasonMatComps[k2] = reasonMat.clone();

            Mat graphAllPairsImg;

            //            omp_set_num_threads(4);

            for(int i=0; i<reasonMat.rows; i++)
            {
                MultiTracker MeanShiftWindow;
                if(rgbHist)
                {
                    if(Tracker[activeAgentID[i]].status)
                        MeanShiftWindow.computeColorDistributionRGB(img2,activeAgentsWin[i],MeanShiftWindow.hist);
                    else
                        MeanShiftWindow.hist = Tracker[activeAgentID[i]].hist.clone();
                }

                if(orientHist || fourDimHist)
                {
                    if(Tracker[activeAgentID[i]].status)
                        MeanShiftWindow.calcOrientHist(img2,activeAgentsWin[i]);
                    else
                        MeanShiftWindow.fourDim_hist = Tracker[activeAgentID[i]].fourDim_hist.clone();
                }

                Rect r1 = activeAgentsWin[i];

#if(IMG_SEG_GRAPH_MATCH)
                vector<Vec3b> clusterColors;
                clusterColors.assign(Tracker[activeAgentID[i]].clusterColors.begin(),Tracker[activeAgentID[i]].clusterColors.end());
                imshow("srx",img(r1));
#endif

#if(IMG_APPEARANCE_MODEL)

                Mat_<int> adjMatSrc;
                vector<Point2f> ccPoints;
                vector<Vec3b> ccColors;

                Mat segmentedImgSelectedROIs, graphImgOut,segmentedImgSrc;
                vector<int> selectedCompInd;
                findSourceBlobs_GMM(img(r1).clone(),Tracker[activeAgentID[i]].m1,Tracker[activeAgentID[i]].m4,
                                    segmentedImgSelectedROIs,graphImgOut,adjMatSrc,ccPoints,ccColors,selectedCompInd,
                                    segmentedImgSrc);

                Mat srcSegmentedHist;
                calcHistogram(segmentedImgSrc,srcSegmentedHist,32);

                //                string graphImageName = outputPath;
                //                graphImageName.append("srcImg_").append(to_string(frameNum)).append(to_string(i)).append(".jpg");
                //                imwrite(graphImageName,segmentedImgSelectedROIs);
                //                graphImageName.append("_.jpg");
                //                imwrite(graphImageName,graphImgOut);

                Mat graphPairsDisplayImg = segmentedImgSelectedROIs.clone();
#endif

#if(IMG_APPEARANCE_MODEL2)

                Mat graphSrcImgInput = Tracker[activeAgentID[i]].agentImgUpdated.clone(), graphSrcImgOutput;
                vector<Point2f> blobCentersSrc;
                vector<Vec3b> blobColorsSrc;

                connectedComponents_FloodFill(graphSrcImgInput,graphSrcImgOutput,blobCentersSrc,blobColorsSrc);

                Mat_<int> graphAdjMat;
                vector< vector<int> > graphNbrList(blobCentersSrc.size());
                draw_subdiv(graphSrcImgInput,blobCentersSrc,graphAdjMat,graphNbrList);

#endif

#if(SURF_MATCHING)

                Mat surfSrcImg = Tracker[activeAgentID[i]].agentImgSolo;
                SURF mySurf;
                vector<KeyPoint> kp1;
                Mat desc1;
                mySurf.extended = 0;
                mySurf.upright = 1;

                mySurf.detect(surfSrcImg,kp1);
                mySurf.compute(surfSrcImg,kp1,desc1);

#endif

                //    #pragma omp parallel for
                for(int j=0; j<reasonMat.cols; j++)
                {
                    Rect r2 = HogWin[j];
                    float graphMatchVal = 0;

                    float overlap = 0.0;

                    //Calc overlap b/w two rectangles
                    calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
                    overlap = overlap/(float)(r1.area()+r2.area()-overlap);


#if(IMG_SEG_GRAPH_MATCH)
                    Mat labels;
                    //                    searchBin(img(HogWin[j]).clone(),Mat(Tracker[activeAgentID[i]].clusterCenters),clusterColors,labels);
                    vector< vector<Point2f> > targetCandidates;
                    findTargetCandidates(img(HogWin[j]).clone(),Tracker[activeAgentID[i]].imgMeans,K_MEANS,targetCandidates);
#endif

#if(IMG_APPEARANCE_MODEL)
                    if(overlap > 0)
                    {
                        vector< vector<Point2f> > targetCandidates;
                        vector< vector<Vec3b> > targetCandColors;
                        Mat tempSegmentedImg, segmentedImgTarget;

                        findTargetCandidateBlobs(img(HogWin[j]).clone(),Tracker[activeAgentID[i]].m1,Tracker[activeAgentID[i]].m4,
                                                 selectedCompInd,targetCandidates,targetCandColors,tempSegmentedImg,
                                                 segmentedImgTarget);

                        vector<int> matchIndex;
                        Mat_<int> adjMatOut;
                        Mat outImgtemp = img(HogWin[j]).clone();
                        float graphMatchProb = 0;

                        bool matchOK = findMatch_MRF_MAP(adjMatSrc,ccPoints,ccColors,targetCandidates,targetCandColors,matchIndex,adjMatOut,
                                                         graphMatchProb,outputPath);

                        if(matchOK)
                        {
                            for(uint i=0; i<adjMatOut.rows; i++)
                                for(uint j=i+1; j<adjMatOut.cols; j++)
                                {
                                    if(adjMatOut[i][j]==1)
                                    {
                                        cv::line(outImgtemp,targetCandidates[i][matchIndex[i]],targetCandidates[j][matchIndex[j]],
                                                 cv::Scalar(0,255,255),2);
                                    }
                                }

                            graphMatchVal = int(matchOK);
                        }
                        else
                        {
                            // Histogram matching of segmented images
                            Mat targetSegmentedHist;

                            calcHistogram(segmentedImgTarget,targetSegmentedHist,32);


                            float BCsegmentedHists = 0;
                            computeBhattacharyaCoefficient(srcSegmentedHist,targetSegmentedHist,&BCsegmentedHists);

                            graphMatchVal = BCsegmentedHists;
                        }

                        //                    graphMatchVal = int(matchOK) /** graphMatchProb*/;

                        //                        imshow("srcImg",segmentedImgSelectedROIs);
                        //                        imshow("graphImg",graphImgOut);
                        //                        imshow("targetImg", tempSegmentedImg);


                        //                        imwrite("targetImg.jpg", tempSegmentedImg);

                        //                        waitKey(0);

                        //                    Mat graphPairsDisplayImgTemp;
                        //                    attachImages(graphPairsDisplayImg,tempSegmentedImg,graphPairsDisplayImgTemp);
                        //                    graphPairsDisplayImg = graphPairsDisplayImgTemp.clone();
                    }
#endif

#if(IMG_APPEARANCE_MODEL2)

                    Mat graphDstImgInput = img(r2).clone(), graphDstImgOutput;

                    vector<Point2f> blobCentersDst;
                    vector<Vec3b> blobColorsDst;

                    connectedComponents_FloodFill(graphDstImgInput,graphDstImgOutput,blobCentersDst,blobColorsDst);

                    vector< vector<Point2f> > targetPoints;
                    vector< vector<Vec3b> > targetColors;
                    for(int k3=0; k3<blobCentersSrc.size(); k3++)
                    {
                        targetPoints.push_back(blobCentersDst);
                        targetColors.push_back(blobColorsDst);

                    }


                    vector<int> graphMatchIndex;
                    Mat_<int> graphAdjMatOut;
                    //                    float graphMatchVal2 = 0;

                    bool graphMatchOK = findMatch_MRF_MAP(graphAdjMat,blobCentersSrc,blobColorsSrc,targetPoints,targetColors,graphMatchIndex,
                                                          graphAdjMatOut,graphMatchVal,outputPath);

                    //                    cout << graphMatchVal << "\t" << graphMatchOK << endl;
                    if(graphMatchOK)
                    {
                        int resultEdgeCounter = 0;
                        for(uint i=0; i<graphAdjMatOut.rows; i++)
                            for(uint j=i+1; j<graphAdjMatOut.cols; j++)
                            {
                                if(graphAdjMatOut[i][j]==1)
                                {
                                    cv::line(graphDstImgInput,targetPoints[i][graphMatchIndex[i]],targetPoints[j][graphMatchIndex[j]],
                                             cv::Scalar(0,255,255),2);
                                    resultEdgeCounter++;
                                }
                            }

                        double minValTemp=0, maxValTemp=0;
                        minMaxLoc(graphAdjMat,&minValTemp,&maxValTemp);

                        graphMatchVal = float(resultEdgeCounter)/float(maxValTemp+1);//int(graphMatchOK);
                    }
                    else
                    {
                        graphMatchVal = 0;
                    }

                    for(int k3=0; k3<blobCentersDst.size(); k3++)
                        circle(graphDstImgOutput,blobCentersDst[k3],2,Scalar(255,255,255),2,CV_AA);

                    imshow("graphSrcImgInput",graphSrcImgInput);
                    imshow("graphDstImgInput",graphDstImgInput);
                    imshow("targetCand",graphDstImgOutput);
                    waitKey(0);
#endif


#if(SURF_MATCHING)

                    Mat surfDstImg = img(r2);

                    vector<KeyPoint> kp2;
                    Mat desc2;
                    mySurf.detect(surfDstImg,kp2);
                    mySurf.compute(surfDstImg,kp2,desc2);

                    Point2f surfCenter = 0.5 * (r1.tl() + r1.br());
                    Rect surfRoiOut;
                    Mat surfImgDraw;
                    double surfPercentMatch = surfDesc_Matching1(surfSrcImg,surfDstImg,kp1,kp2,desc1,desc2,surfCenter,
                                                                 surfRoiOut,surfImgDraw);

                    float surfARerr=0,surfScale=0;
                    bool surfTestOK = verifyResultantRoi(r1,surfRoiOut,surfARerr,surfScale);

                    if(!surfTestOK)
                        surfPercentMatch = 0;

                    //                    cout << "Surf percent match - " << surfPercentMatch << endl;

#endif
                    float matchVal = 0.0;

                    MultiTracker HogWindow;
                    if(rgbHist)
                        HogWindow.computeColorDistributionRGB(img2,HogWin[j],HogWindow.hist);
                    if(orientHist || fourDimHist)
                        HogWindow.calcOrientHist(img2,HogWin[j]);

                    if(rgbHist) computeBhattacharyaCoefficient(HogWindow.hist,MeanShiftWindow.hist,&matchVal);
                    if(orientHist) computeBhattacharyaCoefficient(HogWindow.orient_hist,MeanShiftWindow.orient_hist,&matchVal);
                    if(fourDimHist) computeBhattacharyaCoefficient(HogWindow.fourDim_hist,MeanShiftWindow.fourDim_hist,&matchVal);

                    float scale = (float)(Tracker[activeAgentID[i]].MS_Rect.area())/((float)HogWin[j].area());

                    float c1= Tracker[activeAgentID[i]].trackWindow.x+Tracker[activeAgentID[i]].trackWindow.width/2;
                    float c2= Tracker[activeAgentID[i]].trackWindow.y+Tracker[activeAgentID[i]].trackWindow.height/2;

                    float mag_x = c1 - (HogWin[j].x + HogWin[j].width/2);
                    float mag_y = c2 - (HogWin[j].y + HogWin[j].height/2);

                    float mag = sqrt(pow(mag_x,2)+pow(mag_y,2));

                    float instantVelDirn = atan2(mag_y,mag_x);
                    float errVelDirn = 0;
                    if(frameNum > 0)
                        errVelDirn = abs(Tracker[activeAgentID[i]].avgVelocityDirection - instantVelDirn)/6.28;

                    if(errVelDirn > 1)
                        cerr << "Error > 1 \t" << Tracker[activeAgentID[i]].avgVelocityDirection << "\t" <<
                                instantVelDirn << "\t" << errVelDirn  << endl;

                    float scaleFactor = scale < 1 ? scale : 1/scale;

                    if(graphMatchVal != 0)
                        matchVal = graphMatchVal;

                    //                    cout << activeAgentID[i] << "\t" << overlap << "\t" << matchVal << "\t" << graphMatchVal <<
                    //                            "\t" << (1-errVelDirn) << endl;

                    if(overlap>0 /*&& matchVal > matchThre*/ && scale>0.5 && scale<2 /*&& mag < 100*/)
                    {
                        reasonMat.at<float>(i,j) = weights_reasoning[0] * overlap + weights_reasoning[1] * matchVal
        #if(REASONING_MAT_COMP == 3)
                                + weights_reasoning[2] *  scaleFactor /*graphMatchVal*/ /*outSLC[i][j]*/ /*surfPercentMatch/100 * 2*/
        #elif(REASONING_MAT_COMP == 4)
                                + weights_reasoning[3] * (1-errVelDirn)
        #endif
                                ;
#if(WEIGHTS_LEARN)
                        reasonMatComps[0].at<float>(i,j) = overlap;
                        reasonMatComps[1].at<float>(i,j) = matchVal;
                        reasonMatComps[2].at<float>(i,j) = (1-errVelDirn);
#endif
                    }
                    else
                        reasonMat.at<float>(i,j) = 0.0;
                }

                //                if(i==0)
                //                    graphAllPairsImg = graphPairsDisplayImg.clone();
                //                else
                //                {
                //                    Mat graphAllPairsImgTemp;
                //                    attachImages(graphAllPairsImg,graphPairsDisplayImg,graphAllPairsImgTemp,false);
                //                    graphAllPairsImg = graphAllPairsImgTemp.clone();
                //                }
                //                string graphPairsDisplayImgName = outputPath;
                //                graphPairsDisplayImgName.append("graphPair_");
                //                graphPairsDisplayImgName.append(to_string(frameNum)).append("_").append(to_string(i)).append(".jpg");
                //                imwrite(graphPairsDisplayImgName,graphPairsDisplayImg);
            }
            //            imshow("graphPairs",graphAllPairsImg);
            //            waitKey(0);

            //            cout << reasonMat << endl;

#if(HUNGARIAN)

            start = time.getElapsedTimeInMilliSec();
            if(reasonMat.rows > 0 && reasonMat.cols > 0)
            {
                Matrix<double> reasonMat_HM(reasonMat.rows, reasonMat.cols);
                for(int i=0; i<reasonMat.rows; i++)
                    for(int j=0; j<reasonMat.cols; j++)
                    {
                        reasonMat_HM(i,j) = reasonMat.at<float>(i,j) == 0 ? 100: 1-reasonMat.at<float>(i,j);
                    }

                Munkres m_HM;
                m_HM.solve(reasonMat_HM);



                for(int i=0; i<reasonMat.rows; i++)
                    for(int j=0; j<reasonMat.cols; j++)
                        reasonMat.at<float>(i,j) = reasonMat_HM(i,j) == -1 || reasonMat.at<float>(i,j) == 0 ? 0 : 1;
            }
            end = time.getElapsedTimeInMilliSec();
#else

            start = time.getElapsedTimeInMilliSec();
            getBIPsolution(reasonMat); //BIP Module
            end = time.getElapsedTimeInMilliSec();

            //            assert(countNonZero(reasonMat!=reasonMatOrg) == 0);
#endif
            //            cout << reasonMat << endl;

            if(timingFile)
            {
                if(runavg)
                {
                    runavg_bip = runavg_bip + ((float)(end-start)/1000.0);
                    fprintf(fp_runavg,"%f\t",runavg_bip/((float)avg_count));
                }

                if(indtime)
                {
                    ind_bip = ((float)(end-start)/1000.0);
                    fprintf(fp_ind,"%f\t",ind_bip);
                }
            }

            //Finding whether hogWin is associated to any MS Window
            //if hogAscToAgent_Count[i] = 0: HogWin[i] is not associated to any agent
            for(int i=0; i<reasonMat.rows; i++)
            {
                for(int j=0; j<reasonMat.cols; j++)
                {
                    if(reasonMat.at<float>(i,j)==1)
                        hogAscToAgent_Count[i] = 1;
                }
            }

            vector< vector<float> > assigned_reasonMat_comp(REASONING_MAT_COMP);
            for(int i=0; i<reasonMat.rows; i++)
            {
                //Making agent passive if he is not associated with any HogWin
                if(hogAscToAgent_Count[i] == 0)
                {
                    Tracker[activeAgentID[i]].status = false;

                    if(Tracker[activeAgentID[i]].activeTimeSpan==1)
                        Tracker[activeAgentID[i]].activeTimeSpan = -1;
                }
                //Updating Agent who is associated with some HogWin
                //                if(hogAscToAgent_Count[i] == 1)
                else
                {
                    Tracker[activeAgentID[i]].status = true;

                    int j;
                    for(j=0; j<reasonMat.cols; j++)
                    {
                        if(reasonMat.at<float>(i,j) == 1)
                        {
#if(WEIGHTS_LEARN)
                            for(int k2=0; k2<REASONING_MAT_COMP; k2++)
                                assigned_reasonMat_comp[k2].push_back(reasonMatComps[k2].at<float>(i,j));
#endif
                            break;
                        }
                    }
                    //                    Tracker[activeAgentID[i]].updateAgent(img,HogWin[j],frameNum);
                    Tracker[activeAgentID[i]].initialAssignment = HogWin[j];

                    hogLabel[j] =1;
                }
            }

#if(WEIGHTS_LEARN)

            vector<float> diffValues;
            findDifferentialValues_reasonMatComp(assigned_reasonMat_comp,diffValues);

            //            cout << (Mat)diffValues << endl;
            for(int k1=0; k1<diffValues_reasoning.size(); k1++)
                diffValues_reasoning[k1].push_back(diffValues[k1]);

            //            weights_reasoning[0] = 0.5;
            //            weights_reasoning[1] = 0.5;
            updateWeights_reasonMatComp(weights_reasoning,diffValues_reasoning);

            cout << Mat(weights_reasoning) << endl;

#endif

            vector<int> surfResolvedIds;

#if(GROUP_RESOLVE_SURF)

            int exactGropus =0;//Finding groups formed by agents in each frame
            if(timingFile)
            {
                if(groups.size()==0)
                {
                    fprintf(fp_runavg,"%d\t",exactGropus);
                    fprintf(fp_ind,"%d\t",exactGropus);

                    if(runavg)
                        fprintf(fp_runavg,"%f\n",runavg_surf/avg_count);
                    if(indtime)
                        fprintf(fp_ind,"%f\n",0.0000);
                }
            }


            for(int i=0; i<groups.size(); i++)
            {
                int flag = 0, actvID;//flag saying agents forming groups had broken
                //and active ID saying which agent has been occluded

                if(Tracker[groups[i].x].status==Tracker[groups[i].y].status)
                    flag =1;

                if(flag==0)
                {
                    exactGropus++;

                    if(Tracker[groups[i].x].status == true)
                        actvID = Tracker[groups[i].x].id;
                    else
                        actvID = Tracker[groups[i].y].id;

                    vector<Rect> TargetCand;//storing rectangles of agents whose group has broken from prev frame.
                    TargetCand.push_back(Tracker[groups[i].x].prevWindow);
                    TargetCand.push_back(Tracker[groups[i].y].prevWindow);

                    cout << "\n **************************************" << endl;
                    cout << "use surf matching" << endl;
                    cout << "**************************************\n" << endl;

                    start = time.getElapsedTimeInMilliSec();
                    int index = resolveTargetSurf(img(Tracker[actvID].trackWindow),prevImg,TargetCand);
                    end = time.getElapsedTimeInMilliSec();

                    //Depending on surf match updating agent with rectangle in present frame
                    //and making occlued agent as passive if he is wrongly associated with BIP.
                    if(index == 0)
                    {
                        Tracker[groups[i].x].endangered = true;
                        Tracker[groups[i].y].status = false;

                        Tracker[groups[i].x].initialAssignment = Tracker[actvID].trackWindow;
                        Tracker[groups[i].x].status = true;
                        Tracker[groups[i].y].initialAssignment = Rect(0,0,0,0);
#if(GROUPS_PASSING)
                        int dist = Tracker[groups[i].y].prevWindow.x - Tracker[groups[i].x].prevWindow.x;
                        groupsOcc.push_back(Point3i(groups[i].x,groups[i].y,dist));
                        occRects.push_back(Tracker[groups[i].y].prevWindow);
                        Tracker[groups[i].y].dead = true;
#endif
                    }
                    else if (index == 1)
                    {
                        Tracker[groups[i].y].endangered = true;
                        Tracker[groups[i].x].status = false;

                        Tracker[groups[i].y].initialAssignment = Tracker[actvID].trackWindow;
                        Tracker[groups[i].y].status = true;
                        Tracker[groups[i].x].initialAssignment = Rect(0,0,0,0);
#if(GROUPS_PASSING)
                        int dist = Tracker[groups[i].x].prevWindow.x - Tracker[groups[i].y].prevWindow.x;
                        groupsOcc.push_back(Point3i(groups[i].y,groups[i].x,dist));
                        occRects.push_back(Tracker[groups[i].x].prevWindow);
                        Tracker[groups[i].x].dead = true;
#endif
                    }
                    else
                    {
                        // do nothing if index == -1
                    }

                    surfTime = surfTime + ((float)(end-start)/1000.0);
                }

            }

            if(timingFile)
            {
                if(groups.size()>0)
                {
                    fprintf(fp_runavg,"%d\t",exactGropus);
                    fprintf(fp_ind,"%d\t",exactGropus);

                    if(runavg)
                    {
                        runavg_surf = runavg_surf + surfTime;
                        fprintf(fp_runavg,"%f\n",runavg_surf/(float)avg_count);
                    }
                    if(indtime)
                    {
                        ind_surf = surfTime;
                        fprintf(fp_ind,"%f\n",ind_surf);
                    }
                }
            }

            groups.clear();

            // Reset group flag for all agents
            for(int agents=0; agents<Tracker.size(); agents++)
                Tracker[agents].inGroup = false;

            //Finding groups in present frame
            for(int agents=0; agents<Tracker.size(); agents++)
            {
                if(Tracker[agents].status == false || Tracker[agents].activeTimeSpan < 2)
                    continue;

                for(int ag2=agents+1; ag2<Tracker.size(); ag2++)
                {
                    if(Tracker[ag2].status == false || Tracker[ag2].activeTimeSpan < 2)
                        continue;

                    Rect r1 = Tracker[agents].trackWindow;
                    Rect r2 = Tracker[ag2].trackWindow;
                    float overlap;

                    calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);

                    if(overlap>0.1)
                    {
                        //                    cout << "Found group for " << Tracker[agents].id << "\t" << Tracker[ag2].id <<   endl;
                        groups.push_back(Point2i(agents,ag2));
                        Tracker[agents].inGroup = true;
                        Tracker[ag2].inGroup = true;
                    }
                }
            }
#endif


#if(GROUP_RESOLVE_SURF_2)

            for(int i=0; i<groups.size(); i++)
            {
                int flag = 0;   // Flag for the endangered IDs in a group
                int actvID;     // Active ID saying which agent was endangered
                int otherID;

                if(Tracker[groups[i].x].endangered || Tracker[groups[i].y].endangered)
                    flag = 1;

                if(flag==1)
                {
                    if(Tracker[groups[i].x].endangered)
                    {
                        actvID = Tracker[groups[i].x].id;
                        otherID = Tracker[groups[i].y].id;
                    }
                    else
                    {
                        actvID = Tracker[groups[i].y].id;
                        otherID = Tracker[groups[i].x].id;
                    }

                    cout << "considered IDs = " << actvID << "\t" << otherID << endl;

                    vector<Rect> TargetCand;//storing rectangles of agents whose group has broken from prev frame.
                    TargetCand.push_back(Tracker[groups[i].x].trackWindow);
                    TargetCand.push_back(Tracker[groups[i].y].trackWindow);

                    cout << "\n **************************************" << endl;
                    cout << "use surf matching Part-2" << endl;
                    cout << "**************************************\n" << endl;

                    start = time.getElapsedTimeInMilliSec();
                    //                int index = resolveTargetSurf(prevImg(Tracker[actvID].prevWindow),img,TargetCand);
                    int index = resolveTargetSurf(Tracker[actvID].agentImgSolo,img,TargetCand);
                    end = time.getElapsedTimeInMilliSec();

                    //Depending on surf match updating agent with rectangle in present frame
                    if(index == 0)
                    {
                        //                    Tracker[groups[i].x].updateAgent(img,Tracker[actvID].trackWindow,frameNum);
                        Tracker[groups[i].x].endangered = false;
                        Tracker[groups[i].y].endangered = false;
                        //                    Tracker[groups[i].y].updateAgent(img,Tracker[otherID].trackWindow,frameNum);

                        Tracker[groups[i].x].initialAssignment = Tracker[actvID].trackWindow;
                        Tracker[groups[i].y].initialAssignment = Tracker[otherID].trackWindow;

                        cout << Tracker[groups[i].x].id << " assigned " << actvID << endl;
                        cout << Tracker[groups[i].y].id << " assigned " << otherID << endl;
                    }
                    else if(index == 1)
                    {
                        //                    Tracker[groups[i].y].updateAgent(img,Tracker[actvID].trackWindow,frameNum);
                        Tracker[groups[i].y].endangered = false;
                        Tracker[groups[i].x].endangered = false;
                        //                    Tracker[groups[i].x].updateAgent(img,Tracker[otherID].trackWindow,frameNum);

                        Tracker[groups[i].y].initialAssignment = Tracker[actvID].trackWindow;
                        Tracker[groups[i].x].initialAssignment = Tracker[otherID].trackWindow;

                        cout << Tracker[groups[i].y].id << " assigned " << actvID << endl;
                        cout << Tracker[groups[i].x].id << " assigned " << otherID << endl;
                    }
                    else
                    {
                        // do nothing if index == -1
                    }
                }
            }

#endif

            // Update the agents after thier assignments have been finalised
            for(int i=0; i<reasonMat.rows; i++)
            {
                if(Tracker[activeAgentID[i]].status)
                    Tracker[activeAgentID[i]].updateAgent(img,Tracker[activeAgentID[i]].initialAssignment,frameNum,
                                                          activeAgentsWin,int(GMM_TRAINING));
            }



            // Step - 4
            // Predict the agent windows using Kalman Filter
            for(int agCount =0 ; agCount < Tracker.size() ; agCount++)
            {
                if(Tracker[agCount].status == false && !Tracker[agCount].dead )
                {

                    /*****Using KF class(not openCV)*****/

                    //                for(int j=0; j< Tracker[agCount].KF.Xhat.size();j++)
                    //                    Tracker[agCount].KF.Xhat[j] =  Tracker[agCount].KF.XhatMinus[j];

                    //                Mat kalmanRect = Tracker[agCount].KF.predict();
                    //                Tracker[agCount].KF_RectPredicted.x = kalmanRect.at<float>(0);
                    //                Tracker[agCount].KF_RectPredicted.y = kalmanRect.at<float>(1);
                    //                Tracker[agCount].KF_RectPredicted.width = Tracker[agCount].trackWindow.width;
                    //                Tracker[agCount].KF_RectPredicted.height = Tracker[agCount].trackWindow.height;
                    /***********************************/

#if(PF_MOTION)
                    std::vector<double> zTemp,xfOut(4,0.0);
                    Tracker[agCount].pointCloud.particleFilterUpdate(process, observation, likelihood, zTemp, 0, 1);
                    Tracker[agCount].pointCloud.filterOutput(xfOut);

                    Tracker[agCount].KF_RectPredicted.x = xfOut[0] - Tracker[agCount].KF_RectPredicted.width/2;
                    Tracker[agCount].KF_RectPredicted.y = xfOut[2] - Tracker[agCount].KF_RectPredicted.height/2;
                    Tracker[agCount].KF_RectPredicted.width = Tracker[agCount].trackWindow.width;
                    Tracker[agCount].KF_RectPredicted.height = Tracker[agCount].trackWindow.height;


                    checkBoundary(img,Tracker[agCount].KF_RectPredicted);

                    Tracker[agCount].frameIds.push_back(frameNum);
                    Tracker[agCount].frameROIs.push_back(Tracker[agCount].KF_RectPredicted);
                    Tracker[agCount].frameObservationFlag.push_back(0);

#endif

#if(KF_MOTION)
                    /******Using KF(openCV)************/
                    //Updating KF when there is no observation
                    setIdentity(Tracker[agCount].KF_CV.measurementMatrix,Scalar::all(0));
                    Mat measure = Mat::zeros(2, 1, CV_32F);
                    measure.convertTo(measure,CV_32FC3);
                    Tracker[agCount].KF_CV.correct(measure);

                    //Predicting KF
                    Mat kalmanPt;
                    Tracker[agCount].KF_CV.predict();
                    kalmanPt = Mat(Point2f(Tracker[agCount].KF_CV.statePost.at<float>(0), Tracker[agCount].KF_CV.statePost.at<float>(1)));

                    Tracker[agCount].KF_RectPredicted.x = kalmanPt.at<float>(0)-Tracker[agCount].KF_RectPredicted.width/2;
                    Tracker[agCount].KF_RectPredicted.y = kalmanPt.at<float>(1)-Tracker[agCount].KF_RectPredicted.height/2;
                    Tracker[agCount].KF_RectPredicted.width = Tracker[agCount].trackWindow.width;
                    Tracker[agCount].KF_RectPredicted.height = Tracker[agCount].trackWindow.height;
                    checkBoundary(img,Tracker[agCount].KF_RectPredicted);


                    Tracker[agCount].frameIds.push_back(frameNum);
                    Tracker[agCount].frameROIs.push_back(Tracker[agCount].KF_RectPredicted);
                    Tracker[agCount].frameObservationFlag.push_back(0);

#endif
                    Tracker[agCount].exitTimeSpan = Tracker[agCount].exitTimeSpan+1;//Increase exit time span of passive agent

                    if(Tracker[agCount].KF_RectPredicted.x<=0 || Tracker[agCount].KF_RectPredicted.x+Tracker[agCount].KF_RectPredicted.width >= img.cols )
                        Tracker[agCount].dead = true;
                    //Tracker[agCount].exitTimeSpan = exitThre;

                    if(Tracker[agCount].KF_RectPredicted.y<=0 || Tracker[agCount].KF_RectPredicted.y+Tracker[agCount].KF_RectPredicted.height >= img.rows )
                        Tracker[agCount].dead = true;
                    //                        Tracker[agCount].exitTimeSpan = exitThre;

                    if(Tracker[agCount].exitTimeSpan >= exitThre)
                        Tracker[agCount].dead = true;
                }
            }


            // Step - 5
            // Process newly detected people
            vector<Rect> newAgents;
            for(int k2=0; k2< HogWin.size(); k2++)
            {
                 if(hogLabel[k2]==0)
                {
                    newAgents.push_back(HogWin[k2]);
                    MultiTracker newPerson;
                    Tracker.push_back(newPerson);
                    Tracker[Tracker.size()-1].initAgent(img , HogWin[k2],frameNum,activeAgentsWin,GMM_TRAINING);
                }
            }

#if(GROUPS_PASSING)

            Mat groupPassImg = img.clone();

            vector<int> removePair(groupsOcc.size(),-1);
            for(int i1=0; i1<groupsOcc.size(); i1++)
            {
                int occludedID = groupsOcc[i1].y;
                int occludingID = groupsOcc[i1].x;

                Rect occludingROI = Tracker[occludingID].CurrRegion;
                Point2i c1,c2;
                c1.x = occludingROI.x + occludingROI.width/2 + groupsOcc[i1].z;
                c1.y = occludingROI.y + occludingROI.height/2;

                c2.x = occludingROI.x + occludingROI.width/2 - groupsOcc[i1].z;
                c2.y = occludingROI.y + occludingROI.height/2;

                Rect r1 = occRects[i1];
                r1.x = c1.x - r1.width/2;
                r1.y = c1.y - r1.height/2;

                Rect r2 = occRects[i1];
                r2.x = c2.x - r2.width/2;
                r2.y = c2.y - r2.height/2;

                int maxInd1, maxInd2;
                float maxOverlap1, maxOverlap2;
                findMostOverlappingWindow(r1,newAgents,maxInd1,maxOverlap1);
                findMostOverlappingWindow(r2,newAgents,maxInd2,maxOverlap2);

                float bc1 = 0;
                float bc2 = 0;

                if(maxOverlap1 > 0.5)
                    r1 = newAgents[maxInd1];
                if(maxOverlap2 > 0.5)
                    r2 = newAgents[maxInd2];

                Mat hist1;
                MultiTracker tempTracker;
                tempTracker.computeColorDistributionRGB(img,r1,tempTracker.hist);
                hist1 = tempTracker.hist.clone();

                Mat hist = Tracker[occludedID].hist;
                computeBhattacharyaCoefficient(hist,hist1,&bc1);

                Mat hist2;
                tempTracker.computeColorDistributionRGB(img,r2,tempTracker.hist);
                hist2 = tempTracker.hist.clone();

                computeBhattacharyaCoefficient(hist,hist2,&bc2);

                int maxInd = -1;

                if(bc1 > bc2)
                    maxInd = maxInd1;
                else if(bc1 < bc2)
                    maxInd = maxInd2;
                else
                    maxInd = maxInd1;

                if(bc1 > 0.8 || bc2 > 0.8)
                {
                    Rect recUpdate;

                    if(maxOverlap1 > 0.5 || maxOverlap2 > 0.5)
                    {
                        Tracker[Tracker.size()-newAgents.size()+maxInd].dead = true;
                        Tracker[Tracker.size()-newAgents.size()+maxInd].status = false;

                        recUpdate = newAgents[maxInd];
                        Tracker[occludedID].updateAgent(img,recUpdate,frameNum,activeAgentsWin,int(GMM_TRAINING));
                    }
                    else
                    {
                        recUpdate = bc1>bc2?r1:r2;
                        Tracker[occludedID].updateAgent(img,recUpdate,frameNum,activeAgentsWin,int(GMM_TRAINING));
                    }

                    removePair[i1] = 1;

//                    float overlap = 0;
//                    calcOverlap(occludingROI.tl(),recUpdate.tl(),occludingROI.size(),recUpdate.size(),&overlap);
//                    if(overlap > 0.1)
//                    {
//                        groups.push_back(Point2i(occludingID,occludedID));
//                        Tracker[occludedID].inGroup = true;
//                        Tracker[occludingID].inGroup = true;
//                    }

                }

//                rectangle(groupPassImg,occludingROI,Scalar(0,255,0),2,CV_AA);
//                rectangle(groupPassImg,r1,Scalar(255,255,0),2,CV_AA);
//                rectangle(groupPassImg,r2,Scalar(0,255,255),2,CV_AA);
//                imshow("groupPassImg",groupPassImg);
//                waitKey(0);

            }

            vector<Point3i> groupsTemp;
            vector<Rect> occRectsTemp;
            for(int i1=0; i1<groupsOcc.size(); i1++)
            {
                if(removePair[i1] == -1)
                {
                    groupsTemp.push_back(groupsOcc[i1]);
                    occRectsTemp.push_back(occRects[i1]);
                }
            }

            groupsOcc.clear();
            occRects.clear();
            groupsOcc.assign(groupsTemp.begin(),groupsTemp.end());
            occRects.assign(occRectsTemp.begin(),occRectsTemp.end());
#endif


        }
        // else if no humans detected in this frame
        else
        {
            if(timingFile)
            {
                if(runavg)
                {
                    fprintf(fp_runavg,"%f\t",runavg_ms/avg_count);
                    fprintf(fp_runavg,"%f\t",runavg_bip/avg_count);
                }
                if(indtime)
                {
                    fprintf(fp_ind,"%f\t",0.0000);
                    fprintf(fp_ind,"%f\t",0.0000);
                }
            }

            for(int agCount =0 ; agCount < Tracker.size() ; agCount++)
            {
                if(!Tracker[agCount].dead)
                {
                    if(Tracker[agCount].status == true )
                        Tracker[agCount].status = false;

                    /***********Using KF class(not openCV)**********/
                    //                    else
                    //                    {
                    //                    for(int j=0; j< Tracker[agCount].KF.Xhat.size();j++)
                    //                        Tracker[agCount].KF.Xhat[j] =  Tracker[agCount].KF.XhatMinus[j];
                    //                    }

                    //                    Mat kalmanRect = Tracker[agCount].KF.predict();
                    //                    Tracker[agCount].KF_RectPredicted.x = kalmanRect.at<float>(0);
                    //                    Tracker[agCount].KF_RectPredicted.y = kalmanRect.at<float>(1);
                    //                    Tracker[agCount].KF_RectPredicted.width = Tracker[agCount].trackWindow.width;
                    //                    Tracker[agCount].KF_RectPredicted.height = Tracker[agCount].trackWindow.height;

                    /**********************************************/

#if(PF_MOTION)
                    std::vector<double> zTemp,xfOut(4,0.0);
                    Tracker[agCount].pointCloud.particleFilterUpdate(process, observation, likelihood, zTemp, 0, 1);
                    Tracker[agCount].pointCloud.filterOutput(xfOut);

                    Tracker[agCount].KF_RectPredicted.x = xfOut[0] - Tracker[agCount].KF_RectPredicted.width/2;
                    Tracker[agCount].KF_RectPredicted.y = xfOut[2] - Tracker[agCount].KF_RectPredicted.height/2;
                    Tracker[agCount].KF_RectPredicted.width = Tracker[agCount].trackWindow.width;
                    Tracker[agCount].KF_RectPredicted.height = Tracker[agCount].trackWindow.height;

                    checkBoundary(img,Tracker[agCount].KF_RectPredicted);

                    Tracker[agCount].frameIds.push_back(frameNum);
                    Tracker[agCount].frameROIs.push_back(Tracker[agCount].KF_RectPredicted);
                    Tracker[agCount].frameObservationFlag.push_back(0);

#endif

                    /*****Using KF class(openCV)*****/
#if(KF_MOTION)
                    setIdentity(Tracker[agCount].KF_CV.measurementMatrix,Scalar::all(0));
                    Mat measure = Mat::zeros(2, 1, CV_32F);
                    measure.convertTo(measure,CV_32FC3);
                    Tracker[agCount].KF_CV.correct(measure);

                    Mat kalmanPt;
                    Tracker[agCount].KF_CV.predict();
                    kalmanPt = Mat(Point2f(Tracker[agCount].KF_CV.statePost.at<float>(0), Tracker[agCount].KF_CV.statePost.at<float>(1)));

                    Tracker[agCount].KF_RectPredicted.x = kalmanPt.at<float>(0)-Tracker[agCount].KF_RectPredicted.width/2;
                    Tracker[agCount].KF_RectPredicted.y = kalmanPt.at<float>(1)-Tracker[agCount].KF_RectPredicted.height/2;
                    Tracker[agCount].KF_RectPredicted.width = Tracker[agCount].trackWindow.width;
                    Tracker[agCount].KF_RectPredicted.height = Tracker[agCount].trackWindow.height;

                    checkBoundary(img, Tracker[agCount].KF_RectPredicted);

                    Tracker[agCount].frameIds.push_back(frameNum);
                    Tracker[agCount].frameROIs.push_back(Tracker[agCount].KF_RectPredicted);
                    Tracker[agCount].frameObservationFlag.push_back(0);
#endif

                    if(Tracker[agCount].KF_RectPredicted.x<=0 || Tracker[agCount].KF_RectPredicted.x+Tracker[agCount].KF_RectPredicted.width >= img.cols )
                        Tracker[agCount].dead = true;
                    //                        Tracker[agCount].exitTimeSpan = exitThre;

                    if(Tracker[agCount].KF_RectPredicted.y<=0 || Tracker[agCount].KF_RectPredicted.y+Tracker[agCount].KF_RectPredicted.height >= img.rows )
                        Tracker[agCount].dead = true;
                    //                        Tracker[agCount].exitTimeSpan = exitThre;

                    if(Tracker[agCount].exitTimeSpan < exitThre)
                        Tracker[agCount].exitTimeSpan = Tracker[agCount].exitTimeSpan+1;//Increase exit time span of passive agent

                    if(Tracker[agCount].exitTimeSpan >= exitThre)
                        Tracker[agCount].dead = true;
                }
            }
        }

#endif

#if(THETA_FILE)

        // Write some data
        for(int k1=0; k1<Tracker.size(); k1++)
        {
            if(Tracker[k1].dead || Tracker[k1].activeTimeSpan < 1)
                continue;

            int tempId = Tracker[k1].id;

            string tempFileName = outputPath;
            tempFileName.append("theta_").append(to_string(tempId)).append(".txt");
            ofstream thetaFile(tempFileName,ofstream::out | ofstream::app);

            thetaFile << Tracker[k1].avgVelocityDirection << "\t" << Tracker[k1].scale  << endl;
        }

#endif
        double end_=  getTickCount();//time.getElapsedTimeInMilliSec();
        avgTime = avgTime + ((float)(end_-start_)/getTickFrequency());
        if(timingFile)  /*fprintf(fp_avg,"%f\n",avgTime/(float)(avg_count));*/
        fp_avg << avgTime/(float)(avg_count) << endl;


        // Step - 6
        // Display the results
        for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
            Tracker[i1->first].markAgents(tempImg);

        video3.write(tempImg);

#if(IMG_SEG_GRAPH_MATCH)
        imshow("clusteredImg1",clusteredImg);
#endif

        namedWindow("img",WINDOW_NORMAL);
        if(showImage)
        {
            imshow("img",tempImg);
        }
        char exitChar = waitKey(WAITKEY);
        if(exitChar == 'p' || exitChar == 'P')
            WAITKEY = WAITKEY == 1 ? 0 : 1;
        if(exitChar == '\x1b')
        {
            cout << "exiting" << endl;
            //            fclose(fp_runavg);
            //            fclose(fp_avg);
            //            fclose(fp_ind);
            //            video3.release();
            //            exit(-1);
            destroyWindow("img");
            break;
        }


        //       Write the results of all kinds
        /*********Getting Agent Images and Traj. file*******/
        if(agentImages)
        {
            if(frameNum==0)
                system("rm -r static_res");

            for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
            {
                int id1 = i1->first;
                char textString[100];
                sprintf(textString,"mkdir -p static_res/%d",Tracker[id1].id);
                system(textString);

                if(!Tracker[id1].dead && Tracker[id1].activeTimeSpan > 1)
                {
                    //stroring agent image windows
                    Mat imgRoi;
                    if(Tracker[id1].status)
                    {
                        imgRoi = img(Tracker[id1].trackWindow);
                        sprintf(textString,"./static_res/%d/%d.jpg",Tracker[id1].id,frameNum+1);
                        imwrite(textString,imgRoi);
                    }

                    sprintf(textString,"./static_res/%d/agentInfo.txt",Tracker[id1].id);
                    ofstream traj_scale(textString,ofstream::out | ofstream::app);
                    if(Tracker[id1].status)
                    {
                        traj_scale << frameNum+1 << "\t" << (Tracker[id1].trackWindow.x + Tracker[id1].trackWindow.width/2) <<
                                      "\t" << (Tracker[id1].trackWindow.y + Tracker[id1].trackWindow.height/2) <<"\t"<<Tracker[id1].scale <<endl;
                    }
                    else
                    {
                        traj_scale << frameNum+1 << "\t" << (Tracker[id1].KF_RectPredicted.x + Tracker[id1].KF_RectPredicted.width/2) <<
                                      "\t" << (Tracker[id1].KF_RectPredicted.y + Tracker[id1].KF_RectPredicted.height/2) <<"\t"<<Tracker[id1].scale <<endl;
                    }
                }
            }
        }

#if(AR_OPENCV)        
        vector<int> detected_agentID;
        vector<Rect> detected_agentROI;

        for(int j1=0; j1<Tracker.size(); j1++)
        {
            if(!Tracker[j1].dead && Tracker[j1].activeTimeSpan > 1)
            {
                detected_agentID.push_back(Tracker[j1].id);

                if(Tracker[agents].status)
                    detected_agentROI.push_back(Tracker[j1].trackWindow);
                else
                    detected_agentROI.push_back(Tracker[j1].KF_RectPredicted);
            }
        }

        detected_agentIDs.push_back(detected_agentID);
        detected_agentROIs.push_back(detected_agentROI);
#endif

        char frameNumTxt[20];
        sprintf(frameNumTxt,"%d",frameNum+1);

        capInput >> img;
        prevImg = img.clone();

        if(img.empty())
        {
            cout << "Empty Image read, exiting" << endl;
            break;
        }

        // In Nevatia_USC, they have skipped one frame somehow, so this is for levelling the equation
#if(DATASET == 1)
        if(frameNum == 998)
            break;
#endif
    }

#if(AR_OPENCV)
    FileStorage algo_results("algo_results.xml", FileStorage::WRITE);

    algo_results << "ids" << "[";
    for(int k2=0; k2<detected_agentIDs.size(); k2++)
    {
        algo_results << detected_agentIDs[k2];
    }
    algo_results << "]";

    algo_results << "ROIs" << "[";
    for(int k2=0; k2<detected_agentROIs.size(); k2++)
    {
        algo_results << detected_agentROIs[k2];
    }
    algo_results << "]";

    algo_results.release();
#endif



    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        writeResultFile(Tracker[i1->first],root);
    }

    doc.SaveFile(outTrackDocName.c_str());

    cout << "done" << endl;
    if(timingFile)
    {
        fclose(fp_runavg);
//        fclose(fp_avg);
        fclose(fp_ind);
        fp_avg.close();
    }
    video3.release();
}
